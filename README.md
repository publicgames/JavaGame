# JavaGame
- 3 modules (Game Master, Communication Server, Players)
- TCP/IP connections between them
- Graphical User Interface

How to build this project:
a) Clone whole repo. (or download as zip and unpack it)
b) Go into JavaGame directory.
c) Run command `mvn clean dependency:copy-dependencies package` or run Run_001_BuildProject.bat

Now to run game:
1) Go to 'CommunicationServer/target' and run Communication Server with
java -jar communication-server.jar 
or run Run_002_CommunicationServer.bat
To see available options type:
java -jar communication-server.jar --help

2) Go to 'GameMaster/target' and run To start Game Master with
java -jar game-master.jar
or run Run_003_GameMaster.bat in root
To see available options type:
java -jar game-master.jar --help
or change default_gameMaster_config.cfg 

3) Go to 'Player/target' and run To start Player with
java -jar player.jar
or run Run_004_Player.bat
To see available options type:
java -jar player.jar --help
or change default_player_config.cfg 