package com.se2.comserver.configuration;

public class ComServerConfiguration {
    public final String configuration_file;
    public final int cs_port_number;
    public final String ip_to_connect_to;
    public final int port_number_to_connect_to;
    public final String logger_file;
    public final String name_to_use;
    public final boolean verbose;

    public ComServerConfiguration(String configuration_file, int cs_port_number, String ip_to_connect_to, int port_number_to_connect_to, String logger_file, String name_to_use, boolean verbose) {
        this.configuration_file = configuration_file;
        this.cs_port_number = cs_port_number;
        this.ip_to_connect_to = ip_to_connect_to;
        this.port_number_to_connect_to = port_number_to_connect_to;
        this.logger_file = logger_file;
        this.name_to_use = name_to_use;
        this.verbose = verbose;
    }

    @Override
    public String toString() {
        return "Communication Server Configuration {" +
                "configuration_file='" + configuration_file + '\'' +
                ", cs_port_number=" + cs_port_number +
                ", ip_to_connect_to='" + ip_to_connect_to + '\'' +
                ", port_number_to_connect_to=" + port_number_to_connect_to +
                ", logger_file='" + logger_file + '\'' +
                ", name_to_use='" + name_to_use + '\'' +
                ", verbose=" + verbose +
                '}';
    }
}