package com.se2.comserver;

import com.se2.comserver.configuration.ComServerConfiguration;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ComServerProperties {
    public static final String DEFAULT_CONFIGURATION_FILE = "default_communicationServer_config.cfg";
    public static final String CONFIGURATION_FILE = "CONFIGURATION_FILE";
    public static final String CS_PORT_NUMBER = "CS_PORT_NUMBER";
    public static final String IP_TO_CONNECT_TO = "IP_TO_CONNECT_TO";
    public static final String PORT_TO_CONNECT_TO = "PORT_TO_CONNECT_TO";
    public static final String DEFAULT_LOG_FILE = ".//logs//output.txt";
    public static final String LOG_FILE = "LOG_FILE";
    public static final String NAME_TO_USE = "NAME_TO_USE";
    public static final String TRUE = "y|Y|t|T|true|TRUE|yes|YES";
    public static final String VERBOSE = "VERBOSE";

    public static final List<String> LIST_OF_PROPERTIES = List.of(
            //DEFAULT_CONFIGURATION_FILE,
            CONFIGURATION_FILE,
            CS_PORT_NUMBER,
            IP_TO_CONNECT_TO,
            PORT_TO_CONNECT_TO,
            //DEFAULT_LOG_FILE,
            LOG_FILE,
            NAME_TO_USE,
            TRUE,
            VERBOSE
    );
    public static Map<String, String> valuesOfProperties = new HashMap<>();
    private static Options options = new Options();

    protected static Option addCommandLineOption(String opt, String longOpt, boolean hasArg, String description) {
        Option optionToAdd = new Option(opt, longOpt.toLowerCase(), hasArg, description);
        optionToAdd.setRequired(false);
        optionToAdd.setArgName(longOpt);
        optionToAdd.setType(String.class);
        options.addOption(optionToAdd);
        return optionToAdd;
    }

    public void addCommandLineOptions() {
        addCommandLineOption("h", "help", false, "print out this information");
        addCommandLineOption("c", ComServerProperties.CONFIGURATION_FILE, true, "configuration file with absolute path");
        addCommandLineOption("n", ComServerProperties.NAME_TO_USE, true, "name for console");
        addCommandLineOption("o", ComServerProperties.LOG_FILE, true, "out log file with absolute path");
        addCommandLineOption("p", ComServerProperties.CS_PORT_NUMBER, true, "port of cs for listening");
        addCommandLineOption("gmip", ComServerProperties.IP_TO_CONNECT_TO, true, "ip to connect to");
        addCommandLineOption("gmp", ComServerProperties.PORT_TO_CONNECT_TO, true, "port to connect to");
        addCommandLineOption("v", ComServerProperties.VERBOSE, false, "turn on verbose/debug mode").setType(Boolean.class);
    }

    public Options getOptions() {
        return options;
    }

    public ComServerConfiguration getConfiguration() {
        return new ComServerConfiguration(
                valuesOfProperties.get(CONFIGURATION_FILE),
                Integer.parseInt(valuesOfProperties.get(CS_PORT_NUMBER)),
                valuesOfProperties.get(IP_TO_CONNECT_TO),
                Integer.parseInt(valuesOfProperties.get(PORT_TO_CONNECT_TO)),
                valuesOfProperties.get(LOG_FILE),
                valuesOfProperties.get(NAME_TO_USE),
                valuesOfProperties.get(VERBOSE).matches(TRUE)
        );
    }
}
