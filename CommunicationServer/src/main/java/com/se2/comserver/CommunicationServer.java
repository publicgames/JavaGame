package com.se2.comserver;


import com.se2.tools.Logger;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicInteger;

import static com.se2.tools.ProcessorDefender.sleepPlease;

public class CommunicationServer extends Thread {
    static final AtomicInteger connectionID = new AtomicInteger();
    static int portOfCommunicationServer;
    static String gameMasterIP;
    static int gameMasterPort;


    public CommunicationServer(int portOfCommunicationServer, String gameMasterIP, int gameMasterPort) {
        CommunicationServer.portOfCommunicationServer = portOfCommunicationServer;
        CommunicationServer.gameMasterIP = gameMasterIP;
        CommunicationServer.gameMasterPort = gameMasterPort;
    }

    void log(String text) {
        Logger.log("\tconnectionID=" + connectionID.get() + "\t" + text);
    }

    public void run() {
        while (true) {
            connectionID.incrementAndGet();
            ServerSocket ss = null;
            try {
                ss = new ServerSocket(portOfCommunicationServer);
                log("CommunicationServer is Awaiting new connection attempts.");
                Socket s = ss.accept();
                log("CommunicationServer received connection attempt.");
                Connection t = new Connection(s, connectionID.get());
                t.start();
                ss.close();
                log("CommunicationServer closed socket now. ");
            } catch (IOException ex) {
                log("ERROR: While closing socket got IOException: " + ex);
            }
        }
    }
}

class Connection extends Thread {
    private final int connectionID;
    private Socket s;
    private BufferedReader inputStream;
    private BufferedWriter outputStream;

    Connection(Socket s, int connectionID) throws IOException {
        this.s = s;
        this.connectionID = connectionID;
        this.inputStream = new BufferedReader(new InputStreamReader(s.getInputStream()));
        this.outputStream = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
    }

    void log(String text) {
        Logger.log("\tconnectionID=" + connectionID + "\t" + text);
    }

    private void makeBridge() {
        //connect player to GM
        try (Socket socketToGM = new Socket(CommunicationServer.gameMasterIP, CommunicationServer.gameMasterPort)) {
            BufferedReader inputStreamFromGM = new BufferedReader(new InputStreamReader(socketToGM.getInputStream()));
            BufferedWriter outputStreamToGM = new BufferedWriter(new OutputStreamWriter(socketToGM.getOutputStream()));
            while (socketToGM.isConnected() && s.isConnected()) {
                if (inputStream.ready()) {
                    String passToGM = inputStream.readLine();
                    log("Sending to GM: " + passToGM);
                    outputStreamToGM.write(passToGM + "\n");
                    outputStreamToGM.flush();
                    log("Sent");
                }
                if (inputStreamFromGM.ready()) {
                    String passToPlayer = inputStreamFromGM.readLine();
                    log("Sending to player: " + passToPlayer);
                    outputStream.write(passToPlayer + "\n");
                    outputStream.flush();
                    log("Sent");
                }
                sleepPlease();
            }

            if (!socketToGM.isConnected() && !s.isConnected()) {
                log("CS lost connection to GM and Player.");
                //closing just in case....
                s.close();
                socketToGM.close();
            } else if (s.isConnected()) {
                log("CS lost connection to GM. Closing connection to Player.");
                s.close();
            } else if (socketToGM.isConnected()) {
                log("CS lost connection to Player. Closing connection to GM.");
                socketToGM.close();
            }
            System.exit(1);
        } catch (IOException e) {
            log("CS lost connection to GM or Player. Cleaning.");
            try {
                log("CS will try to close connection to Player.");
                s.close();
                log("CS closed connection to Player.");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }


    public void run() {
        makeBridge();
    }
}
