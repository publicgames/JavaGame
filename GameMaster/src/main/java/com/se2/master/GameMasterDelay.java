package com.se2.master;

import com.se2.communication.messages.*;
import com.se2.master.configuration.GameMasterConfiguration;

import static com.se2.tools.ProcessorDefender.sleepPlease;

public class GameMasterDelay {

    public final int delayNextPiecePlace;
    public final int delayMove;
    public final int delayDiscover;
    public final int delayTest;
    public final int delayPick;
    public final int delayPlace;
    public final int delayBeforeRemovingPawn;
    public final int delayRecalculate;

    public GameMasterDelay(GameMasterConfiguration configuration) {
        this.delayNextPiecePlace = configuration.delayNextPiecePlace;
        this.delayMove = configuration.delayMove;
        this.delayDiscover = configuration.delayDiscover;
        this.delayTest = configuration.delayTest;
        this.delayPick = configuration.delayPick;
        this.delayPlace = configuration.delayPlace;
        this.delayBeforeRemovingPawn = configuration.delayBeforeRemovingPawn;
        this.delayRecalculate = configuration.delayRecalculate;
    }

    public void delay(MoveMessage moveMessage) {
        sleepPlease(delayMove);
    }

    public void delay(DiscoverMessage discoverMessage) {
        sleepPlease(delayDiscover);
    }

    public void delay(TestMessage testMessage) {
        sleepPlease(delayTest);
    }

    public void delay(PickUpMessage pickUpMessage) {
        sleepPlease(delayPick);
    }

    public void delay(PlaceMessage placeMessage) {
        sleepPlease(delayPlace);
    }
}
