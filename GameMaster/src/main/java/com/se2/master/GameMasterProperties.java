package com.se2.master;

import com.se2.game.board.Position;
import com.se2.master.configuration.GameMasterConfiguration;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.se2.tools.PointsUtils.generateListOfPoints;
import static com.se2.tools.PointsUtils.getListOfPointsFromString;

public class GameMasterProperties {

    public static final String DEFAULT_CONFIGURATION_FILE = "default_gameMaster_config.cfg";
    public static final String CONFIGURATION_FILE = "CONFIGURATION_FILE";
    public static final String DEFAULT_LOG_FILE = ".//logs//output.txt";
    public static final String LOG_FILE = "LOG_FILE";
    public static final String NAME_TO_USE = "NAME_TO_USE";
    public static final String TRUE = "y|Y|t|T|true|TRUE|yes|YES";
    public static final String VERBOSE = "VERBOSE";
    public static final String GAMEMASTER_PORT_NUMBER = "GAMEMASTER_PORT_NUMBER";
    public static final String SHAM_PROBABILITY = "SHAM_PROBABILITY";
    public static final String MAX_TEAM_SIZE = "MAX_TEAM_SIZE";
    public static final String MAX_PIECES = "MAX_PIECES";
    public static final String INITIAL_PIECES = "INITIAL_PIECES";
    public static final String GOAL_NUMBER = "GOAL_NUMBER";
    public static final String PREDEFINED_GOAL_POSITIONS = "PREDEFINED_GOAL_POSITIONS";
    public static final String INITIAL_PIECES_POSITIONS = "INITIAL_PIECES_POSITIONS";
    public static final String BOARD_WIDTH = "BOARD_WIDTH";
    public static final String BOARD_TASK_HEIGHT = "BOARD_TASK_HEIGHT";
    public static final String BOARD_GOAL_HEIGHT = "BOARD_GOAL_HEIGHT";
    public static final String DELAY_DESTROY_PIECE = "DELAY_DESTROY_PIECE";
    public static final String DELAY_NEXT_PIECE_PLACE = "DELAY_NEXT_PIECE_PLACE";
    public static final String DELAY_MOVE = "DELAY_MOVE";
    public static final String DELAY_DISCOVER = "DELAY_DISCOVER";
    public static final String DELAY_TEST = "DELAY_TEST";
    public static final String DELAY_PICK = "DELAY_PICK";
    public static final String DELAY_PLACE = "DELAY_PLACE";
    public static final String DELAY_BEFORE_REMOVING_PLAYER = "DELAY_BEFORE_REMOVING_PLAYER";
    public static final String DELAY_RECALCULATE = "DELAY_RECALCULATE";
    public static final List<String> LIST_OF_PROPERTIES = List.of(
            //DEFAULT_CONFIGURATION_FILE,
            CONFIGURATION_FILE,
            //DEFAULT_LOG_FILE,
            LOG_FILE,
            NAME_TO_USE,
            TRUE,
            VERBOSE,

            GAMEMASTER_PORT_NUMBER,

            SHAM_PROBABILITY,
            MAX_TEAM_SIZE,

            MAX_PIECES,
            INITIAL_PIECES,
            GOAL_NUMBER,
            PREDEFINED_GOAL_POSITIONS,
            INITIAL_PIECES_POSITIONS,

            BOARD_WIDTH,
            BOARD_TASK_HEIGHT,
            BOARD_GOAL_HEIGHT,

            DELAY_DESTROY_PIECE,
            DELAY_NEXT_PIECE_PLACE,
            DELAY_MOVE,
            DELAY_DISCOVER,
            DELAY_TEST,
            DELAY_PICK,
            DELAY_PLACE,
            DELAY_BEFORE_REMOVING_PLAYER,
            DELAY_RECALCULATE
    );
    public static Map<String, String> valuesOfProperties = new HashMap<>();
    private static Options options = new Options();

    protected static Option addCommandLineOption(String opt, String longOpt, boolean hasArg, String description) {
        Option optionToAdd = new Option(opt, longOpt.toLowerCase(), hasArg, description);
        optionToAdd.setRequired(false);
        optionToAdd.setArgName(longOpt);
        optionToAdd.setType(String.class);
        options.addOption(optionToAdd);
        return optionToAdd;
    }

    public void addCommandLineOptions() {
        addCommandLineOption("h", "help", false, "print out this information");
        addCommandLineOption("c", GameMasterProperties.CONFIGURATION_FILE, true, "configuration file with absolute path");
        addCommandLineOption("n", GameMasterProperties.NAME_TO_USE, true, "name for console");
        addCommandLineOption("o", GameMasterProperties.LOG_FILE, true, "out log file with absolute path");
        addCommandLineOption("v", GameMasterProperties.VERBOSE, false, "turn on verbose/debug mode").setType(Boolean.class);
        addCommandLineOption("myport", GameMasterProperties.GAMEMASTER_PORT_NUMBER, false, "port of gamemaster").setType(Boolean.class);
        addCommandLineOption("sm", GameMasterProperties.SHAM_PROBABILITY, true, "probability that piece is sham").setType(Double.class);
        addCommandLineOption("mt", GameMasterProperties.MAX_TEAM_SIZE, true, "maximal size of team").setType(Integer.class);
        addCommandLineOption("mp", GameMasterProperties.MAX_PIECES, true, "maximal number of pieces").setType(Integer.class);
        addCommandLineOption("np", GameMasterProperties.INITIAL_PIECES, true, "number of pieces at start").setType(Integer.class);
        addCommandLineOption("goalsnumber", GameMasterProperties.GOAL_NUMBER, true, "number of goals");
        addCommandLineOption("goals", GameMasterProperties.PREDEFINED_GOAL_POSITIONS, true, "positions of goals");
        addCommandLineOption("initpieces", GameMasterProperties.INITIAL_PIECES_POSITIONS, true, "positions of initial pieces");
        addCommandLineOption("boardw", GameMasterProperties.BOARD_WIDTH, true, "board width").setType(Integer.class);
        addCommandLineOption("boardth", GameMasterProperties.BOARD_TASK_HEIGHT, true, "board task area height").setType(Integer.class);
        addCommandLineOption("boardgh", GameMasterProperties.BOARD_GOAL_HEIGHT, true, "board goal area height").setType(Integer.class);
        addCommandLineOption("delaydestroy", GameMasterProperties.DELAY_DESTROY_PIECE, true, "delay for destroy of piece in milliseconds").setType(Integer.class);
        addCommandLineOption("delaypiece", GameMasterProperties.DELAY_NEXT_PIECE_PLACE, true, "delay for adding next piece in milliseconds").setType(Integer.class);
        addCommandLineOption("delaymove", GameMasterProperties.DELAY_MOVE, true, "delay for move in milliseconds").setType(Integer.class);
        addCommandLineOption("delaydiscover", GameMasterProperties.DELAY_DISCOVER, true, "delay for discover in milliseconds").setType(Integer.class);
        addCommandLineOption("delaytest", GameMasterProperties.DELAY_TEST, true, "delay for testing piece in milliseconds").setType(Integer.class);
        addCommandLineOption("delaypick", GameMasterProperties.DELAY_PICK, true, "delay for picking up piece in milliseconds").setType(Integer.class);
        addCommandLineOption("delayplace", GameMasterProperties.DELAY_PLACE, true, "delay for place piece in milliseconds").setType(Integer.class);
        addCommandLineOption("delayremove", GameMasterProperties.DELAY_BEFORE_REMOVING_PLAYER, true, "delay for stuck player in milliseconds").setType(Integer.class);
        addCommandLineOption("delayrecalculate", GameMasterProperties.DELAY_RECALCULATE, true, "delay for recalculate of manhattan distance in milliseconds").setType(Integer.class);
    }

    public Options getOptions() {
        return options;
    }

    public GameMasterConfiguration getConfiguration() {
        Integer boardWidth = Integer.parseInt(valuesOfProperties.get(BOARD_WIDTH));
        Integer taskHeight = Integer.parseInt(valuesOfProperties.get(BOARD_TASK_HEIGHT));
        Integer goalHeight = Integer.parseInt(valuesOfProperties.get(BOARD_GOAL_HEIGHT));



        Integer goalNumber = Integer.parseInt(valuesOfProperties.get(GOAL_NUMBER));
        List<Position> goalPositions;
        if (null==goalNumber) {
            goalPositions = getListOfPointsFromString(valuesOfProperties.get(PREDEFINED_GOAL_POSITIONS));
        } else {
            goalPositions = generateListOfPoints(goalNumber, boardWidth, taskHeight, goalHeight);
        }
        List<Position> piecesPositions = getListOfPointsFromString(valuesOfProperties.get(INITIAL_PIECES_POSITIONS));

        return new GameMasterConfiguration(
                valuesOfProperties.get(CONFIGURATION_FILE),
                valuesOfProperties.get(LOG_FILE),
                valuesOfProperties.get(NAME_TO_USE),
                valuesOfProperties.get(VERBOSE).matches(TRUE),
                Integer.parseInt(valuesOfProperties.get(GAMEMASTER_PORT_NUMBER)),
                Double.parseDouble(valuesOfProperties.get(SHAM_PROBABILITY)),
                Integer.parseInt(valuesOfProperties.get(MAX_TEAM_SIZE)),
                Integer.parseInt(valuesOfProperties.get(MAX_PIECES)),
                Integer.parseInt(valuesOfProperties.get(INITIAL_PIECES)),
                goalNumber,
                goalPositions,
                piecesPositions,
                boardWidth,
                taskHeight,
                goalHeight,
                Integer.parseInt(valuesOfProperties.get(DELAY_DESTROY_PIECE)),
                Integer.parseInt(valuesOfProperties.get(DELAY_NEXT_PIECE_PLACE)),
                Integer.parseInt(valuesOfProperties.get(DELAY_MOVE)),
                Integer.parseInt(valuesOfProperties.get(DELAY_DISCOVER)),
                Integer.parseInt(valuesOfProperties.get(DELAY_TEST)),
                Integer.parseInt(valuesOfProperties.get(DELAY_PICK)),
                Integer.parseInt(valuesOfProperties.get(DELAY_PLACE)),
                Integer.parseInt(valuesOfProperties.get(DELAY_BEFORE_REMOVING_PLAYER)),
                Integer.parseInt(valuesOfProperties.get(DELAY_RECALCULATE))
        );
    }
}
