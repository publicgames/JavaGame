package com.se2.master.enums;

public enum GameMasterStatusEnum {
    INITIALIZING, //connecting to CS
    WAITING_FOR_PLAYERS, //players still connect or are not ready
    GAME_STARTED, //game stated
    GAME_OVER   //game ended.
}
