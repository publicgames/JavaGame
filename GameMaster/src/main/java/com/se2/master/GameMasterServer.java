package com.se2.master;

import com.se2.communication.MessageHelper;
import com.se2.communication.ServerVisitor;
import com.se2.communication.messages.ConnectPlayerMessage;
import com.se2.communication.messages.Message;
import com.se2.tools.Logger;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicInteger;

import static com.se2.tools.ProcessorDefender.sleepPlease;

public class GameMasterServer extends Thread {
    static final AtomicInteger connectionID = new AtomicInteger();
    private static int port;
    private static ServerVisitor visitor;

    public GameMasterServer(int port, ServerVisitor visitor) {
        GameMasterServer.port = port;
        GameMasterServer.visitor = visitor;
    }

    void log(String text) {
        Logger.log("\tconnectionID=" + connectionID.get() + "\t" + text);
    }

    public void run() {

        while (true) {
            connectionID.incrementAndGet();
            ServerSocket ss = null;
            try {
                ss = new ServerSocket(port);
                Logger.log("GameMaster is awaiting new connection attempts.");
                Socket s = ss.accept();
                log("GameMaster received connection attempt.");
                Connection t = new Connection(s, visitor, connectionID.get());
                t.start();
                ss.close();
                log("GameMaster closed socket now. ");
            } catch (IOException ex) {
                log("ERROR: While closing socket got IOException: " + ex);
            }
        }
    }
}

class Connection extends Thread {
    private final int connectionID;
    private Socket s = null;
    private ServerVisitor visitor = null;
    private String playerGuid = null;
    private BufferedReader inputStream;
    private BufferedWriter outputStream;

    Connection(Socket s, ServerVisitor visitor, int connectionID) throws IOException {
        this.s = s;
        this.connectionID = connectionID;
        this.visitor = visitor;
        this.inputStream = new BufferedReader(new InputStreamReader(s.getInputStream()));
        this.outputStream = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
    }

    void log(String text) {
        Logger.log("\tconnectionID=" + connectionID + "\t" + text);
    }

    public void run() {
        log("GM starts new replaying service.");
        while (visitor.gameNotOver()) {
            try {
                if (inputStream.ready()) {
                    String message = inputStream.readLine();
                    Message received = MessageHelper.getMessageFromJson(message);
                    if (null == received) {
                        Logger.log("WARNING: Received message NOT recognised!");
                    } else {
                        if ((playerGuid == null) && (received instanceof ConnectPlayerMessage)) {
                            playerGuid = ((ConnectPlayerMessage) received).getPlayerGuid();
                        }
                        Logger.log("Received message: " + received.toString());
                        Object toSendBack = received.accept(visitor);
                        Logger.log("Respond message: " + toSendBack);
                        if (null != toSendBack) {
                            String jsonToSend = MessageHelper.getJsonFromMessage(toSendBack);
                            outputStream.write(jsonToSend);
                            outputStream.flush();
                        }
                    }
                }
                Object anythingToSend = visitor.getMessageFor(playerGuid);
                if (null != anythingToSend) {
                    Logger.log("Message to be sent: " + anythingToSend);
                    String jsonToSend = MessageHelper.getJsonFromMessage(anythingToSend);
                    Logger.log("Sending out message: " + jsonToSend);
                    outputStream.write(jsonToSend);
                    outputStream.flush();
                    Logger.log("Sent message: " + jsonToSend);
                }
            } catch (IOException ex) {
                Logger.log("ERROR: While receiving message got IOException: " + ex);
                Logger.log("Game Master lost connection to Communication server. Closing...");
                System.out.println("Game Master lost connection to Communication server. Closing...");
                System.exit(1);
            }
            sleepPlease();
        }

        try {
            System.out.println("Socket Closing");
            s.close();
        } catch (IOException ex) {
            Logger.log("ERROR: While closing socket got IOException: " + ex);
            System.exit(1);
        }
    }

}
