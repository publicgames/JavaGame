package com.se2.master;

import com.se2.game.board.Cell;
import com.se2.game.board.Pawn;
import com.se2.ui.BoardGui;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

import static com.se2.ui.CommonParts.setBasicOptions;

public class GameMasterGui extends BoardGui {
    private static int optionsPanelWidth = 125;
    JLabel jLabelGameStatus;
    JLabel jLabelBlue;
    JLabel jLabelRed;
    JLabel jLabelTime;
    //JLabel jLabelConnected;
    private GameMaster gameMaster;


    public GameMasterGui(GameMaster gameMaster) {
        super(gameMaster.getBoard(), "GameMaster", optionsPanelWidth);
        this.gameMaster = gameMaster;
        this.board = gameMaster.getBoard();
    }

    public void paint(Graphics g) {
        if (this.board == null)
            return;
        calculateWidthAndHeight(optionsPanelWidth);
        if (jLabelGameStatus != null)
            jLabelGameStatus.setText(gameMaster.getGameMasterStatus().toString());
        /*if (jLabelConnected != null) {
            String output = "Red: \n";
            for (String playerGUID: gameMaster.redTeam.getTeamGuids(PlayerState.Active)) {
                output += playerGUID + "\n";
            }
            output += "Blue: \n" + "\n";
            for (String playerGUID: gameMaster.blueTeam.getTeamGuids(PlayerState.Active)) {
                output += playerGUID + "\n";
            }
            jLabelConnected.setText(output);
        }*/
        if (jLabelBlue != null)
            jLabelBlue.setText("Blue " + gameMaster.getBlueTeamGoals().toString());
        if (jLabelRed != null)
            jLabelRed.setText("Red " + gameMaster.getRedTeamGoals().toString());
        if (gameMaster.startTime != null && jLabelTime != null) {
            long seconds = (System.currentTimeMillis() - gameMaster.startTime) / (1000);
            int hours = (int) (seconds / 3600);
            String h = hours < 10 ? "0" + hours : "" + hours;
            int minutes = (int) (seconds / 60);
            String m = minutes < 10 ? "0" + minutes : "" + minutes;
            int sec = (int) (seconds % 60);
            String s = sec < 10 ? "0" + sec : "" + sec;
            String elapsed = (seconds / 3600) + ":" + (seconds / 60) + ":" + (seconds % 60);
            jLabelTime.setText(elapsed);
        }
        for (int y = 0; y < boardHeight; y++) {
            for (int x = 0; x < boardWidth; x++) {
                Cell cell = this.board.getCell(x, y);
                processAndDrawCell(g, cell, x, y);

                if (cell.getPawnGuid() != null) {
                    String playerGuid = cell.getPawnGuid();
                    Pawn player = gameMaster.getPawn(playerGuid);
                    drawPlayer(g, player, x, y);
                }
            }
        }
        drawMesh(g);
    }

    public void start() {
        if (thread == null) {
            JFrame window = new JFrame();
            this.window = window;
            setBasicOptions(window, this, "GameMaster");
            thread = new Thread(this);
            thread.start();

            JSplitPane splitPane = new JSplitPane();
            this.window.getContentPane().add(splitPane, BorderLayout.EAST);

            JPanel jPanel = new JPanel(new FlowLayout()); //JPanel containing all buttons
            jPanel.setPreferredSize(new Dimension(optionsPanelWidth, 1700));
            splitPane.setLeftComponent(null);
            splitPane.setRightComponent(jPanel);
            this.setPreferredSize(new Dimension(window.getWidth() - optionsPanelWidth, 1700));

            Dimension preferredSize = new Dimension(125, 25);
            Border border = BorderFactory.createLineBorder(Color.BLACK, 1);

            splitPane.setDividerLocation(0.8);
            JLabel label_1 = new JLabel("Options");
            jPanel.add(label_1);
            JButton btnStartGame = new JButton("Start game");
            btnStartGame.addActionListener(e -> {
                gameMaster.startGame();
            });
            btnStartGame.setPreferredSize(preferredSize);
            jPanel.add(btnStartGame);

            jLabelGameStatus = new JLabel(gameMaster.getGameMasterStatus().toString(), SwingConstants.CENTER);
            jLabelGameStatus.setPreferredSize(preferredSize);
            jLabelGameStatus.setBorder(border);
            jPanel.add(jLabelGameStatus);

            jLabelBlue = new JLabel("Blue " + gameMaster.getBlueTeamGoals().toString(), SwingConstants.CENTER);
            jLabelBlue.setPreferredSize(preferredSize);
            jLabelBlue.setBorder(border);
            jPanel.add(jLabelBlue);

            jLabelRed = new JLabel("Red " + gameMaster.getRedTeamGoals().toString(), SwingConstants.CENTER);
            jLabelRed.setPreferredSize(preferredSize);
            jLabelRed.setBorder(border);
            jPanel.add(jLabelRed);

            jLabelTime = new JLabel("00:00:00", SwingConstants.CENTER);
            jLabelTime.setPreferredSize(preferredSize);
            jLabelTime.setBorder(border);
            jPanel.add(jLabelTime);

           /* jLabelConnected = new JLabel("Connected: ", SwingConstants.CENTER);
            jLabelConnected.setPreferredSize(new Dimension(125, 400));
            jLabelConnected.setBorder(border);
            jPanel.add(jLabelConnected);*/
        }
    }
}