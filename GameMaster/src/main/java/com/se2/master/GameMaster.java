package com.se2.master;

import com.se2.communication.ServerVisitor;
import com.se2.communication.classes.MessageBoard;
import com.se2.communication.classes.MessageFields;
import com.se2.communication.classes.MessagePosition;
import com.se2.communication.enums.PlacementResultEnum;
import com.se2.communication.enums.StatusMessageEnum;
import com.se2.communication.enums.TeamColorMessageEnum;
import com.se2.communication.messages.*;
import com.se2.game.board.Cell;
import com.se2.game.board.Pawn;
import com.se2.game.board.Piece;
import com.se2.game.board.Position;
import com.se2.game.board.enums.Direction;
import com.se2.game.player.enums.PlayerState;
import com.se2.game.team.Team;
import com.se2.game.team.enums.TeamColor;
import com.se2.master.configuration.GameMasterConfiguration;
import com.se2.master.enums.GameMasterStatusEnum;
import com.se2.tools.Logger;
import com.se2.tools.StringUtils;
import com.se2.tools.Uuid;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static com.se2.tools.ConvertersUtils.*;
import static com.se2.tools.Logger.debug;
import static com.se2.tools.Logger.warning;
import static com.se2.tools.ProcessorDefender.sleepPlease;

public class GameMaster implements ServerVisitor {

    private static GameMaster single_gamemaster_instance = null;
    private final GameMasterConfiguration configuration;
    private final GameMasterDelay gameMasterDelay;
    private GameMasterStatusEnum gameMasterStatus;
    Team redTeam;
    Team blueTeam;
    private AtomicInteger redTeamGoals;
    private AtomicInteger blueTeamGoals;
    private String guid;
    private GameMasterBoard gameMasterBoard;
    private Map<String, Queue<Message>> messagesToSend = new HashMap<>();
    Long startTime = null;

    public GameMaster(GameMasterConfiguration configuration) {
        this.configuration = configuration;
        this.gameMasterStatus = GameMasterStatusEnum.INITIALIZING;
        this.gameMasterBoard = new GameMasterBoard(configuration.boardWidth,
                configuration.boardTaskHeight,
                configuration.boardGoalHeight,
                configuration.predefinedGoalPositions,
                configuration.predefinedPiecesPositions,
                configuration.shamProbability);
        this.redTeam = new Team(TeamColor.RED);
        this.blueTeam = new Team(TeamColor.BLUE);
        this.redTeamGoals = new AtomicInteger(configuration.predefinedGoalPositions.size() / 2);
        this.blueTeamGoals = new AtomicInteger(configuration.predefinedGoalPositions.size() / 2);
        this.guid = new Uuid().getId();
        this.gameMasterDelay = new GameMasterDelay(configuration);
    }

    public static GameMaster getInstance(GameMasterConfiguration configuration) {
        if (single_gamemaster_instance == null)
            single_gamemaster_instance = new GameMaster(configuration);
        return single_gamemaster_instance;
    }

    public GameMasterStatusEnum getGameMasterStatus() {
        return gameMasterStatus;
    }

    public AtomicInteger getRedTeamGoals() {
        return redTeamGoals;
    }

    public AtomicInteger getBlueTeamGoals() {
        return blueTeamGoals;
    }

    public GameMasterBoard getBoard() {
        return gameMasterBoard;
    }

    public void waitForPlayers() {
        gameMasterStatus = GameMasterStatusEnum.WAITING_FOR_PLAYERS;
        debug("GM changed status to " + gameMasterStatus);
    }

    public void startGame() {
        gameMasterStatus = GameMasterStatusEnum.GAME_STARTED;
        startTime = System.currentTimeMillis();
        sleepPlease();
        List<String> blueGuids = blueTeam.getTeamGuids(PlayerState.WaitingForGameStartMessage);
        List<String> redGuids = redTeam.getTeamGuids(PlayerState.WaitingForGameStartMessage);
        for (Pawn pawn : getPawnsInStatus(PlayerState.WaitingForGameStartMessage)) {
            Logger.log("  sending");
            pawn.setLastMoveTime(System.currentTimeMillis());
            sendMessage(pawn, new GameStartMessage(
                    pawn.getPlayerGuid(),
                    convertTeamColorToTeamColorMessageEnum(pawn.getTeamColorForPawn()),
                    "Best Team Member",
                    TeamColor.BLUE.equals(pawn.getTeamColorForPawn()) ? blueGuids.size() : redGuids.size(),
                    TeamColor.BLUE.equals(pawn.getTeamColorForPawn()) ? blueGuids : redGuids,
                    new MessagePosition(pawn.getPosition().getX(), pawn.getPosition().getY()),
                    new MessageBoard(getBoard().getBoardWidth(), getBoard().getTaskAreaHeight(), getBoard().getGoalAreaHeight()))
            );

            pawn.setPlayerState(PlayerState.Active);
        }
    }

    public void gameOver() {
        //null means draw
        TeamColorMessageEnum teamColorMessageEnum = redTeamGoals.get() < 1 && blueTeamGoals.get() < 1 ? null : redTeamGoals.get() < 1 ? TeamColorMessageEnum.Red : TeamColorMessageEnum.Blue;

        for (Pawn pawn : getPawnsInStatus(PlayerState.Active)) {
            Logger.log("Sending messages about game end");
            sendMessage(pawn, new EndMessage(teamColorMessageEnum));
            pawn.setPlayerState(PlayerState.Completed);
        }
        sleepPlease();
        gameMasterStatus = GameMasterStatusEnum.GAME_OVER;
    }

    public Pawn getPawn(String guid) {
        Pawn red = redTeam.getPawn(guid);
        return null != red ? red : blueTeam.getPawn(guid);
    }

    void addPawn(String playerGuid) {
        Pawn pawn;
        if (redTeam.getSize() > blueTeam.getSize()) {
            pawn = new Pawn(playerGuid, null, null, null, TeamColor.BLUE);
            blueTeam.addPawn(pawn);
        } else {
            pawn = new Pawn(playerGuid, null, null, null, TeamColor.RED);
            redTeam.addPawn(pawn);
        }
        gameMasterBoard.placePawn(pawn);
        pawn.setPlayerState(PlayerState.WaitingForGameStartMessage);
    }

    private void removePawn(Pawn pawn) {
        gameMasterBoard.removePawn(pawn);
        sendMessage(pawn, new EndMessage(null));
        pawn.setPlayerState(PlayerState.Completed);
    }

    private boolean serverNotFull() {
        return 2 * configuration.maxTeamSize > blueTeam.getSize() + redTeam.getSize();
    }

    private boolean isThereAPawnInStatus(PlayerState playerState) {
        return redTeam.isAnyPawnInStatus(playerState) || blueTeam.isAnyPawnInStatus(playerState);
    }

    private List<Pawn> getPawnsInStatus(PlayerState playerState) {
        List<Pawn> pawns = new ArrayList<>(redTeam.getPawnsInStatus(playerState));
        pawns.addAll(blueTeam.getPawnsInStatus(playerState));
        return pawns;
    }

    private boolean winConditionMeet() {
        return redTeamGoals.get() < 1 || blueTeamGoals.get() < 1;
    }

    private void sendMessage(Pawn player, Message message) {
        Queue<Message> messages = messagesToSend.computeIfAbsent(player.getPlayerGuid(), k -> new LinkedList<>());
        messages.add(message);
    }

    private Message receivedMessageNotForMe(Message message) {
        Logger.log("GameMaster received message: " + message.toString());
        return null;
    }

    @Override
    public Message visit(Message message) {
        return receivedMessageNotForMe(message);
    }

    @Override
    public Message visit(MessageReturn message) {
        return receivedMessageNotForMe(message);
    }

    @Override
    public Message visit(ConnectPlayerMessage message) {
        String guid = message.getPlayerGuid();
        if (!serverNotFull() || !gameMasterStatus.equals(GameMasterStatusEnum.WAITING_FOR_PLAYERS) || StringUtils.isEmpty(guid) || (null != getPawn(message.getPlayerGuid()))) {
            Logger.log(message.action, guid, StatusMessageEnum.DENIED, "");
            return new ConnectPlayerReturnMessage(guid, message.getPortNumber(), StatusMessageEnum.DENIED);
        } else {
            addPawn(guid);
            Logger.log(message.action, guid, StatusMessageEnum.OK, "");
            return new ConnectPlayerReturnMessage(guid, message.getPortNumber(), StatusMessageEnum.OK);
        }
    }

    @Override
    public Message visit(ConnectPlayerReturnMessage message) {
        return receivedMessageNotForMe(message);
    }

    @Override
    public Message visit(DiscoverMessage message) {
        gameMasterDelay.delay(message);
        String guid = message.getPlayerGuid();
        if (!gameMasterStatus.equals(GameMasterStatusEnum.GAME_STARTED) || StringUtils.isEmpty(guid) || (null == getPawn(message.getPlayerGuid()))) {
            return null;
        }
        Pawn receivedPawn = getPawn(message.getPlayerGuid());
        Position receivedPosition = convertMessagePositionToPosition(message.getMessagePosition());
        if (null == receivedPosition || !receivedPosition.equals(receivedPawn.getPosition())) {
            Logger.log("GameMaster received message with wrong position of player: " + message.toString());
            Logger.log(message.action, guid, StatusMessageEnum.DENIED, "");
            return new DiscoverMessageReturn(guid, message.getMessagePosition(), StatusMessageEnum.DENIED, new ArrayList<>());
        }
        List<Cell> toSendBack = gameMasterBoard.discover(receivedPosition);
        if (null == toSendBack) {
            Logger.log(message.action, guid, StatusMessageEnum.DENIED, "");
            return new DiscoverMessageReturn(guid, message.getMessagePosition(), StatusMessageEnum.DENIED, new ArrayList<>());
        }
        List<MessageFields> convertedToSendBack = convertCellsToMessageFields(toSendBack);
        Logger.log(message.action, guid, StatusMessageEnum.OK, "");
        return new DiscoverMessageReturn(guid, message.getMessagePosition(), StatusMessageEnum.OK, convertedToSendBack);
    }

    @Override
    public Message visit(DiscoverMessageReturn message) {
        return receivedMessageNotForMe(message);
    }

    @Override
    public Message visit(EndMessage message) {
        return receivedMessageNotForMe(message);
    }

    @Override
    public Message visit(GameStartMessage message) {
        return receivedMessageNotForMe(message);
    }

    @Override
    public Message visit(MoveMessage message) {
        gameMasterDelay.delay(message);
        String guid = message.getPlayerGuid();
        if (!gameMasterStatus.equals(GameMasterStatusEnum.GAME_STARTED) || StringUtils.isEmpty(guid) || (null == getPawn(message.getPlayerGuid()))) {
            return null;
        }

        Pawn receivedPawn = getPawn(message.getPlayerGuid());
        receivedPawn.setLastMoveTime(System.currentTimeMillis());
        Direction direction = convertMessageDirectionToDirection(message.getDirectionMessageEnum());
        if (gameMasterBoard.movePawn(receivedPawn, direction)) {
            //sending back proper position
            Logger.log(message.action, guid, StatusMessageEnum.OK, message.toString());
            return new MoveMessageReturn(StatusMessageEnum.OK, message.getPlayerGuid(), message.getDirectionMessageEnum(), convertPositionToMessagePosition(receivedPawn.getPosition()));
        } else {
            Logger.log(message.action, guid, StatusMessageEnum.DENIED, message.toString());
            return new MoveMessageReturn(StatusMessageEnum.DENIED, message.getPlayerGuid(), message.getDirectionMessageEnum(), convertPositionToMessagePosition(receivedPawn.getPosition()));
        }
    }

    @Override
    public Message visit(MoveMessageReturn message) {
        return receivedMessageNotForMe(message);
    }

    @Override
    public Message visit(PickUpMessage message) {
        gameMasterDelay.delay(message);
        String guid = message.getPlayerGuid();
        if (!gameMasterStatus.equals(GameMasterStatusEnum.GAME_STARTED) || StringUtils.isEmpty(guid) || (null == getPawn(message.getPlayerGuid()))) {
            return null;
        }

        Pawn receivedPawn = getPawn(message.getPlayerGuid());
        if (gameMasterBoard.takePiece(receivedPawn)) {
            Logger.log(message.action, guid, StatusMessageEnum.OK, receivedPawn.getPiece().isSham().toString());
            return new PickUpMessageReturn(StatusMessageEnum.OK, message.getPlayerGuid());
        } else {
            Logger.log(message.action, guid, StatusMessageEnum.DENIED, "");
            return new PickUpMessageReturn(StatusMessageEnum.DENIED, message.getPlayerGuid());
        }
    }

    @Override
    public Message visit(PickUpMessageReturn message) {
        return receivedMessageNotForMe(message);
    }

    @Override
    public Message visit(PlaceMessage message) {
        gameMasterDelay.delay(message);
        String guid = message.getPlayerGuid();
        if (!gameMasterStatus.equals(GameMasterStatusEnum.GAME_STARTED) || StringUtils.isEmpty(guid) || (null == getPawn(message.getPlayerGuid()))) {
            return null;
        }

        Pawn receivedPawn = getPawn(message.getPlayerGuid());
        if (null == receivedPawn.getPiece()) {
            Logger.log(message.action, guid, StatusMessageEnum.DENIED, "");
            return new PlaceMessageReturn(StatusMessageEnum.DENIED, message.getPlayerGuid(), null);
        }
        if (receivedPawn.getPiece().isSham()) {
            return null;
        }
        if (gameMasterBoard.placePieceByPawn(receivedPawn)) {
            Logger.log(message.action, guid, StatusMessageEnum.OK, "");
            if (TeamColor.RED.equals(receivedPawn.getTeamColorForPawn())) {
                redTeamGoals.decrementAndGet();
            }
            if (TeamColor.BLUE.equals(receivedPawn.getTeamColorForPawn())) {
                blueTeamGoals.decrementAndGet();
            }
            return new PlaceMessageReturn(StatusMessageEnum.OK, message.getPlayerGuid(), PlacementResultEnum.Correct);
        } else {
            Logger.log(message.action, guid, StatusMessageEnum.OK, "");
            return new PlaceMessageReturn(StatusMessageEnum.OK, message.getPlayerGuid(), PlacementResultEnum.Pointless);
        }
    }

    @Override
    public Message visit(PlaceMessageReturn message) {
        return receivedMessageNotForMe(message);
    }

    @Override
    public Message visit(TestMessage message) {
        gameMasterDelay.delay(message);
        String guid = message.getPlayerGuid();
        if (!gameMasterStatus.equals(GameMasterStatusEnum.GAME_STARTED) || StringUtils.isEmpty(guid) || (null == getPawn(message.getPlayerGuid()))) {
            return null;
        }

        Pawn receivedPawn = getPawn(message.getPlayerGuid());
        if (null == receivedPawn.getPiece()) {
            Logger.log(message.action, guid, StatusMessageEnum.DENIED, "null");
            return new TestMessageReturn(StatusMessageEnum.DENIED, message.getPlayerGuid(), false);
        } else {
            Piece toTest = receivedPawn.getPiece();
            if (toTest.isSham()) {
                Logger.log(message.action, guid, StatusMessageEnum.OK, "false");
                receivedPawn.setPiece(null);
                return new TestMessageReturn(StatusMessageEnum.OK, message.getPlayerGuid(), false);
            } else {
                Logger.log(message.action, guid, StatusMessageEnum.OK, "true");
                return new TestMessageReturn(StatusMessageEnum.OK, message.getPlayerGuid(), true);
            }
        }
    }

    @Override
    public Message visit(TestMessageReturn message) {
        return receivedMessageNotForMe(message);
    }

    @Override
    public boolean gameNotOver() {
        return !GameMasterStatusEnum.GAME_OVER.equals(gameMasterStatus);
    }

    @Override
    public synchronized Message getMessageFor(String playerGuid) {
        Queue<Message> messages = messagesToSend.get(playerGuid);
        //change to linked list
        if (null != messages && null != messages.peek()) {
            return messages.poll();
        } else {
            return null;
        }
    }

    public void startOneGame() {
        (new Thread(new Game())).start();
    }

    public class Game implements Runnable {

        @Override
        public void run() {
            if (gameMasterStatus.equals(GameMasterStatusEnum.INITIALIZING)) {
                gameMasterStatus = GameMasterStatusEnum.WAITING_FOR_PLAYERS;
            }

            while (gameMasterStatus.equals(GameMasterStatusEnum.WAITING_FOR_PLAYERS) && (serverNotFull())) {
                sleepPlease(1000);
            }
            sleepPlease(2000);
            if (gameMasterStatus.equals(GameMasterStatusEnum.WAITING_FOR_PLAYERS)) {
                startGame();
            }

            long mark1 = System.currentTimeMillis();
            long recalculateManhattanDistances = System.currentTimeMillis();
            while (gameMasterStatus.equals(GameMasterStatusEnum.GAME_STARTED)) {
                if ((System.currentTimeMillis()) - mark1 > gameMasterDelay.delayNextPiecePlace) {
                    gameMasterBoard.clearPieces();
                    gameMasterBoard.generatePieces(configuration.maxPieces);
                    mark1 = System.currentTimeMillis();
                }
                if ((System.currentTimeMillis()) - recalculateManhattanDistances > gameMasterDelay.delayRecalculate) {
                    gameMasterBoard.calculateManhattanDistances();
                    recalculateManhattanDistances = System.currentTimeMillis();
                }
                for (Pawn pawn : getPawnsInStatus(PlayerState.Active)) {
                    if ((System.currentTimeMillis()) - pawn.getLastMoveTime() > gameMasterDelay.delayBeforeRemovingPawn) {
                        warning("Removing player: " + pawn);
                        removePawn(pawn);
                    }
                }
                if (winConditionMeet()) {
                    gameOver();
                }
                sleepPlease();
            }
        }
    }
}
