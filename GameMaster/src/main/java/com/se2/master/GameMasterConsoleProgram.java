package com.se2.master;

import com.se2.master.configuration.GameMasterConfiguration;
import com.se2.tools.Logger;
import org.apache.commons.cli.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class GameMasterConsoleProgram {

    public static GameMaster gameMaster;
    public static GameMasterProperties properties;
    public static GameMasterConfiguration configuration;

    private static CommandLine cmd;

    public static void main(String[] argv) {
        loadConfiguration(argv);
        //Start logger
        Logger.start(configuration.verbose, configuration.logger_file);
        Logger.log(configuration.toString());

        //Create instance of GM
        gameMaster = GameMaster.getInstance(configuration);

        //start server for replaying messages
        GameMasterServer gameMasterServer = new GameMasterServer(configuration.gm_port_number, gameMaster);
        gameMasterServer.start();

        gameMaster.startOneGame();

        //open gui
        GameMasterGui gameMasterGui = new GameMasterGui(gameMaster);
        gameMasterGui.start();
        System.out.println("GameServer Started");
    }

    public static void loadConfiguration(String[] argv) {
        setUpPropertiesToBeUsed();
        processCommandLine(argv, properties);
        Properties prop = processChoosenConfigFile(properties);
        establishFinalConfiguration(prop);
        configuration = properties.getConfiguration();
    }

    public static void setUpPropertiesToBeUsed() {
        properties = new GameMasterProperties();
    }

    private static void processCommandLine(String[] args, GameMasterProperties properties) {
        properties.addCommandLineOptions();

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        cmd = null;

        try {
            cmd = parser.parse(properties.getOptions(), args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("Game Master", properties.getOptions());
            System.exit(1);
        }

        if (cmd.hasOption("help")) {
            formatter.printHelp("Game Master", properties.getOptions());
            System.exit(1);
        }

        for (Option option : cmd.getOptions()) {
            if (option.hasArg()) {
                GameMasterProperties.valuesOfProperties.put(option.getArgName(), option.getValue());
            } else {
                GameMasterProperties.valuesOfProperties.put(option.getArgName(), "Y");
            }
        }
    }

    public static Properties processChoosenConfigFile(GameMasterProperties properties) {
        String fileToLoadFrom = GameMasterProperties.valuesOfProperties.getOrDefault(GameMasterProperties.CONFIGURATION_FILE, GameMasterProperties.DEFAULT_CONFIGURATION_FILE);
        Properties prop = new Properties();
        try {
            InputStream is = null;
            is = ClassLoader.getSystemResourceAsStream(fileToLoadFrom);
            if (null == is) {
                System.out.println("Configuration file not found in resources: " + fileToLoadFrom);
                //try to upload from directory
                is = new FileInputStream(fileToLoadFrom);
                if (null == is) {
                    System.out.println("Configuration file not found in directory: " + fileToLoadFrom);
                    System.exit(1);
                }
            }
            prop.load(is);
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
            System.out.println("File not found: " + fileToLoadFrom);
            System.exit(1);
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.out.println("Exception IOException when trying to load content of: " + fileToLoadFrom);
            System.exit(1);
        }
        return prop;
    }

    public static void establishFinalConfiguration(Properties prop) {
        List<String> configurationToFill = new ArrayList<String>(GameMasterProperties.LIST_OF_PROPERTIES);
        configurationToFill.removeAll(GameMasterProperties.valuesOfProperties.keySet());
        List<String> filled = new ArrayList<String>();
        for (String configurable : configurationToFill) {
            GameMasterProperties.valuesOfProperties.put(configurable, (String) prop.get(configurable));
            filled.add(configurable);
        }
        configurationToFill.removeAll(filled);
        if (!configurationToFill.isEmpty()) {
            System.out.println("Not not set: " + configurationToFill);
        }
    }
}
