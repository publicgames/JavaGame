package com.se2.master.configuration;

import com.se2.game.board.Position;

import java.util.List;

public class GameMasterConfiguration {

    public final String configuration_file;
    public final String logger_file;
    public final String name_to_use;
    public final boolean verbose;
    public final double shamProbability;
    public final int maxTeamSize;
    public final int maxPieces;
    public final int initialPieces;
    public final int goalNumber;
    public final List<Position> predefinedGoalPositions;
    public final List<Position> predefinedPiecesPositions;
    public final int boardWidth;
    public final int boardTaskHeight;
    public final int boardGoalHeight;
    public final int delayDestroyPiece;
    public final int delayNextPiecePlace;
    public final int delayMove;
    public final int delayDiscover;
    public final int delayTest;
    public final int delayPick;
    public final int delayPlace;
    public final int delayBeforeRemovingPawn;
    public final int delayRecalculate;
    public int gm_port_number;

    public GameMasterConfiguration(String configuration_file, String logger_file, String name_to_use, boolean verbose, int gm_port_number, double shamProbability, int maxTeamSize, int maxPieces, int initialPieces, int goalNumber, List<Position> predefinedGoalPositions, List<Position> predefinedPiecesPositions, int boardWidth, int boardTaskHeight, int boardGoalHeight, int delayDestroyPiece, int delayNextPiecePlace, int delayMove, int delayDiscover, int delayTest, int delayPick, int delayPlace, int delayBeforeRemovingPawn, int delayRecalculate) {
        this.configuration_file = configuration_file;
        this.logger_file = logger_file;
        this.name_to_use = name_to_use;
        this.verbose = verbose;
        this.gm_port_number = gm_port_number;
        this.shamProbability = shamProbability;
        this.maxTeamSize = maxTeamSize;
        this.maxPieces = maxPieces;
        this.initialPieces = initialPieces;
        this.goalNumber = goalNumber;
        this.predefinedGoalPositions = predefinedGoalPositions;
        this.predefinedPiecesPositions = predefinedPiecesPositions;
        this.boardWidth = boardWidth;
        this.boardTaskHeight = boardTaskHeight;
        this.boardGoalHeight = boardGoalHeight;
        this.delayDestroyPiece = delayDestroyPiece;
        this.delayNextPiecePlace = delayNextPiecePlace;
        this.delayMove = delayMove;
        this.delayDiscover = delayDiscover;
        this.delayTest = delayTest;
        this.delayPick = delayPick;
        this.delayPlace = delayPlace;
        this.delayBeforeRemovingPawn = delayBeforeRemovingPawn;
        this.delayRecalculate = delayRecalculate;
    }

    @Override
    public String toString() {
        return "GameMasterConfiguration{" +
                "configuration_file='" + configuration_file + '\'' +
                ", logger_file='" + logger_file + '\'' +
                ", name_to_use='" + name_to_use + '\'' +
                ", verbose=" + verbose +
                ", shamProbability=" + shamProbability +
                ", maxTeamSize=" + maxTeamSize +
                ", maxPieces=" + maxPieces +
                ", initialPieces=" + initialPieces +
                ", goalNumber=" + goalNumber +
                ", predefinedGoalPositions=" + predefinedGoalPositions +
                ", predefinedPiecesPositions=" + predefinedPiecesPositions +
                ", boardWidth=" + boardWidth +
                ", boardTaskHeight=" + boardTaskHeight +
                ", boardGoalHeight=" + boardGoalHeight +
                ", delayDestroyPiece=" + delayDestroyPiece +
                ", delayNextPiecePlace=" + delayNextPiecePlace +
                ", delayMove=" + delayMove +
                ", delayDiscover=" + delayDiscover +
                ", delayTest=" + delayTest +
                ", delayPick=" + delayPick +
                ", delayPlace=" + delayPlace +
                ", delayBeforeRemovingPawn=" + delayBeforeRemovingPawn +
                ", delayRecalculate=" + delayRecalculate +
                ", gm_port_number=" + gm_port_number +
                '}';
    }
}
