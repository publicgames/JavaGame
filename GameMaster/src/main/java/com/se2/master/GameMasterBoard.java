package com.se2.master;

import com.se2.game.board.*;
import com.se2.game.board.enums.CellState;
import com.se2.game.board.enums.Direction;
import com.se2.game.team.enums.TeamColor;
import com.se2.tools.exceptions.BadConfigurationException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

public class GameMasterBoard extends Board {
    private final double shamProbability;
    private List<Position> piecesPosition;

    public GameMasterBoard(int width, int taskAreaHeight, int goalAreaHeight, double shamProbability) {
        super(width, taskAreaHeight, goalAreaHeight, false);
        this.shamProbability = shamProbability;
        this.piecesPosition = new ArrayList<>();
    }

    public GameMasterBoard(int width, int taskAreaHeight, int goalAreaHeight, List<Position> predefinedGoalPositions, double shamProbability) {
        this(width, taskAreaHeight, goalAreaHeight, shamProbability);

        for (Position goalPosition : predefinedGoalPositions) {
            if (isInTaskArea(goalPosition)) {
                throw new BadConfigurationException("Goal cannot be defined in the Task Area! X: " + goalPosition.getX() + " Y: " + goalPosition.getY());
            }

            updateCellWithCellState(goalPosition.getX(), goalPosition.getY(), CellState.UNDISCOVERED_GOAL);
        }
    }

    public GameMasterBoard(int width, int taskAreaHeight, int goalAreaHeight, List<Position> predefinedGoalPositions, int initialPieces, double shamProbability) {
        this(width, taskAreaHeight, goalAreaHeight, predefinedGoalPositions, shamProbability);
        this.piecesPosition = new ArrayList<>();

        if (initialPieces > taskAreaHeight * boardWidth) {
            throw new BadConfigurationException("Number of initial pieces cannot be grater than Task Area! Number of initial pieces: " + initialPieces);
        }
        initializePieces(initialPieces);
        calculateManhattanDistances();
    }

    public GameMasterBoard(int width, int taskAreaHeight, int goalAreaHeight, List<Position> predefinedGoalPositions, List<Position> initialPiecesPosition, double shamProbability) {
        this(width, taskAreaHeight, goalAreaHeight, predefinedGoalPositions, shamProbability);
        this.piecesPosition = new ArrayList<>();

        if (initialPiecesPosition.size() > taskAreaHeight * boardWidth) {
            throw new BadConfigurationException("Number of initial pieces cannot be grater than Task Area! Number of initial pieces: " + initialPiecesPosition.size());
        }
        initializePieces(initialPiecesPosition);
        calculateManhattanDistances();
    }

    public void calculateManhattanDistances() {
        for (int j = 0; j < this.cells.length; j++) {
            for (int i = 0; i < this.cells[j].length; i++) {
                int manhattanDistance = calculateManhattanDistance(new Position(i, j));
                updateCellWithDistance(i, j, manhattanDistance);
            }
        }
    }

    public boolean placePawn(Pawn pawn, Position position) {
        if (canBePlaced(position, pawn.getTeamColorForPawn())) {
            updateCellWithPawnGuid(position.getX(), position.getY(), pawn.getPlayerGuid());
            pawn.setPosition(position);
            return true;
        }
        return false;
    }

    public void removePawn(Pawn pawn) {
        Position position = pawn.getPosition();
        updateCellWithPawnGuid(position.getX(), position.getY(), null);
        pawn.setPosition(null);
    }

    public boolean placePawn(Pawn pawn) {
        Position position = getNextPositionToPlacePawn(pawn);
        return placePawn(pawn, position);
    }

    public boolean movePawn(Pawn pawn, Direction direction) {
        Position currentPosition = pawn.getPosition();
        Position newPosition = getNewPosition(currentPosition, direction);

        if (!canMove(newPosition, pawn.getTeamColorForPawn())) {
            return false;
        }

        updateCellWithPawnGuid(currentPosition.getX(), currentPosition.getY(), null);
        updateCellWithPawnGuid(newPosition.getX(), newPosition.getY(), pawn.getPlayerGuid());
        pawn.setPosition(newPosition);

        return true;
    }

    public boolean addPiece(Piece piece, Position position) {
        if (!canBePieceAdded(position)) {
            return false;
        }
        this.piecesPosition.add(position);
        updateCellWithPiece(position.getX(), position.getY(), piece);

        return true;
    }

    public boolean placePieceByPawn(Pawn pawn) {
        Position currentPosition = pawn.getPosition();
        int currentX = currentPosition.getX();
        int currentY = currentPosition.getY();

        Cell currentCell = this.getCell(currentX, currentY);
        Piece pawnPiece = pawn.getPiece();
        boolean placedCorrectly = false;

        if (pawnPiece != null && !pawnPiece.isSham()) {
            if (CellState.UNDISCOVERED_GOAL.equals(currentCell.getCellState())) {
                updateCellWithCellState(currentX, currentY, CellState.DISCOVERED_GOAL);
                placedCorrectly = true;
            } else if (CellState.DISCOVERED_GOAL.equals(currentCell.getCellState())) {
                //nothing to do
            } else if (!isInTaskArea(currentPosition)) {
                updateCellWithCellState(currentX, currentY, CellState.DISCOVERED_NON_GOAL);
            }
        }

        pawn.setPiece(null);

        return placedCorrectly;
    }

    public boolean takePiece(Pawn pawn) {
        Position position = pawn.getPosition();
        if (!canBePieceTaken(position) || pawn.getPiece() != null) {
            return false;
        }

        Piece piece = this.getCell(position.getX(), position.getY()).getPiece();

        this.piecesPosition.remove(position);
        pawn.setPiece(piece);
        updateCellWithPiece(position.getX(), position.getY(), null);
        return true;
    }

    public void clearPieces() {
        for (Position piecePosition : this.piecesPosition) {
            this.updateCellWithPiece(piecePosition.getX(), piecePosition.getY(), null);
        }
        this.piecesPosition.clear();
    }

    public void generatePieces(int maxPieces) {
        while (this.piecesPosition.size() < maxPieces) {
            generatePiece();
        }
    }

    public void generatePiece() {
        boolean generated = false;
        while (!generated) {
            Position randomPosition = getRandomPositionFromTaskArea();
            if (canBePieceAdded(randomPosition)) {
                Piece piece = new Piece(new Random().nextInt(100) < (this.shamProbability * 100));
                this.piecesPosition.add(randomPosition);
                updateCellWithPiece(randomPosition.getX(), randomPosition.getY(), piece);
                generated = true;
            }
        }
    }


    public List<Cell> discover(Position position) {
        if (!isInTaskArea(position)) {
            return null;
        }

        return getNeighbouringCells(position);
    }

    public int getNumberOfPieces() {
        return this.piecesPosition.size();
    }


    private void initializePieces(List<Position> piecesPosition) {
        for (Position piecePosition : piecesPosition) {
            if (isInTaskArea(piecePosition)) {
                this.piecesPosition.add(piecePosition);
                Piece piece = new Piece(new Random().nextInt(100) < (this.shamProbability * 100));
                updateCellWithPiece(piecePosition.getX(), piecePosition.getY(), piece);
            }
        }
    }

    private void initializePieces(int initialPieces) {
        int i = 0;
        while (i < initialPieces) {
            int pieceX = new Random().nextInt(this.boardWidth);
            int pieceY = new Random().nextInt(this.taskAreaHeight) + this.goalAreaHeight;
            if (this.getCell(pieceX, pieceY).getCellState() != CellState.PIECE) {
                this.piecesPosition.add(new Position(pieceX, pieceY));
                Piece piece = new Piece(new Random().nextInt(100) < (this.shamProbability * 100));
                updateCellWithPiece(pieceX, pieceY, piece);
                i++;
            }

        }
    }

    private int calculateManhattanDistance(Position position) {
        Optional<Position> piecePosition = findClosestPositionWithPiece(position);
        int manhattanDistance = -1;
        if (piecePosition.isPresent()) {
            manhattanDistance = Math.abs(piecePosition.get().getX() - position.getX()) +
                    Math.abs(piecePosition.get().getY() - position.getY());
        }

        return manhattanDistance;
    }


    private Position getRandomPositionFromTaskArea() {
        Random random = new Random();
        int x = random.nextInt(this.boardWidth);
        int y = random.nextInt(this.taskAreaHeight) + this.goalAreaHeight;

        return new Position(x, y);
    }


    private Optional<Position> findClosestPositionWithPiece(Position position) {
        Position closestPiecePosition = null;
        int currentDistance = Integer.MAX_VALUE;
        if (this.piecesPosition.size() != 0) {
            for (Position piecePosition : this.piecesPosition) {
                int diffX = Math.abs(position.getX() - piecePosition.getX());
                int diffY = Math.abs(position.getY() - piecePosition.getY());
                int total = diffX + diffY;
                if (total < currentDistance) {
                    closestPiecePosition = new Position(piecePosition.getX(), piecePosition.getY());
                    currentDistance = total;
                }
            }
        }

        if (closestPiecePosition == null || (closestPiecePosition.getX() == boardWidth && closestPiecePosition.getY() == boardHeight)) {
            return Optional.empty();
        } else {
            return Optional.of(closestPiecePosition);
        }
    }


    private boolean canBePlaced(Position position, TeamColor pawnTeamColor) {
        if (!isInBounds(position)) {
            return false;
        }

        if (isInOtherTeamGoalArea(position, pawnTeamColor)) {
            return false;
        }

        Cell cell = this.getCell(position.getX(), position.getY());

        return cell.getPawnGuid() == null;
    }

    private boolean canMove(Position newPosition, TeamColor pawnTeamColor) {
        int newX = newPosition.getX();
        int newY = newPosition.getY();

        if (!isInBounds(newPosition)) { //check bounds
            return false;
        }

        Cell newCell = this.getCell(newX, newY);

        if (newCell.getPawnGuid() != null) { //check if cell is not occupied
            return false;
        }

        return !isInOtherTeamGoalArea(newPosition, pawnTeamColor);
    }

    private boolean canBePieceAdded(Position position) {
        if (!isInTaskArea(position)) {
            return false;
        }

        Cell cell = this.getCell(position.getX(), position.getY());
        return cell.getPiece() == null && cell.getCellState() == CellState.EMPTY;
    }

    private boolean canBePieceTaken(Position position) {
        if (!isInTaskArea(position)) {
            return false;
        }

        Cell cell = this.getCell(position.getX(), position.getY());

        return cell.getPiece() != null && cell.getCellState() == CellState.PIECE;
    }


    private void updateCellWithDistance(int x, int y, int distance) {
        Cell cell = this.getCell(x, y);
        cell.setDistance(distance);
    }

    private void updateCellWithCellState(int x, int y, CellState cellState) {
        Cell cell = this.getCell(x, y);
        cell.setCellState(cellState);
    }

    private void updateCellWithPawnGuid(int x, int y, String pawnGuid) {
        Cell cell = this.getCell(x, y);
        cell.setPawnGuid(pawnGuid);
    }

    private void updateCellWithPiece(int x, int y, Piece piece) {
        Cell cell = this.getCell(x, y);
        if (piece != null) {
            cell.setPiece(piece);
            cell.setCellState(CellState.PIECE);
        } else {
            cell.setPiece(null);
            cell.setCellState(CellState.EMPTY);
        }
    }


    private Position getNewPosition(Position currentPosition, Direction direction) {
        int newX = currentPosition.getX();
        int newY = currentPosition.getY();

        if (direction == Direction.UP) {
            newY--;
        } else if (direction == Direction.DOWN) {
            newY++;
        } else if (direction == Direction.RIGHT) {
            newX++;
        } else {
            newX--;
        }

        return new Position(newX, newY);
    }

    private synchronized Position getNextPositionToPlacePawn(Pawn pawn) {
        Position output = null;
        if (TeamColor.BLUE.equals(pawn.getTeamColorForPawn())) {
            //from top left - move right - then to next line from start (left)... and so on
            output = new Position(blueInitialPosition.getX(), blueInitialPosition.getY());
            Position blue = new Position(blueInitialPosition.getX() + 1, blueInitialPosition.getY());
            if (isInBounds(blue)) {
                blueInitialPosition = blue;
            } else {
                blueInitialPosition = new Position(0, blueInitialPosition.getY() + 1);
            }
        } else {
            //from bottom right - move left - then to previous line from end (right)... and so on
            output = new Position(redInitialPosition.getX(), redInitialPosition.getY());
            Position red = new Position(redInitialPosition.getX() - 1, redInitialPosition.getY());
            if (isInBounds(red)) {
                redInitialPosition = red;
            } else {
                redInitialPosition = new Position(boardWidth - 1, redInitialPosition.getY() - 1);
            }
        }
        if (!isInBounds(output)) {
            throw new BadConfigurationException("Run out of space for new players");
        }
        return output;
    }
}
