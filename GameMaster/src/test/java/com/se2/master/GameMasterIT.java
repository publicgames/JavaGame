package com.se2.master;

import com.google.gson.Gson;
import com.se2.communication.messages.ConnectPlayerMessage;
import com.se2.game.board.Position;
import com.se2.master.configuration.GameMasterConfiguration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.se2.tools.PointsUtils.getListOfPointsFromString;

class GameMasterIT {

    //    private static String gameMasterUUID = "10000000-0000-0000-0000-000000000000";
    private static String playerUUID01 = "00000000-0000-0000-0000-000000000001";
    private static String playerUUID02 = "00000000-0000-0000-0000-000000000002";
    private static String playerUUID03 = "00000000-0000-0000-0000-000000000003";
    private static String playerUUID04 = "00000000-0000-0000-0000-000000000004";
    private static int playerPortToConnectTo = 1111;
    private static Position playerPosition01 = new Position(1, 2);
    private static Position playerPosition02 = new Position(3, 2);
    //class to test
    private GameMaster gameMaster;
    //real classes
    private GameMasterConfiguration gameMasterConfiguration;
    private GameMasterBoard gameMasterBoard; //Board is set up as 5x5

    @BeforeEach
    public void setUp() {
        String PREDEFINED_GOAL_POSITIONS = "1,1:2,1:3,2:4,1:4,2:0,8:1,9:2,9:3,9:4,9";
        String INITIAL_PIECES_POSITIONS = "0,4:4,6";
        gameMasterConfiguration = new GameMasterConfiguration(
                "DefaultFile",
                "log.out",
                "DefaultCommunicationServer",
//                gameMasterUUID,
                true,
                7777,
                0.23,
                2,
                3,
                1,
                5,
                getListOfPointsFromString(PREDEFINED_GOAL_POSITIONS),
                getListOfPointsFromString(INITIAL_PIECES_POSITIONS),
                5,
                5,
                3,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                90000,
                3000);

        gameMaster = new GameMaster(gameMasterConfiguration);
    }

    private String getConnectPlayerMessageAsJsonFor(String playerUUID01, int playerPort01) {
        Gson gson = new Gson();
        ConnectPlayerMessage gameSetupin = new ConnectPlayerMessage(playerUUID01, playerPort01);
        return gson.toJson(gameSetupin, ConnectPlayerMessage.class);
    }

    //Integration test from initialization to sending all players GameStartMessages
    @Test
    public void fromInitializationTillGameStartMessagesTest() {
        //start GM server.
        //FIXME
        //send messages to it

    }
}