package com.se2.master;

import com.se2.communication.classes.MessageBoard;
import com.se2.communication.classes.MessageFields;
import com.se2.communication.classes.MessagePosition;
import com.se2.communication.enums.DirectionMessageEnum;
import com.se2.communication.enums.PlacementResultEnum;
import com.se2.communication.enums.StatusMessageEnum;
import com.se2.communication.enums.TeamColorMessageEnum;
import com.se2.communication.messages.*;
import com.se2.game.board.Piece;
import com.se2.game.board.Position;
import com.se2.game.player.enums.PlayerState;
import com.se2.master.configuration.GameMasterConfiguration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.se2.tools.ConvertersUtils.convertCellsToMessageFields;
import static com.se2.tools.ConvertersUtils.convertPositionToMessagePosition;
import static com.se2.tools.PointsUtils.getListOfPointsFromString;
import static org.junit.jupiter.api.Assertions.*;

class GameMasterTest {

    //    private static String gameMasterUUID = "10000000-0000-0000-0000-000000000000";
    private static int playerPort = 7777;
    private static String playerUUID01RED = "00000000-0000-0000-0000-000000000001";
    private static String playerUUID02BLUE = "00000000-0000-0000-0000-000000000002";
    private static String playerUUID03 = "00000000-0000-0000-0000-000000000003";
    private static String playerUUID04 = "00000000-0000-0000-0000-000000000004";
    private static String playerUUID05 = "00000000-0000-0000-0000-000000000005";
    private static Position player01PositionRED = new Position(4, 10);
    private static Position player02PositionBLUE = new Position(0, 0);

    //configuration
    private GameMasterConfiguration gameMasterConfiguration;
    //class to test
    private GameMaster gameMaster;

    @BeforeEach
    public void setUp() {
        String PREDEFINED_GOAL_POSITIONS = "1,1:2,1:3,2:4,1:4,2:0,8:1,9:2,9:3,9:4,9";
        String INITIAL_PIECES_POSITIONS = "0,4:4,6";
        gameMasterConfiguration = new GameMasterConfiguration(
                "DefaultFile",
                "log.out",
                "DefaultCommunicationServer",
                true,
                7777,
                0.23,
                2,
                3,
                1,
                5,
                getListOfPointsFromString(PREDEFINED_GOAL_POSITIONS),
                getListOfPointsFromString(INITIAL_PIECES_POSITIONS),
                5,
                5,
                3,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                90000,
                3000);

        gameMaster = new GameMaster(gameMasterConfiguration);
        //STATUS
        /*
        +-----+
        |B    |
        | GG G|
        |   GG|
        +-----+
        |     |
        |P    |
        |     |
        |    P|
        |     |
        +-----+
        |G    |
        | GGGG|
        |    R|
        +-----+
        */
    }

    private void add2Players() {
        gameMaster.waitForPlayers();
        gameMaster.addPawn(playerUUID01RED); //added first so RED
        gameMaster.addPawn(playerUUID02BLUE);
        gameMaster.getPawn(playerUUID01RED).setPlayerState(PlayerState.Connected);
        gameMaster.getPawn(playerUUID02BLUE).setPlayerState(PlayerState.Connected);
    }

    /**
     * Use Case 1: As GameMaster I want to process players connect messages and respond to them, so that players can join game
     * subcase01 - there is place for new player
     */
    @Test
    public void connectPlayerMessage01Test() {
        add2Players();
        ConnectPlayerMessage received = new ConnectPlayerMessage(playerUUID03, playerPort);
        ConnectPlayerReturnMessage expected = new ConnectPlayerReturnMessage(playerUUID03, playerPort, StatusMessageEnum.OK);
        assertEquals(expected, gameMaster.visit(received));
    }

    /**
     * Use Case 1: As GameMaster I want to process players connect messages and respond to them, so that players can join game
     * subcase02 - all team are full
     */
    @Test
    public void connectPlayerMessage02Test() {
        add2Players();
        //setup
        gameMaster.visit(new ConnectPlayerMessage(playerUUID03, playerPort));
        gameMaster.visit(new ConnectPlayerMessage(playerUUID04, playerPort));
        //test
        ConnectPlayerMessage received = new ConnectPlayerMessage(playerUUID05, playerPort);
        ConnectPlayerReturnMessage expected = new ConnectPlayerReturnMessage(playerUUID05, playerPort, StatusMessageEnum.DENIED);
        assertEquals(expected, gameMaster.visit(received));
    }

    /**
     * Use Case 1: As GameMaster I want to process players connect messages and respond to them, so that players can join game
     * subcase03 - used already connected
     */
    @Test
    public void connectPlayerMessage03Test() {
        add2Players();
        //setup
        gameMaster.visit(new ConnectPlayerMessage(playerUUID03, playerPort));
        //test
        ConnectPlayerMessage received = new ConnectPlayerMessage(playerUUID03, playerPort);
        ConnectPlayerReturnMessage expected = new ConnectPlayerReturnMessage(playerUUID03, playerPort, StatusMessageEnum.DENIED);
        assertEquals(expected, gameMaster.visit(received));
    }

    /**
     * Use Case 1: As GameMaster I want to process players connect messages and respond to them, so that players can join game
     * subcase04 - Gamemaster received ConnectPlayerReturnMessage
     */
    @Test
    public void connectPlayerMessage04Test() {
        add2Players();
        //test
        ConnectPlayerReturnMessage received = new ConnectPlayerReturnMessage(playerUUID03, playerPort, StatusMessageEnum.OK);
        assertNull(gameMaster.visit(received));
    }

    /**
     * Use Case 1: As GameMaster I want to process players connect messages and respond to them, so that players can join game
     * subcase05 - gameStarted, players will not be able to connect
     */
    @Test
    public void connectPlayerMessage05Test() {
        add2Players();
        //setup
        gameMaster.startGame();
        //test
        ConnectPlayerMessage received = new ConnectPlayerMessage(playerUUID03, playerPort);
        ConnectPlayerReturnMessage expected = new ConnectPlayerReturnMessage(playerUUID03, playerPort, StatusMessageEnum.DENIED);
        assertEquals(expected, gameMaster.visit(received));
    }

    /**
     * Use Case 1: As GameMaster I want to process players connect messages and respond to them, so that players can join game
     * subcase06 - GameOver, players will not be able to connect
     */
    @Test
    public void connectPlayerMessage06Test() {
        add2Players();
        //setup
        gameMaster.startGame();
        gameMaster.gameOver();
        //test
        ConnectPlayerMessage received = new ConnectPlayerMessage(playerUUID03, playerPort);
        ConnectPlayerReturnMessage expected = new ConnectPlayerReturnMessage(playerUUID03, playerPort, StatusMessageEnum.DENIED);
        assertEquals(expected, gameMaster.visit(received));
    }

    /**
     * Use Case 2: As GameMaster I want to process wrong messages ConnectPlayerReturnMessage and respond to them, so that I will not get stuck with it
     * subcase01 - GM gets ConnectPlayerReturnMessage and returns null
     */
    @Test
    public void dealWithConnectPlayerReturnMessageTest() {
        add2Players();
        ConnectPlayerReturnMessage received = new ConnectPlayerReturnMessage(playerUUID03, playerPort, StatusMessageEnum.DENIED);
        assertNull(gameMaster.visit(received));
    }

    /**
     * Use Case 5: As GameMaster I want to process players DiscoverMessage and respond to them, so that players can see surroundings
     * subcase01 - GM is not in GAME_STARTED state, respond with DENIED
     */
    @Test
    public void dealWithDiscoverMessage01Test() {
        add2Players();
        MessagePosition messagePositionOfPlayer1 = new MessagePosition(player02PositionBLUE.getX(), player02PositionBLUE.getY());
        DiscoverMessage received = new DiscoverMessage(playerUUID01RED, messagePositionOfPlayer1);
        assertNull(gameMaster.visit(received));
        gameMaster.startGame();
        gameMaster.gameOver();
        assertNull(gameMaster.visit(received));
    }

    /**
     * Use Case 5: As GameMaster I want to process players DiscoverMessage and respond to them, so that players can see surroundings
     * subcase02 - GM is in GAME_STARTED state, respond with OK
     */
    @Test
    public void dealWithDiscoverMessage02Test() {
        add2Players();
        gameMaster.startGame();
        DirectionMessageEnum direction = DirectionMessageEnum.Down;
        MoveMessage move = new MoveMessage(playerUUID02BLUE, direction);
        gameMaster.visit(move);
        gameMaster.visit(move);
        gameMaster.visit(move);

        Position newPosition = new Position(player02PositionBLUE.getX(), player02PositionBLUE.getY() + 3);
        MessagePosition messagePositionOfPlayer1 = convertPositionToMessagePosition(newPosition);
        DiscoverMessage received = new DiscoverMessage(playerUUID02BLUE, messagePositionOfPlayer1);

        List<MessageFields> fieldsToSend = convertCellsToMessageFields(gameMaster.getBoard().getNeighbouringCells(newPosition));
        DiscoverMessageReturn expected = new DiscoverMessageReturn(playerUUID02BLUE, messagePositionOfPlayer1, StatusMessageEnum.OK, fieldsToSend);
        assertEquals(expected, gameMaster.visit(received));
    }

    /**
     * Use Case 5: As GameMaster I want to process players DiscoverMessage and respond to them, so that players can see surroundings
     * subcase03 - GM is in GAME_STARTED state, respond with DENIED - player notin task area
     */
    @Test
    public void dealWithDiscoverMessage03Test() {
        add2Players();
        gameMaster.startGame();

        MessagePosition messagePositionOfPlayer1 = new MessagePosition(player02PositionBLUE.getX(), player02PositionBLUE.getY());
        DiscoverMessage received = new DiscoverMessage(playerUUID02BLUE, messagePositionOfPlayer1);

        DiscoverMessageReturn expected = new DiscoverMessageReturn(playerUUID02BLUE, messagePositionOfPlayer1, StatusMessageEnum.DENIED, new ArrayList<>());
        assertEquals(expected, gameMaster.visit(received));
    }

    /**
     * Use Case 6: As GameMaster I want to process players DiscoverMessageReturn and not get stuck on them
     * subcase01 - GM gets DiscoverMessageReturn and returns null
     */
    @Test
    public void dealWithDiscoverMessageReturn02Test() {
        add2Players();
        MessagePosition messagePositionOfPlayer1 = new MessagePosition(player02PositionBLUE.getX(), player02PositionBLUE.getY());
        DiscoverMessageReturn received = new DiscoverMessageReturn(playerUUID01RED, messagePositionOfPlayer1, StatusMessageEnum.OK, new ArrayList<>());
        assertNull(gameMaster.visit(received));
        gameMaster.startGame();
        assertNull(gameMaster.visit(received));
        gameMaster.gameOver();
        assertNull(gameMaster.visit(received));
    }

    /**
     * Use Case 6: As GameMaster I want to process players EndMessage and not get stuck on them
     * subcase01 - GM gets EndMessage and returns null
     */
    @Test
    public void dealWithEndMessage01Test() {
        add2Players();
        EndMessage received = new EndMessage(TeamColorMessageEnum.Blue);
        assertNull(gameMaster.visit(received));
        gameMaster.startGame();
        assertNull(gameMaster.visit(received));
        gameMaster.gameOver();
        assertNull(gameMaster.visit(received));
    }

    /**
     * Use Case 6: As GameMaster I want to process players GameStartMessage and not get stuck on them
     * subcase03 - GM gets GameStartMessage and returns null
     */
    @Test
    public void dealWithGameStartMessage01Test() {
        add2Players();
        GameStartMessage received = prepareStartMessage();

        assertNull(gameMaster.visit(received));
        gameMaster.startGame();
        assertNull(gameMaster.visit(received));
        gameMaster.gameOver();
        assertNull(gameMaster.visit(received));
    }

    /**
     * Use Case 7: As GameMaster I want to process players MoveMessage so that players will be able to move
     * subcase01 - GM gets MoveMessage and returns null as not in proper state
     */
    @Test
    public void dealWithMoveMessage01Test() {
        add2Players();
        DirectionMessageEnum direction = DirectionMessageEnum.Right;
        MoveMessage received = new MoveMessage(playerUUID02BLUE, direction);
        assertNull(gameMaster.visit(received));
        gameMaster.startGame();
        gameMaster.gameOver();
        assertNull(gameMaster.visit(received));
    }

    /**
     * Use Case 7: As GameMaster I want to process players MoveMessage so that players will be able to move
     * subcase02 - GM gets MoveMessage and returns MoveMessageReturn with denied when move not possible
     */
    @Test
    public void dealWithMoveMessage02Test() {
        add2Players();
        DirectionMessageEnum direction = DirectionMessageEnum.Left;
        MoveMessage received = new MoveMessage(playerUUID02BLUE, direction);
        MessagePosition messagePositionOfPlayer1 = new MessagePosition(player02PositionBLUE.getX(), player02PositionBLUE.getY());
        MoveMessageReturn expected = new MoveMessageReturn(StatusMessageEnum.DENIED, playerUUID02BLUE, direction, messagePositionOfPlayer1);
        assertNull(gameMaster.visit(received));
        gameMaster.startGame();
        assertEquals(expected, gameMaster.visit(received));
        gameMaster.gameOver();
        assertNull(gameMaster.visit(received));
    }

    /**
     * Use Case 7: As GameMaster I want to process players MoveMessage so that players will be able to move
     * subcase03 - GM gets MoveMessage and returns MoveMessageReturn with ok when move possible
     */
    @Test
    public void dealWithMoveMessage03Test() {
        add2Players();
        DirectionMessageEnum direction = DirectionMessageEnum.Down;
        MoveMessage received = new MoveMessage(playerUUID02BLUE, direction);
        MessagePosition messagePositionOfPlayer1 = new MessagePosition(player02PositionBLUE.getX(), player02PositionBLUE.getY() + 1);
        MoveMessageReturn expected = new MoveMessageReturn(StatusMessageEnum.OK, playerUUID02BLUE, direction, messagePositionOfPlayer1);
        assertNull(gameMaster.visit(received));
        gameMaster.startGame();
        assertEquals(expected, gameMaster.visit(received));
        gameMaster.gameOver();
        assertNull(gameMaster.visit(received));
    }

    /**
     * Use Case 8: As GameMaster I want to process players PickUpMessage so that players will be able to pickup
     * subcase01 - GM gets PickUpMessage and returns PickUpMessageReturn with denied when pickup not possible and with ok when its possible
     * or null when it should not reply as in not proper state
     */
    @Test
    public void dealWithPickUpMessage01Test() {
        add2Players();
        PickUpMessage received = new PickUpMessage(playerUUID02BLUE);
        assertNull(gameMaster.visit(received));
        gameMaster.startGame();
        PickUpMessageReturn expected = new PickUpMessageReturn(StatusMessageEnum.DENIED, playerUUID02BLUE);
        assertEquals(expected, gameMaster.visit(received));

        DirectionMessageEnum direction = DirectionMessageEnum.Down;
        MoveMessage move = new MoveMessage(playerUUID02BLUE, direction);
        gameMaster.visit(move);
        gameMaster.visit(move);
        gameMaster.visit(move);
        gameMaster.visit(move);

        PickUpMessageReturn expectedWhenOverPiece = new PickUpMessageReturn(StatusMessageEnum.OK, playerUUID02BLUE);
        assertEquals(expectedWhenOverPiece, gameMaster.visit(received));

        gameMaster.gameOver();
        assertNull(gameMaster.visit(received));
    }

    /**
     * Use Case 9: As GameMaster I want to process players PlaceMessage so that players will be able to place piece
     * <p>
     * Basing on:
     * Page 29.
     * • If the piece is brought to the Goals area by the Player, he/she needs to
     * place the piece in the right field. If the Player places a piece correctly,
     * he/she will be notified that one of the goals have been completed.
     * • If the Piece was placed incorrectly, the Player will be notified that they
     * discovered a non-goal.
     * • If the Piece turns out to be sham, the player gets no information.
     * <p>
     * Page 17
     * "PlaceStatus message"
     * 57 " i f ’ status ’ is DENIED, the ’placementResult ’ is null "
     * <p>
     * <p>
     * subcase01 - GM gets PlaceMessage and returns null as GM not in GAME_STARTED
     * and return PickUpMessageReturn Denied when player did not had piece
     */
    @Test
    public void dealWithPlaceMessage01Test() {
        add2Players();
        PlaceMessage received = new PlaceMessage(playerUUID02BLUE);
        assertNull(gameMaster.visit(received));
        gameMaster.startGame();
        PlaceMessageReturn expected = new PlaceMessageReturn(StatusMessageEnum.DENIED, playerUUID02BLUE, null);
        assertEquals(expected, gameMaster.visit(received));
        gameMaster.gameOver();
        assertNull(gameMaster.visit(received));
    }

    /**
     * Use Case 9
     * subcase02 - GM gets PlaceMessage and returns OK/Correct as player stands over goal with real piece
     */
    @Test
    public void dealWithPlaceMessage02Test() {
        add2Players();
        Piece pieceThatWillBePickedUp = new Piece(false);
        gameMaster.getBoard().getCell(0, 4).setPiece(pieceThatWillBePickedUp);
        gameMaster.startGame();

        DirectionMessageEnum direction = DirectionMessageEnum.Down;
        MoveMessage move = new MoveMessage(playerUUID02BLUE, direction);
        gameMaster.visit(move);
        gameMaster.visit(move);
        gameMaster.visit(move);
        gameMaster.visit(move);

        assertNull(gameMaster.getPawn(playerUUID02BLUE).getPiece());
        gameMaster.visit(new PickUpMessage(playerUUID02BLUE));
        assertNotNull(gameMaster.getPawn(playerUUID02BLUE).getPiece());

        direction = DirectionMessageEnum.Up;
        move = new MoveMessage(playerUUID02BLUE, direction);
        gameMaster.visit(move);
        gameMaster.visit(move);
        gameMaster.visit(move);

        direction = DirectionMessageEnum.Right;
        move = new MoveMessage(playerUUID02BLUE, direction);
        gameMaster.visit(move);

        PlaceMessage received = new PlaceMessage(playerUUID02BLUE);
        PlaceMessageReturn expected = new PlaceMessageReturn(StatusMessageEnum.OK, playerUUID02BLUE, PlacementResultEnum.Correct);
        assertEquals(expected, gameMaster.visit(received));
    }

    /**
     * Use Case 9
     * subcase03 - GM gets PlaceMessage and returns OK/pointless as player stands over non-goal with real piece
     */
    @Test
    public void dealWithPlaceMessage03Test() {
        add2Players();
        Piece pieceThatWillBePickedUp = new Piece(false);
        gameMaster.getBoard().getCell(0, 4).setPiece(pieceThatWillBePickedUp);
        gameMaster.startGame();

        DirectionMessageEnum direction = DirectionMessageEnum.Down;
        MoveMessage move = new MoveMessage(playerUUID02BLUE, direction);
        gameMaster.visit(move);
        gameMaster.visit(move);
        gameMaster.visit(move);
        gameMaster.visit(move);

        assertNull(gameMaster.getPawn(playerUUID02BLUE).getPiece());
        gameMaster.visit(new PickUpMessage(playerUUID02BLUE));
        assertNotNull(gameMaster.getPawn(playerUUID02BLUE).getPiece());

        direction = DirectionMessageEnum.Up;
        move = new MoveMessage(playerUUID02BLUE, direction);
        gameMaster.visit(move);
        gameMaster.visit(move);
        gameMaster.visit(move);

        PlaceMessage received = new PlaceMessage(playerUUID02BLUE);
        PlaceMessageReturn expected = new PlaceMessageReturn(StatusMessageEnum.OK, playerUUID02BLUE, PlacementResultEnum.Pointless);
        assertEquals(expected, gameMaster.visit(received));
    }

    /**
     * Use Case 9
     * subcase04 - GM gets PlaceMessage and returns OK/pointless as player stands in target area with real piece
     */
    @Test
    public void dealWithPlaceMessage04Test() {
        add2Players();
        Piece pieceThatWillBePickedUp = new Piece(false);
        gameMaster.getBoard().getCell(0, 4).setPiece(pieceThatWillBePickedUp);
        gameMaster.startGame();

        DirectionMessageEnum direction = DirectionMessageEnum.Down;
        MoveMessage move = new MoveMessage(playerUUID02BLUE, direction);
        gameMaster.visit(move);
        gameMaster.visit(move);
        gameMaster.visit(move);
        gameMaster.visit(move);

        assertNull(gameMaster.getPawn(playerUUID02BLUE).getPiece());
        gameMaster.visit(new PickUpMessage(playerUUID02BLUE));
        assertNotNull(gameMaster.getPawn(playerUUID02BLUE).getPiece());

        PlaceMessage received = new PlaceMessage(playerUUID02BLUE);
        PlaceMessageReturn expected = new PlaceMessageReturn(StatusMessageEnum.OK, playerUUID02BLUE, PlacementResultEnum.Pointless);
        assertEquals(expected, gameMaster.visit(received));
    }

    /**
     * Use Case 9
     * subcase05 - GM gets PlaceMessage and returns null as player stands over goal with sham piece
     */
    @Test
    public void dealWithPlaceMessage05Test() {
        add2Players();
        Piece pieceThatWillBePickedUp = new Piece(true);
        gameMaster.getBoard().getCell(0, 4).setPiece(pieceThatWillBePickedUp);
        gameMaster.startGame();

        DirectionMessageEnum direction = DirectionMessageEnum.Down;
        MoveMessage move = new MoveMessage(playerUUID02BLUE, direction);
        gameMaster.visit(move);
        gameMaster.visit(move);
        gameMaster.visit(move);
        gameMaster.visit(move);

        assertNull(gameMaster.getPawn(playerUUID02BLUE).getPiece());
        gameMaster.visit(new PickUpMessage(playerUUID02BLUE));
        assertNotNull(gameMaster.getPawn(playerUUID02BLUE).getPiece());

        direction = DirectionMessageEnum.Up;
        move = new MoveMessage(playerUUID02BLUE, direction);
        gameMaster.visit(move);
        gameMaster.visit(move);
        gameMaster.visit(move);

        direction = DirectionMessageEnum.Right;
        move = new MoveMessage(playerUUID02BLUE, direction);
        gameMaster.visit(move);

        PlaceMessage received = new PlaceMessage(playerUUID02BLUE);
        assertNull(gameMaster.visit(received));
    }

    /**
     * Use Case 9
     * subcase06 - GM gets PlaceMessage and returns null as player stands over non-goal with sham piece
     */
    @Test
    public void dealWithPlaceMessage06Test() {
        add2Players();
        Piece pieceThatWillBePickedUp = new Piece(true);
        gameMaster.getBoard().getCell(0, 4).setPiece(pieceThatWillBePickedUp);
        gameMaster.startGame();

        DirectionMessageEnum direction = DirectionMessageEnum.Down;
        MoveMessage move = new MoveMessage(playerUUID02BLUE, direction);
        gameMaster.visit(move);
        gameMaster.visit(move);
        gameMaster.visit(move);
        gameMaster.visit(move);

        assertNull(gameMaster.getPawn(playerUUID02BLUE).getPiece());
        gameMaster.visit(new PickUpMessage(playerUUID02BLUE));
        assertNotNull(gameMaster.getPawn(playerUUID02BLUE).getPiece());

        direction = DirectionMessageEnum.Up;
        move = new MoveMessage(playerUUID02BLUE, direction);
        gameMaster.visit(move);
        gameMaster.visit(move);
        gameMaster.visit(move);

        PlaceMessage received = new PlaceMessage(playerUUID02BLUE);
        assertNull(gameMaster.visit(received));
    }

    /**
     * Use Case 9
     * subcase06 - GM gets PlaceMessage and returns null as player in target area with sham piece
     */
    @Test
    public void dealWithPlaceMessage07Test() {
        add2Players();
        Piece pieceThatWillBePickedUp = new Piece(true);
        gameMaster.getBoard().getCell(0, 4).setPiece(pieceThatWillBePickedUp);
        gameMaster.startGame();

        DirectionMessageEnum direction = DirectionMessageEnum.Down;
        MoveMessage move = new MoveMessage(playerUUID02BLUE, direction);
        gameMaster.visit(move);
        gameMaster.visit(move);
        gameMaster.visit(move);
        gameMaster.visit(move);

        assertNull(gameMaster.getPawn(playerUUID02BLUE).getPiece());
        gameMaster.visit(new PickUpMessage(playerUUID02BLUE));
        assertNotNull(gameMaster.getPawn(playerUUID02BLUE).getPiece());

        PlaceMessage received = new PlaceMessage(playerUUID02BLUE);
        assertNull(gameMaster.visit(received));
    }

    /**
     * Use Case 10: As GameMaster I want to process MoveMessageReturn and not get stuck on them
     * subcase01 - GM gets MoveMessageReturn and returns null
     */
    @Test
    public void dealWithMoveMessageReturn01Test() {
        add2Players();
        DirectionMessageEnum direction = DirectionMessageEnum.Right;
        MessagePosition messagePositionOfPlayer1 = new MessagePosition(player02PositionBLUE.getX(), player02PositionBLUE.getY());
        MoveMessageReturn received = new MoveMessageReturn(StatusMessageEnum.OK, playerUUID02BLUE, direction, messagePositionOfPlayer1);

        assertNull(gameMaster.visit(received));
        gameMaster.startGame();
        assertNull(gameMaster.visit(received));
        gameMaster.gameOver();
        assertNull(gameMaster.visit(received));
    }

    /**
     * Use Case 10: As GameMaster I want to process PickUpMessageReturn and not get stuck on them
     * subcase01 - GM gets PickUpMessageReturn and returns null
     */
    @Test
    public void dealWithPickUpMessageReturn01Test() {
        add2Players();
        PickUpMessageReturn received = new PickUpMessageReturn(StatusMessageEnum.OK, playerUUID02BLUE);

        assertNull(gameMaster.visit(received));
        gameMaster.startGame();
        assertNull(gameMaster.visit(received));
        gameMaster.gameOver();
        assertNull(gameMaster.visit(received));
    }

    /**
     * Use Case 11: As GameMaster I want to process PlaceMessageReturn and not get stuck on them
     * subcase01 - GM gets PlaceMessageReturn and returns null
     */
    @Test
    public void dealWithPlaceMessageReturn01Test() {
        add2Players();
        PlaceMessageReturn received = new PlaceMessageReturn(StatusMessageEnum.OK, playerUUID02BLUE, PlacementResultEnum.Pointless);

        assertNull(gameMaster.visit(received));
        gameMaster.startGame();
        assertNull(gameMaster.visit(received));
        gameMaster.gameOver();
        assertNull(gameMaster.visit(received));
    }

    /**
     * Use Case 14
     * subcase01 - GM gets TestMessage and returns OK/true as player tests real piece
     */
    @Test
    public void dealWithTestMessage01Test() {
        add2Players();
        Piece pieceThatWillBePickedUp = new Piece(false);
        gameMaster.getBoard().getCell(0, 4).setPiece(pieceThatWillBePickedUp);
        gameMaster.startGame();

        DirectionMessageEnum direction = DirectionMessageEnum.Down;
        MoveMessage move = new MoveMessage(playerUUID02BLUE, direction);
        gameMaster.visit(move);
        gameMaster.visit(move);
        gameMaster.visit(move);
        gameMaster.visit(move);

        assertNull(gameMaster.getPawn(playerUUID02BLUE).getPiece());
        gameMaster.visit(new PickUpMessage(playerUUID02BLUE));
        assertNotNull(gameMaster.getPawn(playerUUID02BLUE).getPiece());

        TestMessage received = new TestMessage(playerUUID02BLUE);
        TestMessageReturn expected = new TestMessageReturn(StatusMessageEnum.OK, playerUUID02BLUE, true);
        assertEquals(expected, gameMaster.visit(received));
    }

    /**
     * Use Case 14
     * subcase02 - GM gets TestMessage and returns OK/false as player tests sham piece
     */
    @Test
    public void dealWithTestMessage02Test() {
        add2Players();
        Piece pieceThatWillBePickedUp = new Piece(true);
        gameMaster.getBoard().getCell(0, 4).setPiece(pieceThatWillBePickedUp);
        gameMaster.startGame();

        DirectionMessageEnum direction = DirectionMessageEnum.Down;
        MoveMessage move = new MoveMessage(playerUUID02BLUE, direction);
        gameMaster.visit(move);
        gameMaster.visit(move);
        gameMaster.visit(move);
        gameMaster.visit(move);

        assertNull(gameMaster.getPawn(playerUUID02BLUE).getPiece());
        gameMaster.visit(new PickUpMessage(playerUUID02BLUE));
        assertNotNull(gameMaster.getPawn(playerUUID02BLUE).getPiece());

        TestMessage received = new TestMessage(playerUUID02BLUE);
        TestMessageReturn expected = new TestMessageReturn(StatusMessageEnum.OK, playerUUID02BLUE, false);
        assertEquals(expected, gameMaster.visit(received));
    }

    /**
     * Use Case 14
     * subcase03 - GM gets TestMessage and returns DENIED/false as player tests without having a piece
     */
    @Test
    public void dealWithTestMessage03Test() {
        add2Players();
        gameMaster.startGame();

        TestMessage received = new TestMessage(playerUUID02BLUE);
        TestMessageReturn expected = new TestMessageReturn(StatusMessageEnum.DENIED, playerUUID02BLUE, false);
        assertEquals(expected, gameMaster.visit(received));
    }

    /**
     * Use Case 15: As GameMaster I want to process TestMessageReturn and not get stuck with it
     * subcase01 - happy path
     */
    @Test
    public void dealWithTestMessageReturn01Test() {
        add2Players();
        TestMessageReturn received = new TestMessageReturn(StatusMessageEnum.OK, playerUUID02BLUE, true);
        assertNull(gameMaster.visit(received));
    }

    GameStartMessage prepareStartMessage() {
        String playerGuid = playerUUID01RED;
        TeamColorMessageEnum teamColorMessageEnum = TeamColorMessageEnum.Red;
        String teamRole = "Best Team Member Test";
        int teamSize = 1;
        List<String> teamGuids = Collections.singletonList(playerGuid);
        MessagePosition messagePosition = new MessagePosition(player01PositionRED.getX(), player01PositionRED.getY());
        MessageBoard messageBoard = new MessageBoard(gameMasterConfiguration.boardWidth, gameMasterConfiguration.boardTaskHeight, gameMasterConfiguration.boardGoalHeight);
        return new GameStartMessage(playerGuid, teamColorMessageEnum, teamRole, teamSize, teamGuids, messagePosition, messageBoard);
    }
}