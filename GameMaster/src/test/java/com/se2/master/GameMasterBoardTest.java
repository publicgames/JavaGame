package com.se2.master;

import com.se2.game.board.Cell;
import com.se2.game.board.Pawn;
import com.se2.game.board.Piece;
import com.se2.game.board.Position;
import com.se2.game.board.enums.CellColor;
import com.se2.game.board.enums.CellState;
import com.se2.game.board.enums.Direction;
import com.se2.game.team.enums.TeamColor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class GameMasterBoardTest {
    private static final int boardWidth = 5;
    private static final int taskAreaHeight = 6;
    private static final int goalAreaHeight = 2;
    private static final int boardHeight = 10;
    private static final double shamProbability = 0.75d;
    private static final int initialPieces = 4;

    private static final int x = 3;
    private static final int y = 4;

    private static final String playerGuid = "test-player";

    private static final List<Position> predefinedGoalPositions = List.of(new Position(0, 0),
            new Position(0, boardHeight - 1),
            new Position(boardWidth - 1, 0),
            new Position(boardWidth - 1, boardHeight - 1),
            new Position(2, 1),
            new Position(2, boardHeight - 2));

    private static final List<Position> predefinedPiecesPosition = List.of(new Position(0, goalAreaHeight),
            new Position(0, taskAreaHeight + goalAreaHeight - 1),
            new Position(boardWidth - 1, goalAreaHeight),
            new Position(boardWidth - 1, taskAreaHeight + goalAreaHeight - 1),
            new Position(2, boardHeight / 2));

    private GameMasterBoard gameMasterBoard;

    private static Stream<Arguments> createMoves() {
        return Stream.of(
                Arguments.of(Direction.UP, 3, 3),
                Arguments.of(Direction.DOWN, 3, 5),
                Arguments.of(Direction.LEFT, 2, 4),
                Arguments.of(Direction.RIGHT, 4, 4)
        );
    }

    private static Stream<Arguments> createInvalidMoves() {
        return Stream.of(
                Arguments.of(Direction.UP, 0, 0, TeamColor.BLUE),
                Arguments.of(Direction.LEFT, 0, 0, TeamColor.BLUE),
                Arguments.of(Direction.DOWN, boardWidth - 1, boardHeight - 1, TeamColor.RED),
                Arguments.of(Direction.RIGHT, boardWidth - 1, boardHeight - 1, TeamColor.RED)
        );
    }

    private static Stream<Arguments> createMovesOnOtherTeamGoalArea() {
        return Stream.of(
                Arguments.of(Direction.UP, 0, goalAreaHeight, TeamColor.RED),
                Arguments.of(Direction.DOWN, 0, boardHeight - goalAreaHeight - 1, TeamColor.BLUE)
        );
    }

    private static List<Cell> prepareNeighbouringCells(int x, int y) {
        List<Cell> neighbouringCells = new ArrayList<>();
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 2, new Position(x - 1, y - 1), CellColor.GRAY, null));
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 2, new Position(x, y - 1), CellColor.GRAY, null));
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 1, new Position(x + 1, y - 1), CellColor.GRAY, null));
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 1, new Position(x - 1, y), CellColor.GRAY, null));
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 2, new Position(x, y), CellColor.GRAY, null));
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 2, new Position(x + 1, y), CellColor.GRAY, null));
        neighbouringCells.add(new Cell(CellState.PIECE, null, 0, new Position(x - 1, y + 1), CellColor.GRAY, new Piece(true)));
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 1, new Position(x, y + 1), CellColor.GRAY, null));
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 2, new Position(x + 1, y + 1), CellColor.GRAY, null));

        return neighbouringCells;
    }

    private static List<Cell> prepareNeighbouringCellsOnLeftEdge(int x, int y) {
        List<Cell> neighbouringCells = new ArrayList<>();
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 2, new Position(x, y - 1), CellColor.GRAY, null));
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 2, new Position(x + 1, y - 1), CellColor.GRAY, null));
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 2, new Position(x, y), CellColor.GRAY, null));
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 1, new Position(x + 1, y), CellColor.GRAY, null));
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 1, new Position(x, y + 1), CellColor.GRAY, null));
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 2, new Position(x + 1, y + 1), CellColor.GRAY, null));

        return neighbouringCells;
    }

    private static List<Cell> prepareNeighbouringCellsOnRightEdge(int x, int y) {
        List<Cell> neighbouringCells = new ArrayList<>();
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 2, new Position(x - 1, y - 1), CellColor.GRAY, null));
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 2, new Position(x, y - 1), CellColor.GRAY, null));
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 1, new Position(x - 1, y), CellColor.GRAY, null));
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 2, new Position(x, y), CellColor.GRAY, null));
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 2, new Position(x - 1, y + 1), CellColor.GRAY, null));
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 1, new Position(x, y + 1), CellColor.GRAY, null));

        return neighbouringCells;
    }

    private static List<Cell> prepareNeighbouringCellsOnTopLeftCorner(int x, int y) {
        List<Cell> neighbouringCells = new ArrayList<>();
        neighbouringCells.add(new Cell(CellState.PIECE, null, 0, new Position(x, y), CellColor.GRAY, new Piece(true)));
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 1, new Position(x + 1, y), CellColor.GRAY, null));
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 1, new Position(x, y + 1), CellColor.GRAY, null));
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 2, new Position(x + 1, y + 1), CellColor.GRAY, null));

        return neighbouringCells;
    }

    private static List<Cell> prepareNeighbouringCellsOnBottomRightCorner(int x, int y) {
        List<Cell> neighbouringCells = new ArrayList<>();
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 2, new Position(x - 1, y - 1), CellColor.GRAY, null));
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 1, new Position(x, y - 1), CellColor.GRAY, null));
        neighbouringCells.add(new Cell(CellState.EMPTY, null, 1, new Position(x - 1, y), CellColor.GRAY, null));
        neighbouringCells.add(new Cell(CellState.PIECE, null, 0, new Position(x, y), CellColor.GRAY, new Piece(true)));

        return neighbouringCells;
    }

    @BeforeEach
    public void setup() {
        gameMasterBoard = new GameMasterBoard(boardWidth,
                taskAreaHeight,
                goalAreaHeight,
                predefinedGoalPositions,
                0,
                shamProbability);
    }

    @Test
    public void initializePredefinedGoals() {
        gameMasterBoard = new GameMasterBoard(boardWidth,
                taskAreaHeight,
                goalAreaHeight,
                predefinedGoalPositions,
                shamProbability);

        assertAll(
                () -> {
                    for (Position position : predefinedGoalPositions) {
                        assertEquals(CellState.UNDISCOVERED_GOAL, gameMasterBoard.getCell(position.getX(), position.getY()).getCellState());
                    }
                }
        );
    }

    @Test
    public void initializePieces() {
        gameMasterBoard = new GameMasterBoard(boardWidth,
                taskAreaHeight,
                goalAreaHeight,
                predefinedGoalPositions,
                initialPieces,
                shamProbability);

        assertEquals(initialPieces, this.gameMasterBoard.getNumberOfPieces());
    }

    @Test
    public void calculateManhattanDistancesTest() {
        int[][] distances = {{2, 3, 4, 3, 2},
                {1, 2, 3, 2, 1},
                {0, 1, 2, 1, 0},
                {1, 2, 2, 2, 1},
                {2, 2, 1, 2, 2},
                {2, 1, 0, 1, 2},
                {1, 2, 1, 2, 1},
                {0, 1, 2, 1, 0},
                {1, 2, 3, 2, 1},
                {2, 3, 4, 3, 2}};

        gameMasterBoard = new GameMasterBoard(boardWidth,
                taskAreaHeight,
                goalAreaHeight,
                predefinedGoalPositions,
                predefinedPiecesPosition,
                shamProbability);

        assertAll(
                () -> {
                    for (Position piecePosition : predefinedPiecesPosition) {
                        Cell pieceCell = gameMasterBoard.getCell(piecePosition.getX(), piecePosition.getY());
                        assertEquals(CellState.PIECE, pieceCell.getCellState());
                    }
                },
                () -> {
                    for (int j = 0; j < distances.length; j++) {
                        for (int i = 0; i < distances[j].length; i++) {
                            assertEquals(distances[j][i], this.gameMasterBoard.getCell(i, j).getDistance());
                        }
                    }
                }
        );
    }

    @Test
    public void placePawn() {
        //given
        Position placementPosition = new Position(x, y);
        Pawn player = new Pawn(playerGuid);
        player.setTeamColorForPawn(TeamColor.RED);

        //when
        gameMasterBoard.placePawn(player, placementPosition);
        Cell newCell = gameMasterBoard.getCell(x, y);

        //then
        assertEquals(playerGuid, newCell.getPawnGuid());
    }

    @Test
    public void placePawnOnOccupiedCell() {
        //given
        String player2Guid = "test-player-2";
        Pawn player = new Pawn(playerGuid);
        player.setTeamColorForPawn(TeamColor.RED);
        Pawn player2 = new Pawn(player2Guid);
        player.setTeamColorForPawn(TeamColor.BLUE);
        Position occupiedCellPosition = new Position(x, y);
        this.gameMasterBoard.placePawn(player2, occupiedCellPosition);

        //when
        boolean result = this.gameMasterBoard.placePawn(player, occupiedCellPosition);
        Cell cellAfterPlacement = this.gameMasterBoard.getCell(x, y);

        //then
        assertAll(
                () -> assertFalse(result),
                () -> assertEquals(player2.getPlayerGuid(), cellAfterPlacement.getPawnGuid())
        );
    }

    @Test
    public void placePawnOnOtherTeamGoalArea() {
        //given
        Pawn player = new Pawn(playerGuid);
        player.setTeamColorForPawn(TeamColor.BLUE);
        Position otherTeamGoalAreaCell = new Position(boardWidth - 1, boardHeight - 1);

        //when
        boolean result = this.gameMasterBoard.placePawn(player, otherTeamGoalAreaCell);

        //then
        assertAll(
                () -> assertFalse(result)
        );
    }

    @ParameterizedTest
    @MethodSource("createMoves")
    public void movePawn(Direction direction, int newX, int newY) {
        //given
        Position currentPosition = new Position(x, y);
        Pawn player = new Pawn(playerGuid);
        player.setTeamColorForPawn(TeamColor.RED);
        gameMasterBoard.placePawn(player, currentPosition);
        //when
        gameMasterBoard.movePawn(player, direction);

        Cell oldCell = gameMasterBoard.getCell(x, y);
        Cell newCell = gameMasterBoard.getCell(newX, newY);

        //then
        assertAll(
                () -> assertNull(oldCell.getPawnGuid()),
                () -> assertEquals(playerGuid, newCell.getPawnGuid())
        );
    }

    @ParameterizedTest
    @MethodSource("createInvalidMoves")
    public void movePawnInvalid(Direction direction, int x, int y, TeamColor teamColor) {
        //given
        Position currentPosition = new Position(x, y);
        Pawn player = new Pawn(playerGuid);
        player.setTeamColorForPawn(teamColor);
        this.gameMasterBoard.placePawn(player, currentPosition);

        //when
        Cell playerCell = gameMasterBoard.getCell(x, y);
        boolean movementResult = gameMasterBoard.movePawn(player, direction);

        //then
        assertAll(
                () -> assertEquals(playerGuid, playerCell.getPawnGuid()),
                () -> assertFalse(movementResult)
        );
    }

    @ParameterizedTest
    @MethodSource("createMovesOnOtherTeamGoalArea")
    public void movePawnOnOtherTeamGoalArea(Direction direction, int x, int y, TeamColor teamColor) {
        //given
        Pawn player = new Pawn(playerGuid);
        player.setTeamColorForPawn(teamColor);
        Position playerPosition = new Position(x, y);
        this.gameMasterBoard.placePawn(player, playerPosition);

        //when
        boolean movementResult = this.gameMasterBoard.movePawn(player, direction);
        Cell cellAfterMovement = gameMasterBoard.getCell(x, y);

        //then
        assertAll(
                () -> assertEquals(playerGuid, cellAfterMovement.getPawnGuid()),
                () -> assertFalse(movementResult)
        );
    }

    @Test
    public void movePawnOnOccupiedCell() {
        //given
        Direction direction = Direction.UP;
        String player2Guid = "test-player-2";
        Pawn player = new Pawn(playerGuid);
        player.setTeamColorForPawn(TeamColor.RED);
        Pawn player2 = new Pawn(player2Guid);
        player.setTeamColorForPawn(TeamColor.BLUE);

        Position currentPosition = new Position(x, y);
        this.gameMasterBoard.placePawn(player, currentPosition);
        Position occupiedCellPosition = new Position(x, y - 1);

        boolean res = this.gameMasterBoard.placePawn(player2, occupiedCellPosition);
        this.gameMasterBoard.movePawn(player, direction);

        //when
        boolean movementResult = gameMasterBoard.movePawn(player, direction);
        Cell cellAfterMovement = gameMasterBoard.getCell(x, y - 1);

        //then
        assertAll(
                () -> assertEquals(player2Guid, cellAfterMovement.getPawnGuid()),
                () -> assertFalse(movementResult)
        );
    }

    @ParameterizedTest
    @CsvSource(value = {"0, 3", "0, 6", "3, 3", "3, 2", "2, 7"})
    public void addPieceInEmptyCell(int xLocation, int yLocation) {
        //given
        Piece newPiece = new Piece(true);
        Position position = new Position(xLocation, yLocation);

        //when
        boolean result = gameMasterBoard.addPiece(newPiece, position);
        Cell cell = gameMasterBoard.getCell(xLocation, yLocation);

        //then
        assertAll(
                () -> assertTrue(result),
                () -> assertNotNull(cell.getPiece()),
                () -> assertEquals(CellState.PIECE, cell.getCellState()),
                () -> assertEquals(newPiece.isSham(), cell.getPiece().isSham())
        );
    }

    @ParameterizedTest
    @CsvSource(value = {"0, 0", "1, 1", "2, 9", "3, 8", "4, 9"})
    public void addPieceInGoalArea(int xLocation, int yLocation) {
        //given
        Piece newPiece = new Piece(true);
        Position position = new Position(xLocation, yLocation);

        //when
        boolean result = gameMasterBoard.addPiece(newPiece, position);
        Cell cell = gameMasterBoard.getCell(xLocation, yLocation);

        //then
        assertAll(
                () -> assertFalse(result),
                () -> assertNull(cell.getPiece())
        );
    }

    @Test
    public void addPieceOnCellWithPieceCellState() {
        //given
        Piece oldPiece = new Piece(false);
        Position position = new Position(x, y);
        this.gameMasterBoard.addPiece(oldPiece, position);
        Piece newPiece = new Piece(true);

        //when
        boolean result = this.gameMasterBoard.addPiece(newPiece, position);
        Cell cell = gameMasterBoard.getCell(x, y);

        //then
        assertAll(
                () -> assertFalse(result),
                () -> assertNotNull(cell.getPiece()),
                () -> assertEquals(false, cell.getPiece().isSham())
        );
    }

    @Test
    public void placePieceByPawnNotSham() {
        //given
        Pawn player = new Pawn(playerGuid);
        player.setTeamColorForPawn(TeamColor.BLUE);
        player.setPiece(new Piece(false));
        int x = 2, y = 1;
        Position playerPosition = new Position(x, y);
        this.gameMasterBoard.placePawn(player, playerPosition);
        player.setPosition(playerPosition);

        //when
        boolean result = this.gameMasterBoard.placePieceByPawn(player);

        //then
        assertAll(
                () -> assertTrue(result),
                () -> assertEquals(CellState.DISCOVERED_GOAL, this.gameMasterBoard.getCell(x, y).getCellState()),
                () -> assertNull(player.getPiece())
        );
    }

    @Test
    public void placePieceByPawnSham() {
        //given
        Pawn player = new Pawn(playerGuid);
        player.setTeamColorForPawn(TeamColor.BLUE);
        player.setPiece(new Piece(true));
        int x = 2, y = 1;
        Position playerPosition = new Position(x, y);
        this.gameMasterBoard.placePawn(player, playerPosition);
        player.setPosition(playerPosition);

        //when
        boolean result = this.gameMasterBoard.placePieceByPawn(player);
        CellState expectedCellState = CellState.UNDISCOVERED_GOAL;
        CellState updatedCellState = this.gameMasterBoard.getCell(x, y).getCellState();

        //then
        assertAll(
                () -> assertFalse(result),
                () -> assertEquals(expectedCellState, updatedCellState),
                () -> assertNull(player.getPiece())
        );
    }

    @Test
    public void placePieceByPawnNotOnGoalArea() {
        //given
        Pawn player = new Pawn(playerGuid);
        player.setTeamColorForPawn(TeamColor.BLUE);
        player.setPiece(new Piece(true));
        Position playerPosition = new Position(2, 2);
        this.gameMasterBoard.placePawn(player, playerPosition);
        player.setPosition(playerPosition);

        //when
        boolean result = this.gameMasterBoard.placePieceByPawn(player);

        //then
        assertAll(
                () -> assertFalse(result),
                () -> assertNull(player.getPiece())
        );
    }

    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    public void takePiece(boolean isSham) {
        //given
        Piece newPiece = new Piece(isSham);
        Position position = new Position(x, y);
        gameMasterBoard.addPiece(newPiece, position);
        Pawn player = new Pawn(playerGuid);
        player.setTeamColorForPawn(TeamColor.BLUE);
        gameMasterBoard.placePawn(player, position);

        //when
        boolean result = gameMasterBoard.takePiece(player);
        Piece takenPiece = player.getPiece();

        //then
        assertAll(
                () -> assertNotNull(player.getPiece()),
                () -> assertEquals(newPiece.isSham(), takenPiece.isSham()),
                () -> assertTrue(result)
        );
    }

    @Test
    public void takePieceFromCellWithEmptyCellState() {
        //given
        Position position = new Position(x, y);
        Pawn player = new Pawn(playerGuid);
        player.setTeamColorForPawn(TeamColor.BLUE);
        gameMasterBoard.placePawn(player, position);

        //when

        boolean result = gameMasterBoard.takePiece(player);
        Piece takenPiece = player.getPiece();

        //then
        assertAll(
                () -> assertNull(takenPiece),
                () -> assertFalse(result)
        );
    }

    @ParameterizedTest
    @CsvSource(value = {"0, 0", "1, 1", "4, 1"})
    public void takePieceFromBlueGoalArea(int x, int y) {
        //given
        Position position = new Position(x, y);
        Pawn player = new Pawn(playerGuid);
        player.setTeamColorForPawn(TeamColor.BLUE);
        gameMasterBoard.placePawn(player, position);

        //when
        boolean result = gameMasterBoard.takePiece(player);
        Piece takenPiece = player.getPiece();

        //then
        assertAll(
                () -> assertNull(takenPiece),
                () -> assertFalse(result)

        );
    }

    @ParameterizedTest
    @CsvSource(value = {"2, 9", "3, 8", "4, 9"})
    public void takePieceFromRedGoalArea(int x, int y) {
        //given
        Position position = new Position(x, y);
        Pawn player = new Pawn(playerGuid);
        player.setTeamColorForPawn(TeamColor.RED);
        gameMasterBoard.placePawn(player, position);

        //when
        boolean result = gameMasterBoard.takePiece(player);
        Piece takenPiece = player.getPiece();

        //then
        assertAll(
                () -> assertNull(takenPiece),
                () -> assertFalse(result)
        );
    }


    @Test
    public void clearPieces() {
        //given
        this.gameMasterBoard.addPiece(new Piece(true), new Position(0, goalAreaHeight + 1));
        this.gameMasterBoard.addPiece(new Piece(false), new Position(3, goalAreaHeight + 2));

        //when
        this.gameMasterBoard.clearPieces();
        int expectedNumberOfPieces = 0;
        int actualNumberOfPieces = this.gameMasterBoard.getNumberOfPieces();
        //then
        assertEquals(expectedNumberOfPieces, actualNumberOfPieces);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5, 6})
    public void generatePieces(int maxPieces) {
        //given
        this.gameMasterBoard.clearPieces();

        //when
        this.gameMasterBoard.generatePieces(maxPieces);
        int expectedNumberOfPieces = maxPieces;
        int actualNumberOfPieces = this.gameMasterBoard.getNumberOfPieces();

        //then
        assertEquals(expectedNumberOfPieces, actualNumberOfPieces);
    }

    @Test
    public void discover() {
        //given
        initializeGameMasterBoardWithInitialPiecesPositions();

        Position testPosition = new Position(x, y);
        int listSize = 9;
        List<Cell> neighbouringCells = prepareNeighbouringCells(x, y);
        //when
        List<Cell> discoveredCells = gameMasterBoard.discover(testPosition);

        //then
        assertAll(
                () -> assertEquals(listSize, discoveredCells.size()),
                () -> assertEquals(neighbouringCells, discoveredCells)
        );
    }

    @Test
    public void discoverOnLeftEdge() {
        //given
        initializeGameMasterBoardWithInitialPiecesPositions();
        int x = 0;
        int y = 5;
        Position testPosition = new Position(x, y);
        int listSize = 6;
        List<Cell> neighbouringCells = prepareNeighbouringCellsOnLeftEdge(x, y);

        //when
        List<Cell> discoveredCells = gameMasterBoard.discover(testPosition);

        //then
        assertAll(
                () -> assertEquals(listSize, discoveredCells.size()),
                () -> assertEquals(neighbouringCells, discoveredCells)
        );
    }

    @Test
    public void discoverOnRightEdge() {
        //given
        initializeGameMasterBoardWithInitialPiecesPositions();
        int x = boardWidth - 1;
        int y = 5;
        Position testPosition = new Position(x, y);
        int listSize = 6;
        List<Cell> neighbouringCells = prepareNeighbouringCellsOnRightEdge(x, y);

        //when
        List<Cell> discoveredCells = gameMasterBoard.discover(testPosition);

        //then
        assertAll(
                () -> assertEquals(listSize, discoveredCells.size()),
                () -> assertEquals(neighbouringCells, discoveredCells)
        );
    }

    @Test
    public void discoverOnTopLeftCorner() {
        //given
        initializeGameMasterBoardWithInitialPiecesPositions();
        int x = 0;
        int y = goalAreaHeight;
        Position testPosition = new Position(x, y);
        int listSize = 4;
        List<Cell> neighbouringCells = prepareNeighbouringCellsOnTopLeftCorner(x, y);

        //when
        List<Cell> discoveredCells = gameMasterBoard.discover(testPosition);

        //then
        assertAll(
                () -> assertEquals(listSize, discoveredCells.size()),
                () -> assertEquals(neighbouringCells, discoveredCells)
        );
    }

    @Test
    public void discoverOnBottomRightCorner() {
        //given
        initializeGameMasterBoardWithInitialPiecesPositions();
        int x = boardWidth - 1;
        int y = boardHeight - goalAreaHeight - 1;
        Position testPosition = new Position(x, y);
        int listSize = 4;
        List<Cell> neighbouringCells = prepareNeighbouringCellsOnBottomRightCorner(x, y);

        //when
        List<Cell> discoveredCells = gameMasterBoard.discover(testPosition);

        //then
        assertAll(
                () -> assertEquals(listSize, discoveredCells.size()),
                () -> assertEquals(neighbouringCells, discoveredCells)
        );
    }

    private void initializeGameMasterBoardWithInitialPiecesPositions() {
        gameMasterBoard = new GameMasterBoard(boardWidth,
                taskAreaHeight,
                goalAreaHeight,
                predefinedGoalPositions,
                predefinedPiecesPosition,
                shamProbability);
    }
}