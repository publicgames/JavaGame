package com.se2.game.board;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PieceTest {
    @Test
    public void shouldReturnFalse() {
        //given
        boolean isSham = false;

        //when
        Piece testPiece = new Piece(isSham);

        //then
        assertFalse(testPiece.isSham());
    }

    @Test
    public void shouldReturnTrue() {
        //given
        boolean isSham = true;

        //when
        Piece testPiece = new Piece(isSham);

        //then
        assertTrue(testPiece.isSham());
    }

}