package com.se2.game.board;

import com.se2.game.board.enums.CellColor;
import com.se2.game.board.enums.CellState;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.se2.game.board.enums.CellState.EMPTY;

/**
 * Unit test for CellState.
 */
public class CellStateTest {
    @Test
    public void isThereDiscGoal() {
        // Given
        CellState cellState1 = CellState.DISCOVERED_GOAL;
        String playerGuid1 = "playerGuid";
        Position position1 = null;
        int distance1 = 2;
        CellColor cellColor1 = CellColor.RED;
        Piece piece1 = new Piece(true);
        Cell cell = new Cell(cellState1, playerGuid1, distance1, position1, cellColor1, piece1);
        CellState expected_result = cellState1;
        // When
        CellState result = cell.getCellState();
        // Then
        Assertions.assertEquals(result, expected_result);
    }

    @Test
    public void isThereUndiscGoal() {
        // Given
        CellState cellState1 = CellState.UNDISCOVERED_GOAL;
        String playerGuid1 = "playerGuid";
        Position position1 = null;
        int distance1 = 2;
        CellColor cellColor1 = CellColor.RED;
        Piece piece1 = new Piece(true);
        Cell cell = new Cell(cellState1, playerGuid1, distance1, position1, cellColor1, piece1);
        CellState expected_result = cellState1;
        // When
        CellState result = cell.getCellState();
        // Then
        Assertions.assertEquals(result, expected_result);
    }

    @Test
    public void isTherePiece() {
        // Given
        CellState cellState1 = CellState.PIECE;
        String playerGuid1 = "playerGuid";
        Position position1 = null;
        int distance1 = 2;
        CellColor cellColor1 = CellColor.RED;
        Piece piece1 = new Piece(true);
        Cell cell = new Cell(cellState1, playerGuid1, distance1, position1, cellColor1, piece1);
        CellState expected_result = cellState1;
        // When
        CellState result = cell.getCellState();
        // Then
        Assertions.assertEquals(result, expected_result);
    }

    @Test
    public void isEmpty() {
        // Given
        CellState cellState1 = EMPTY;
        String playerGuid1 = "playerGuid";
        Position position1 = null;
        int distance1 = 2;
        CellColor cellColor1 = CellColor.RED;
        Piece piece1 = new Piece(true);
        Cell cell = new Cell(cellState1, playerGuid1, distance1, position1, cellColor1, piece1);
        CellState expected_result = cellState1;
        // When
        CellState result = cell.getCellState();
        // Then
        Assertions.assertEquals(result, expected_result);
    }

    @Test
    public void isStateUnknown() {
        // Given
        CellState cellState1 = CellState.UNKNOWN;
        String playerGuid1 = "playerGuid";
        Position position1 = null;
        int distance1 = 2;
        CellColor cellColor1 = CellColor.RED;
        Piece piece1 = new Piece(true);
        Cell cell = new Cell(cellState1, playerGuid1, distance1, position1, cellColor1, piece1);
        CellState expected_result = cellState1;
        // When
        CellState result = cell.getCellState();
        // Then
        Assertions.assertEquals(result, expected_result);
    }

}
