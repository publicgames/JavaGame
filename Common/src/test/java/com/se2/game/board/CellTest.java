package com.se2.game.board;

import com.se2.game.board.enums.CellColor;
import com.se2.game.board.enums.CellState;
import org.junit.jupiter.api.Test;

import static com.se2.game.board.enums.CellState.EMPTY;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Unit test for Cell equals()
 */
public class CellTest {
    @Test
    void testEquals() {
        //given
        CellState cellState1 = EMPTY;
        String playerGuid1 = "1";
        int distance1 = 6;
        Position position1 = new Position(3, 7);
        CellColor cellColor1 = CellColor.GRAY;
        Piece piece1 = null;
        Cell cell1 = new Cell(cellState1, playerGuid1, distance1, position1, cellColor1, piece1);
        Cell cell2 = new Cell(cellState1, playerGuid1, distance1, position1, cellColor1, piece1);
        boolean expected_result = true;
        //when
        boolean result = cell1.equals(cell2);
        //then
        assertEquals(expected_result, result);
    }
}
