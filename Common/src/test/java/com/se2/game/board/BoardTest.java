package com.se2.game.board;

import com.se2.game.board.enums.CellColor;
import com.se2.game.board.enums.CellState;
import com.se2.tools.exceptions.BadConfigurationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class BoardTest {
    private static final int boardWidth = 5;
    private static final int taskAreaHeight = 6;
    private static final int goalAreaHeight = 2;
    private static final int boardHeight = 10;
    private static final int x = 4;
    private static final int y = 7;
    private static Board board;

    @BeforeEach
    public void setup() {
        board = new Board(boardWidth, taskAreaHeight, goalAreaHeight, false);
    }

    @Test
    void testConstructor() {
        //given
        //when
        Board testBoard = new Board(boardWidth, taskAreaHeight, goalAreaHeight, false);

        //then
        assertAll(
                () -> assertEquals(boardWidth, testBoard.getBoardWidth()),
                () -> assertEquals(taskAreaHeight, testBoard.getTaskAreaHeight()),
                () -> assertEquals(goalAreaHeight, testBoard.getGoalAreaHeight()),
                () -> assertEquals(boardHeight, testBoard.getBoardHeight())
        );
    }

    @ParameterizedTest
    @CsvSource(value = {"-2, 2, 2", "2, -2, 2", "2, 2, -2", "0, 2, 2", "2, 0, 2", "2, 2, 0", "33, 4, 4", "5, 20, 20"})
    public void testConstructorWithInvalidValues(int boardWidth, int taskAreaHeight, int goalAreaHeight) {
        assertThrows(BadConfigurationException.class,
                () -> new Board(boardWidth, taskAreaHeight, goalAreaHeight, false));
    }

    @ParameterizedTest
    @CsvSource(value = {"0, 0", "2, 4", "4, 7", "4, 8"})
    public void getCell(int x, int y) {
        //given

        //when
        Cell testCell = board.getCell(x, y);

        //then
        assertAll(
                () -> assertEquals(x, testCell.getPosition().getX()),
                () -> assertEquals(y, testCell.getPosition().getY())
        );
    }

    @ParameterizedTest
    @CsvSource(value = {"-1, 2", "2, -1", "20, 4", "4, 20"})
    public void getCellWithInvalidValues(int x, int y) {
        assertThrows(BadConfigurationException.class,
                () -> board.getCell(x, y));
    }

    @Test
    public void updateCellWithEmptyCellState() {
        //given
        CellState cellState = CellState.EMPTY;
        String playerGuid = null;
        Position position = new Position(x, y);
        int distance = 0;
        CellColor cellColor = CellColor.GRAY;
        Piece piece = null;

        //when
        Cell testCell = new Cell(cellState, playerGuid, distance, position, cellColor, piece);

        board.updateCell(testCell);

        Cell updatedCell = board.getCell(x, y);

        //then
        assertEquals(testCell, updatedCell);
    }

    @Test
    public void updateCellWithPieceCellState() {
        //given
        CellState cellState = CellState.PIECE;
        String playerGuid = null;
        Position position = new Position(x, y);
        int distance = 0;
        CellColor cellColor = CellColor.GRAY;
        Piece piece = new Piece(false);

        //when
        Cell testCell = new Cell(cellState, playerGuid, distance, position, cellColor, piece);

        board.updateCell(testCell);

        Cell updatedCell = board.getCell(x, y);

        //then
        assertEquals(testCell, updatedCell);
    }

    @Test
    public void updateCellWithDiscoveredGoalCellState() {
        //given
        CellState cellState = CellState.DISCOVERED_GOAL;
        String playerGuid = null;
        Position position = new Position(x, y);
        int distance = 0;
        CellColor cellColor = CellColor.YELLOW;
        Piece piece = null;

        //when
        Cell testCell = new Cell(cellState, playerGuid, distance, position, cellColor, piece);

        board.updateCell(testCell);

        Cell updatedCell = board.getCell(x, y);

        //then
        assertEquals(testCell, updatedCell);
    }
}