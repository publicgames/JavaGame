package com.se2.game.team;

import com.se2.game.board.Pawn;
import com.se2.game.player.enums.PlayerState;
import com.se2.game.team.enums.TeamColor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class TeamTest {

    private Team testTeam;
    private TeamColor teamColor;
    private Pawn pawnOne;
    private Pawn pawnTwo;
    private String playerUUID01 = "00000000-0000-0000-0000-000000000001";
    private String playerUUID02 = "00000000-0000-0000-0000-000000000002";

    @BeforeEach
    public void setUp() {
        teamColor = TeamColor.BLUE;
        testTeam = new Team(teamColor);
        pawnOne = new Pawn(playerUUID01);
        pawnOne.setTeamColorForPawn(teamColor);
        pawnTwo = new Pawn(playerUUID02);
        pawnTwo.setTeamColorForPawn(teamColor);
    }

    @Test
    public void testAddPlayer() {
        //when
        testTeam.addPawn(pawnOne);
        testTeam.addPawn(pawnTwo);

        //then
        assertAll(
                () -> assertEquals(2, testTeam.getSize()),
                () -> assertEquals(true, testTeam.getPawns().contains(pawnOne)),
                () -> assertEquals(true, testTeam.getPawns().contains(pawnTwo)),
                () -> assertEquals(TeamColor.BLUE, pawnOne.getTeamColorForPawn()),
                () -> assertEquals(TeamColor.BLUE, pawnOne.getTeamColorForPawn())
        );
    }

    @Test
    void playersNotReady() {
        testTeam.addPawn(pawnOne);
        testTeam.addPawn(pawnTwo);

        assertEquals(true, testTeam.isAnyPawnInStatus(PlayerState.Initializing));
    }

    @Test
    void playersReady() {
        testTeam.addPawn(pawnOne);
        pawnOne.setPlayerState(PlayerState.Active);
        testTeam.addPawn(pawnTwo);
        pawnTwo.setPlayerState(PlayerState.Active);

        assertEquals(false, testTeam.isAnyPawnInStatus(PlayerState.Initializing));
    }

    @Test
    void getNotReadyPlayers() {
        testTeam.addPawn(pawnOne);
        pawnOne.setPlayerState(PlayerState.Active);
        testTeam.addPawn(pawnTwo);

        List<Pawn> expected = new ArrayList<Pawn>();
        expected.add(pawnTwo);
        assertEquals(expected, testTeam.getPawnsInStatus(PlayerState.Initializing));
    }

    @Test
    void getNotReadyPlayersWhereThereAreNon() {
        testTeam.addPawn(pawnOne);
        pawnOne.setPlayerState(PlayerState.Active);
        testTeam.addPawn(pawnTwo);
        pawnTwo.setPlayerState(PlayerState.Active);

        assertEquals(new ArrayList<Pawn>(), testTeam.getPawnsInStatus(PlayerState.Initializing));
    }

    @Test
    void getPlayerByGuid() {
        assertNull(testTeam.getPawn(playerUUID01));
        testTeam.addPawn(pawnOne);
        assertEquals(pawnOne, testTeam.getPawn(playerUUID01));
        assertNull(testTeam.getPawn(playerUUID02));
    }
}