package com.se2.communication.classes;

import com.se2.communication.enums.CellStateMessageEnum;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MessageFieldsTest {
    @Test
    void testEquals() {
        //given
        CellStateMessageEnum cellState1 = CellStateMessageEnum.Empty;
        int distance1 = 4;
        String playerGuid1 = "1";
        MessageCell messageCell1 = new MessageCell(cellState1, distance1, playerGuid1);
        MessagePosition messagePosition1 = new MessagePosition(4, 7);
        MessageFields messageFields1 = new MessageFields(messagePosition1, messageCell1);
        MessageFields messageFields2 = new MessageFields(messagePosition1, messageCell1);
        boolean expected_result = true;
        //when
        boolean result = messageFields1.equals(messageFields2);
        //then
        assertEquals(result, expected_result);
    }
}
