package com.se2.communication.classes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MessageBoardTest {

    @Test
    void testEquals() {
        //given
        int boardWidth1 = 6;
        int taskAreaHeight1 = 8;
        int goalAreaHeight1 = 3;
        MessageBoard messageBoard1 = new MessageBoard(boardWidth1, taskAreaHeight1, goalAreaHeight1);
        MessageBoard messageBoard2 = new MessageBoard(boardWidth1, taskAreaHeight1, goalAreaHeight1);
        boolean expected_result = true;
        //when
        boolean result = messageBoard1.equals(messageBoard2);
        //then
        assertEquals(result, expected_result);
    }
}
