package com.se2.communication.classes;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MessagePositionTest {
    @Test
    void testEquals() {
        //given
        MessagePosition messagePosition1 = new MessagePosition(3, 7);
        MessagePosition messagePosition2 = new MessagePosition(3, 7);
        boolean expected_result = true;
        //when
        boolean result = messagePosition1.equals(messagePosition2);
        //then
        assertEquals(result, expected_result);
    }
}
