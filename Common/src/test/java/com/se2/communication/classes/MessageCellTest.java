package com.se2.communication.classes;

import com.se2.communication.enums.CellStateMessageEnum;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MessageCellTest {
    @Test
    void testEquals() {
        //given
        CellStateMessageEnum cellState1 = CellStateMessageEnum.Empty;
        int distance1 = 7;
        String playerGuid1 = "2";
        MessageCell messageCell1 = new MessageCell(cellState1, distance1, playerGuid1);
        MessageCell messageCell2 = new MessageCell(cellState1, distance1, playerGuid1);
        boolean expected_result = true;
        //when
        boolean result = messageCell1.equals(messageCell2);
        //then
        assertEquals(result, expected_result);
    }
}
