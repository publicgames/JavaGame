package com.se2.tools;

public class StringUtils {
    public static boolean isEmpty(String toCheck) {
        return toCheck != null && toCheck.isEmpty();
    }

    /*
     * return first string not null and not empty
     */
    public static String coalesce(String[] strings) {
        for (String string : strings) {
            if (!isEmpty(string)) {
                return string;
            }
        }
        return null;
    }
}
