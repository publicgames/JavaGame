package com.se2.tools;

public class BooleanUtils {
    public static boolean isEmpty(Boolean toCheck) {
        return toCheck == null;
    }

    /*
     * return first Boolean not null and not empty
     */
    public static Boolean coalesce(Boolean[] booleans) {
        for (Boolean b : booleans) {
            if (!isEmpty(b)) {
                return b;
            }
        }
        return null;
    }
}
