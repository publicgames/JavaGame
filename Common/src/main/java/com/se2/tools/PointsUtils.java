package com.se2.tools;

import com.se2.game.board.Position;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PointsUtils {
    public static List<Position> getListOfPointsFromString(String pointsToSplit) {
        String[] points = pointsToSplit.split(":");
        List<Position> output = new ArrayList<>();
        for (String point : points) {
            String[] coordinates = point.split(",");
            output.add(new Position(Integer.parseInt(coordinates[0]), Integer.parseInt(coordinates[1])));
        }
        return output;
    }

    public static List<Position> generateListOfPoints(Integer goalNumber, Integer boardWidth, Integer taskHeight, Integer goalHeight) {
        System.out.println("Generating goals");
        List<Position> output = new ArrayList<>();
        for (int i = 0 ; i < goalNumber;) {
            int x = new Random().nextInt(boardWidth);
            int y = new Random().nextInt(goalHeight);
            Position toAdd = new Position(x, y);
            if (!output.contains(toAdd)) {
               output.add(toAdd);
               output.add(inverse(toAdd, boardWidth, taskHeight + 2 * goalHeight));
               i++;
            }
        }
        Logger.log("Generated goals:" + output.toString());
        return output;
    }

    private static Position inverse(Position toAdd, Integer boardWidth, Integer boardHeight) {
        return new Position(boardWidth-toAdd.getX()-1, boardHeight - toAdd.getY()-1);
    }
}
