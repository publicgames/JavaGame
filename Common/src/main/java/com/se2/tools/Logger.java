package com.se2.tools;

import com.se2.communication.enums.ActionMessageEnum;
import com.se2.communication.enums.StatusMessageEnum;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class Logger {
    private static final String DEBUG_FILE_HEADER = "Debug file header\n";
    private static final String LOG_FILE_HEADER = "Action type\tGuid\t\t\t\t\t\t\t\t\t\tStatus\t\t\tRemark\t\tTimestamp\n";

    private static Path debug_file;
    private static Path log_file;
    private static boolean debug;

//    public Logger(String filePath, boolean isDebugOn) {
//        this.isDebugOn = isDebugOn;
//    }

    public static void start(boolean debugOn, String logFile) {
        debug = debugOn;
        prepareLogFiles(logFile);
//        file = logFile;
    }

    private static void prepareLogFiles(String filePath) {
        String logfilePath = "log_" + filePath;

        Path path = Paths.get(filePath);
        Path logPath = Paths.get(logfilePath);
        try {
            if (Files.exists(path)) {
                Files.delete(path);
            }
            if (Files.exists(logPath)) {
                Files.delete(logPath);
            }
            debug_file = Files.createFile(path);
            log_file = Files.createFile(logPath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        write(DEBUG_FILE_HEADER);
        writeLog(LOG_FILE_HEADER);
    }

    public static void log(ActionMessageEnum actionType, String guid, StatusMessageEnum status, String remark) {
        String message = String.format("%s\t\t%s\t\t%s\t\t%s\t\t%s",
                actionType,
                guid,
                status,
                remark,
                LocalTime.now().truncatedTo(ChronoUnit.NANOS));
        writeLog(message);
    }

    private static void writeLog(String message) {
        try {
            if (log_file != null && Files.exists(log_file)) {
                Files.write(log_file,
                        (message + (message.endsWith("\n") ? "" : "\n")).getBytes(),
                        StandardOpenOption.CREATE,
                        StandardOpenOption.APPEND);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void write(String message) {
        try {
            if (debug_file != null && Files.exists(debug_file)) {
                Files.write(debug_file,
                        (LocalTime.now().truncatedTo(ChronoUnit.MILLIS) + ": " + message + (message.endsWith("\n") ? "" : "\n")).getBytes(),
                        StandardOpenOption.CREATE,
                        StandardOpenOption.APPEND);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void log(String s) {
        write(s);
    }

    public static void error(String s) {
        write("ERROR: " + s);
    }

    public static void debug(String s) {
        if (debug) {
            System.out.println("DEBUG:" + s);
            write(s);
        }
    }

    public static void warning(String s) {
        System.out.println("WARNING:" + s);
        write(s);
    }
}
