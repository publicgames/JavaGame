package com.se2.tools;

import java.util.UUID;

public class Uuid {

    private final String id;

    public Uuid() {
        UUID uuid = new UUID(15l, 5l);
        id = uuid.randomUUID().toString();
    }

    public String getId() {
        return id;
    }
}
