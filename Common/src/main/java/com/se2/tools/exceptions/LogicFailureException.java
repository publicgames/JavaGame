package com.se2.tools.exceptions;

public class LogicFailureException extends RuntimeException {
    public LogicFailureException(String message) {
        super(message);
    }
}
