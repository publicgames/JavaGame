package com.se2.tools;

import com.se2.communication.classes.MessageBoard;
import com.se2.communication.classes.MessageCell;
import com.se2.communication.classes.MessageFields;
import com.se2.communication.classes.MessagePosition;
import com.se2.communication.enums.CellStateMessageEnum;
import com.se2.communication.enums.DirectionMessageEnum;
import com.se2.communication.enums.TeamColorMessageEnum;
import com.se2.game.board.Board;
import com.se2.game.board.Cell;
import com.se2.game.board.Piece;
import com.se2.game.board.Position;
import com.se2.game.board.enums.CellColor;
import com.se2.game.board.enums.CellState;
import com.se2.game.board.enums.Direction;
import com.se2.game.team.enums.TeamColor;

import java.util.ArrayList;
import java.util.List;

import static com.se2.tools.Logger.warning;

public class ConvertersUtils {
    public static List<Cell> convertMessageFieldsToCells(List<MessageFields> fieldsFromMessages) {
        List<Cell> output = new ArrayList<>();
        for (MessageFields field : fieldsFromMessages) {
            CellState cellState = convertCellStateMessageEnumToCellState(field.getMessageCell().getCellState());
            String playerGuid = field.getMessageCell().getPlayerGuid();
            int distance = field.getMessageCell().getDistance();
            Position position = convertMessagePositionToPosition(field.getMessagePosition());
            CellColor cellColor = null;
            Piece piece = null;
            output.add(new Cell(cellState, playerGuid, distance, position, cellColor, piece));
        }
        return output;
    }

    public static List<MessageFields> convertCellsToMessageFields(List<Cell> cells) {
        List<MessageFields> output = new ArrayList<>();
        for (Cell cell : cells) {
            MessagePosition messagePosition = convertPositionToMessagePosition(cell.getPosition());
            CellStateMessageEnum cellStateMessageEnum = convertCellStateToCellStateMessageEnum(cell.getCellState());
            int distance = cell.getDistance();
            String playerGuid = cell.getPawnGuid();
            MessageCell messageCell = new MessageCell(cellStateMessageEnum, distance, playerGuid);
            output.add(new MessageFields(messagePosition, messageCell));
        }
        return output;
    }

    public static Position convertMessagePositionToPosition(MessagePosition messagePosition) {
        if (messagePosition == null) {
            return null;
        } else {
            return new Position(messagePosition.getX(), messagePosition.getY());
        }
    }

    public static MessagePosition convertPositionToMessagePosition(Position position) {
        if (position == null) {
            return null;
        } else {
            return new MessagePosition(position.getX(), position.getY());
        }
    }

    public static CellState convertCellStateMessageEnumToCellState(CellStateMessageEnum cellStateMessageEnum) {
        if (cellStateMessageEnum == null) {
            return null;
        } else {
            switch (cellStateMessageEnum) {
                case Empty:
                    return CellState.EMPTY;
                case Piece:
                    return CellState.PIECE;
                default:
                    warning("CellStateMessageEnum passed without Empty or Piece state!");
                    return null;
            }
        }
    }

    public static CellStateMessageEnum convertCellStateToCellStateMessageEnum(CellState cellState) {
        if (cellState == null) {
            return null;
        } else {
            switch (cellState) {
                case PIECE:
                    return CellStateMessageEnum.Piece;
                case EMPTY:
                case DISCOVERED_GOAL:
                case UNDISCOVERED_GOAL:
                case UNKNOWN:
                    return CellStateMessageEnum.Empty;
                default:
                    return null;
            }
        }
    }

    public static Direction convertMessageDirectionToDirection(DirectionMessageEnum directionMessageEnum) {
        if (directionMessageEnum == null) {
            return null;
        } else {
            switch (directionMessageEnum) {
                case Up:
                    return Direction.UP;
                case Down:
                    return Direction.DOWN;
                case Left:
                    return Direction.LEFT;
                case Right:
                    return Direction.RIGHT;
                default:
                    return null;
            }
        }
    }

    public static DirectionMessageEnum convertDirectionToMessageDirection(Direction direction) {
        if (direction == null) {
            return null;
        } else {
            switch (direction) {
                case UP:
                    return DirectionMessageEnum.Up;
                case DOWN:
                    return DirectionMessageEnum.Down;
                case LEFT:
                    return DirectionMessageEnum.Left;
                case RIGHT:
                    return DirectionMessageEnum.Right;
                default:
                    return null;
            }
        }
    }

    public static TeamColor convertTeamColorMessageEnumToTeamColor(TeamColorMessageEnum teamColorMessageEnum) {
        if (teamColorMessageEnum == null) {
            return null;
        } else {
            switch (teamColorMessageEnum) {
                case Red:
                    return TeamColor.RED;
                case Blue:
                    return TeamColor.BLUE;
                default:
                    return null;
            }
        }
    }

    public static TeamColorMessageEnum convertTeamColorToTeamColorMessageEnum(TeamColor teamColor) {
        if (teamColor == null) {
            return null;
        } else {
            switch (teamColor) {
                case RED:
                    return TeamColorMessageEnum.Red;
                case BLUE:
                    return TeamColorMessageEnum.Blue;
                default:
                    return null;
            }
        }
    }

    public static Board convertMessageBoardToBoard(MessageBoard messageBoard, boolean fieldsUnknown) {
        if (messageBoard == null) {
            return null;
        } else {
            return new Board(messageBoard.getBoardWidth(), messageBoard.getTaskAreaHeight(), messageBoard.getGoalAreaHeight(), fieldsUnknown);
        }
    }
}