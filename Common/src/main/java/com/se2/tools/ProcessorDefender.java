package com.se2.tools;

public class ProcessorDefender {

    public static void sleepPlease() {
        long snoozeFor = 20; // in milliseconds
        sleepPlease(snoozeFor);
    }

    public static void sleepPlease(long wakeMeUpIn) {
        try {
            Thread.sleep(wakeMeUpIn);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
