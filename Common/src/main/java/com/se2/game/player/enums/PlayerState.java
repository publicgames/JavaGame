package com.se2.game.player.enums;

public enum PlayerState {
    Initializing,//
    Connected, // receive ConnectMessageReturn with OK.
    WaitingForGameStartMessage, //Waiting for Start Message from GM
    Active, //  game started - status for most of the game
    Completed // GameOverStatus
}
