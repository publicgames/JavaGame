package com.se2.game.board.enums;

public enum CellState {
    EMPTY,
    DISCOVERED_GOAL,
    DISCOVERED_NON_GOAL,
    UNDISCOVERED_GOAL,
    PIECE,
    UNKNOWN
}
