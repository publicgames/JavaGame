package com.se2.game.board;

import com.se2.game.board.enums.CellColor;
import com.se2.game.board.enums.CellState;

import java.util.Objects;

public class Cell {
    private final Position position;
    private CellState cellState;
    private String pawnGuid;
    private int distance;
    private CellColor cellColor;
    private Piece piece;

    public Cell(CellState cellState, String pawnGuid, int distance, Position position, CellColor cellColor, Piece piece) {
        this.cellState = cellState;
        this.pawnGuid = pawnGuid;
        this.distance = distance;
        this.position = position;
        this.cellColor = cellColor;
        this.piece = piece;
    }

    public CellState getCellState() {
        return this.cellState;
    }

    public void setCellState(CellState cellState) {
        this.cellState = cellState;
    }

    public Position getPosition() {
        return this.position;
    }

    public CellColor getCellColor() {
        return this.cellColor;
    }

    public void setCellColor(CellColor cellColor) {
        this.cellColor = cellColor;
    }

    public Piece getPiece() {
        return this.piece;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }

    public String getPawnGuid() {
        return this.pawnGuid;
    }

    public void setPawnGuid(String pawnGuid) {
        this.pawnGuid = pawnGuid;
    }

    public int getDistance() {
        return this.distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cell)) return false;
        Cell cell = (Cell) o;
        return distance == cell.distance &&
                Objects.equals(position, cell.position) &&
                cellState == cell.cellState &&
                Objects.equals(pawnGuid, cell.pawnGuid) &&
                cellColor == cell.cellColor &&
                ((piece == null && cell.piece == null) || (piece != null && cell.piece != null));
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, cellState, pawnGuid, distance, cellColor, piece);
    }

    @Override
    public String toString() {
        return "Cell{" +
                "position=" + position +
                ", cellState=" + cellState +
                ", pawnGuid='" + pawnGuid + '\'' +
                ", distance=" + distance +
                ", cellColor=" + cellColor +
                ", piece=" + piece +
                '}';
    }
}
