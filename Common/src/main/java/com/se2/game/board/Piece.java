package com.se2.game.board;

import java.util.Objects;

public class Piece {
    private Boolean isSham; //null value is for player before testing piece

    public Piece(Boolean isSham) {
        this.isSham = isSham;
    }

    public Boolean isSham() {
        return isSham;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Piece)) return false;
        Piece piece = (Piece) o;
        return Objects.equals(isSham, piece.isSham);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isSham);
    }

    @Override
    public String toString() {
        return "Piece{" +
                "isSham=" + isSham +
                '}';
    }
}
