package com.se2.game.board.enums;

public enum Direction {
    UP,
    DOWN,
    LEFT,
    RIGHT
}
