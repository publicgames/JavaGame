package com.se2.game.board.enums;

public enum CellColor {
    RED,
    BLUE,
    GRAY,
    YELLOW
}
