package com.se2.game.board;

import com.se2.game.player.enums.PieceState;
import com.se2.game.player.enums.PlayerState;
import com.se2.game.team.enums.TeamColor;
import com.se2.tools.Uuid;

import java.util.Objects;

public class Pawn {
    //represents player on board
    //created to separate Player implementation from GM

    protected Position position;
    protected Piece piece;
    protected PieceState pieceState;
    protected PlayerState playerState;
    protected TeamColor teamColorForPawn;
    protected String playerGuid;
    protected long lastMoveTime;

    public Pawn() {
        this.playerGuid = new Uuid().getId();
        this.position = null;
        this.piece = null;
        this.pieceState = null;
        this.playerState = PlayerState.Initializing;
        this.teamColorForPawn = null;
        this.lastMoveTime = 0L;
    }

    public Pawn(String playerGuid) {
        this.playerGuid = playerGuid;
        this.position = null;
        this.piece = null;
        this.pieceState = null;
        this.playerState = PlayerState.Initializing;
        this.teamColorForPawn = null;
        this.lastMoveTime = 0L;
    }

    public Pawn(String playerGuid, TeamColor teamColorForPawn) {
        this.playerGuid = playerGuid;
        this.position = null;
        this.piece = null;
        this.pieceState = null;
        this.playerState = PlayerState.Initializing;
        this.teamColorForPawn = teamColorForPawn;
        this.lastMoveTime = 0L;
    }

    public Pawn(String playerGuid, Position position, Piece piece, PieceState pieceState, TeamColor teamColorForPawn) {
        this.playerGuid = playerGuid;
        this.position = position;
        this.piece = piece;
        this.pieceState = pieceState;
        this.playerState = PlayerState.Initializing;
        this.teamColorForPawn = teamColorForPawn;
        this.lastMoveTime = 0L;
    }

    public long getLastMoveTime() {
        return lastMoveTime;
    }

    public void setLastMoveTime(long lastMoveTime) {
        this.lastMoveTime = lastMoveTime;
    }

    public boolean isPawnInStatus(PlayerState playerState) {
        return this.playerState.equals(playerState);
    }

    public String getPlayerGuid() {
        return playerGuid;
    }

    public void setPlayerGuid(String playerGuid) {
        this.playerGuid = playerGuid;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Piece getPiece() {
        return piece;
    }

    public void setPiece(Piece piece) {
        this.piece = piece;
    }

    public PieceState getPieceState() {
        return pieceState;
    }

    public void setPieceState(PieceState pieceState) {
        this.pieceState = pieceState;
    }

    public PlayerState getPlayerState() {
        return playerState;
    }

    public void setPlayerState(PlayerState playerState) {
        this.playerState = playerState;
    }

    public TeamColor getTeamColorForPawn() {
        return teamColorForPawn;
    }

    public void setTeamColorForPawn(TeamColor teamColorForPawn) {
        this.teamColorForPawn = teamColorForPawn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pawn)) return false;
        Pawn pawn = (Pawn) o;
        return Objects.equals(playerGuid, pawn.playerGuid) &&
                Objects.equals(position, pawn.position) &&
                Objects.equals(piece, pawn.piece) &&
                pieceState == pawn.pieceState &&
                playerState == pawn.playerState &&
                teamColorForPawn == pawn.teamColorForPawn &&
                playerGuid.equals(pawn.playerGuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerGuid, position, piece, pieceState, playerState, teamColorForPawn);
    }

    @Override
    public String toString() {
        return "Pawn{" +
                "playerGuid='" + playerGuid + '\'' +
                ", position=" + position +
                ", piece=" + piece +
                ", pieceState=" + pieceState +
                ", playerState=" + playerState +
                ", teamColorForPawn=" + teamColorForPawn +
                '}';
    }
}
