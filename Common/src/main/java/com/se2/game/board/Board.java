package com.se2.game.board;

import com.se2.game.board.enums.CellColor;
import com.se2.game.board.enums.CellState;
import com.se2.game.team.enums.TeamColor;
import com.se2.tools.exceptions.BadConfigurationException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Board {
    protected final int boardWidth;
    protected final int boardHeight;
    protected final int taskAreaHeight;
    protected final int goalAreaHeight;
    private final int MIN_DIMENSTION = 4;
    private final int MAX_DIMENSION = 32;
    protected Cell[][] cells;

    protected Position redInitialPosition;
    protected Position blueInitialPosition;

    public Board(int width, int taskAreaHeight, int goalAreaHeight, boolean fieldsUnknown) {
        if (width < MIN_DIMENSTION || width > MAX_DIMENSION) {
            throw new BadConfigurationException("Invalid width: " + width);
        }

        if (taskAreaHeight <= 0) {
            throw new BadConfigurationException("Invalid taskAreaHeight: " + taskAreaHeight);
        }

        if (goalAreaHeight <= 0) {
            throw new BadConfigurationException("Invalid goalAreaHeight: " + goalAreaHeight);
        }

        if (goalAreaHeight * 2 + taskAreaHeight < MIN_DIMENSTION ||
                goalAreaHeight * 2 + taskAreaHeight > MAX_DIMENSION) {
            throw new BadConfigurationException("Invalid height: " + (goalAreaHeight * 2 + taskAreaHeight));
        }

        this.boardWidth = width;
        this.taskAreaHeight = taskAreaHeight;
        this.goalAreaHeight = goalAreaHeight;
        this.boardHeight = goalAreaHeight * 2 + taskAreaHeight;

        this.redInitialPosition = new Position(boardWidth - 1, boardHeight - 1);
        this.blueInitialPosition = new Position(0, 0);

        cells = new Cell[boardHeight][boardWidth];

        initializeCells(fieldsUnknown);
    }

    public Cell getCell(Position position) {
        return null == position ? null : getCell(position.getX(), position.getY());
    }

    public Cell getCell(int x, int y) {
        if (x < 0 || x >= boardWidth) {
            throw new BadConfigurationException("Invalid x: " + x);
        }

        if (y < 0 || y >= boardHeight) {
            throw new BadConfigurationException("Invalid y: " + y);
        }

        return cells[y][x];
    }

    public void updateCell(Cell cell) { //deleted position because cell already has such field
        int x = cell.getPosition().getX();
        int y = cell.getPosition().getY();

        cells[y][x] = cell;
    }

    public List<Cell> getNeighbouringCells(Position position) {
        int x = position.getX();
        int y = position.getY();
        List<Cell> neighbouringCells = new ArrayList<>();

        List<Position> neighbouringPositions = getNeighbouringPositions(x, y);

        for (Position neighbouringPosition : neighbouringPositions) {
            if (isInTaskArea(neighbouringPosition)) {
                int neighbouringX = neighbouringPosition.getX();
                int neighbouringY = neighbouringPosition.getY();
                neighbouringCells.add(this.getCell(neighbouringX, neighbouringY));
            }
        }

        return neighbouringCells;
    }

    public List<Position> getNeighbouringPositions(int x, int y) {
        return Arrays.asList(
                new Position(x - 1, y - 1),
                new Position(x, y - 1),
                new Position(x + 1, y - 1),
                new Position(x - 1, y),
                new Position(x, y),
                new Position(x + 1, y),
                new Position(x - 1, y + 1),
                new Position(x, y + 1),
                new Position(x + 1, y + 1)
        );
    }

    protected void initializeCells(boolean fieldsUnknown) {
        CellState fillWith = null;
        if (fieldsUnknown) {
            fillWith = CellState.UNKNOWN;
        } else {
            fillWith = CellState.EMPTY;
        }
        for (int y = 0; y < boardHeight; y++) {
            for (int x = 0; x < boardWidth; x++) {
                CellColor cellColor;
                if (y < goalAreaHeight) {
                    cellColor = CellColor.BLUE;
                } else if (y < goalAreaHeight + taskAreaHeight) {
                    cellColor = CellColor.GRAY;
                } else {
                    cellColor = CellColor.RED;
                }

                Cell cell = new Cell(fillWith, null, 0, new Position(x, y), cellColor, null);
                updateCell(cell);
            }
        }
    }

    protected boolean isInBounds(Position position) {
        return (position.getY() >= 0 && position.getY() < this.boardHeight &&
                position.getX() >= 0 && position.getX() < this.boardWidth);
    }

    protected boolean isInOtherTeamGoalArea(Position position, TeamColor playerTeamColor) {
        if (playerTeamColor == TeamColor.BLUE) { //check other team goal area
            if (position.getY() >= this.boardHeight - this.goalAreaHeight)
                return true;
        } else {
            if (position.getY() < this.goalAreaHeight)
                return true;
        }

        return false;
    }

    public boolean isInTaskArea(Position position) {
        int x = position.getX();
        int y = position.getY();

        return (x >= 0 && x < this.boardWidth &&
                y >= this.goalAreaHeight && y < this.boardHeight - this.goalAreaHeight);
    }

    public int getBoardWidth() {
        return this.boardWidth;
    }

    public int getBoardHeight() {
        return this.boardHeight;
    }

    public int getTaskAreaHeight() {
        return this.taskAreaHeight;
    }

    public int getGoalAreaHeight() {
        return this.goalAreaHeight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Board)) return false;
        Board board = (Board) o;
        return MIN_DIMENSTION == board.MIN_DIMENSTION &&
                MAX_DIMENSION == board.MAX_DIMENSION &&
                boardWidth == board.boardWidth &&
                boardHeight == board.boardHeight &&
                taskAreaHeight == board.taskAreaHeight &&
                goalAreaHeight == board.goalAreaHeight &&
                Arrays.deepEquals(cells, board.cells) &&
                Objects.equals(redInitialPosition, board.redInitialPosition) &&
                Objects.equals(blueInitialPosition, board.blueInitialPosition);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(MIN_DIMENSTION, MAX_DIMENSION, boardWidth, boardHeight, taskAreaHeight, goalAreaHeight, redInitialPosition, blueInitialPosition);
        result = 31 * result + Arrays.hashCode(cells);
        return result;
    }

    @Override
    public String toString() {
        return "Board{" +
                "MIN_DIMENSTION=" + MIN_DIMENSTION +
                ", MAX_DIMENSION=" + MAX_DIMENSION +
                ", boardWidth=" + boardWidth +
                ", boardHeight=" + boardHeight +
                ", taskAreaHeight=" + taskAreaHeight +
                ", goalAreaHeight=" + goalAreaHeight +
                ", cells=" + Arrays.deepToString(cells) +
                ", redInitialPosition=" + redInitialPosition +
                ", blueInitialPosition=" + blueInitialPosition +
                '}';
    }
}
