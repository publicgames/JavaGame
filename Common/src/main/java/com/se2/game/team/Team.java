package com.se2.game.team;

import com.se2.game.board.Pawn;
import com.se2.game.player.enums.PlayerState;
import com.se2.game.team.enums.TeamColor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Team {
    private final TeamColor teamColor;
    private List<Pawn> pawns;

    public Team(TeamColor teamColor) {
        this.teamColor = teamColor;
        this.pawns = new ArrayList<>();
    }

    public List<Pawn> getPawns() {
        return pawns;
    }

    public List<String> getTeamGuids(PlayerState playerState) {
        List<String> output = new ArrayList<>();
        for (Pawn pawn : getPawnsInStatus(playerState)) {
            output.add(pawn.getPlayerGuid());
        }
        return output;
    }

    public TeamColor getTeamColor() {
        return teamColor;
    }

    public int getSize() {
        return this.pawns.size();
    }

    public void addPawn(Pawn pawnToAdd) {
        pawns.add(pawnToAdd);
    }

    public Pawn getPawn(String guid) {
        for (Pawn pawn : pawns) {
            if (guid.equals(pawn.getPlayerGuid())) {
                return pawn;
            }
        }
        return null;
    }

    public boolean isAnyPawnInStatus(PlayerState playerState) {
        for (Pawn pawn : pawns) {
            if (pawn.isPawnInStatus(playerState)) {
                return true;
            }
        }
        return false;
    }

    public List<Pawn> getPawnsInStatus(PlayerState playerState) {
        List<Pawn> output = new ArrayList<>();
        for (Pawn pawn : pawns) {
            if (pawn.isPawnInStatus(playerState)) {
                output.add(pawn);
            }
        }
        return output;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Team)) return false;
        Team team = (Team) o;
        return teamColor == team.teamColor &&
                Objects.equals(pawns, team.pawns);
    }

    @Override
    public int hashCode() {
        return Objects.hash(teamColor, pawns);
    }

    @Override
    public String toString() {
        return "Team{" +
                "teamColor=" + teamColor +
                ", pawns=" + pawns +
                '}';
    }
}
