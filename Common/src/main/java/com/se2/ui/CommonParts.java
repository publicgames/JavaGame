package com.se2.ui;

import javax.swing.*;
import java.awt.*;

public class CommonParts {
    public static void setBasicOptions(JFrame window, JPanel jPanel, String title) {
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setBounds(30, 30, 300, 300);
        window.getContentPane().add(jPanel);
        window.setVisible(true);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setSize(700, 700);
        window.setExtendedState(JFrame.MAXIMIZED_BOTH);
        window.setTitle(title);
        window.setBackground(new Color(255, 255, 255));
    }
}