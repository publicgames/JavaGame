package com.se2.ui;

import com.se2.game.board.Board;
import com.se2.game.board.Cell;
import com.se2.game.board.Pawn;
import com.se2.game.board.Position;
import com.se2.game.board.enums.CellColor;
import com.se2.game.board.enums.CellState;
import com.se2.game.team.enums.TeamColor;

import javax.swing.*;
import java.awt.*;

import static com.se2.tools.ProcessorDefender.sleepPlease;
import static com.se2.ui.CommonParts.setBasicOptions;

public class BoardGui extends JPanel implements Runnable {
    protected final Color redPlayerColor = new Color(255, 70, 62);
    protected final Color bluePlayerColor = new Color(8, 161, 255);
    protected final int optionsPanelWidth;
    protected Thread thread = null;
    protected Board board;
    protected int boardWidth;
    protected int boardHeight;
    protected int rectWidth;
    protected int rectHeight;
    protected int movementSize;
    protected JFrame window;
    protected String title;

    public BoardGui(Board board, String title, int optionsPanelWidth) {
        this.board = board;
        if (board != null) {
            this.boardWidth = board.getBoardWidth();
            this.boardHeight = board.getBoardHeight();
        }
        this.optionsPanelWidth = optionsPanelWidth;
        this.title = title;
    }

    public void start() {
        if (thread == null) {
            JFrame window = new JFrame();
            this.window = window;
            setBasicOptions(window, this, title);
            thread = new Thread(this);
            thread.start();
        }
    }

    public void stop() {
        thread = null;
    }

    public void run() {
        while (thread != null) {
            sleepPlease();
            repaint();
        }
        thread = null;
    }

    protected void calculateWidthAndHeight(int additionalMovement) {
        double screenX = this.window.getSize().getWidth();
        double screenY = this.window.getSize().getHeight();
        int rectWidth = (int) ((screenX - this.optionsPanelWidth - 50) / boardWidth);
        int rectHeight = (int) ((screenY - 200) / boardHeight);
        this.rectWidth = Math.min(rectWidth, rectHeight);
        this.rectHeight = Math.min(rectWidth, rectHeight);

        movementSize = ((int) screenX - this.rectWidth * boardWidth - additionalMovement) / 2;
    }

    protected void drawMesh(Graphics g) {
        g.setColor(Color.black);
        int xStart = 0;
        int xEnd = boardWidth * this.rectWidth;

        for (int y = 0; y <= boardHeight; y++) {
            g.drawLine(this.movementSize + xStart, this.rectHeight * y, this.movementSize + xEnd, this.rectHeight * y);
        }

        int yStart = 0;
        int yEnd = boardHeight * this.rectHeight;

        for (int x = 0; x <= boardWidth; x++) {
            g.drawLine(this.movementSize + this.rectWidth * x, yStart, this.movementSize + this.rectWidth * x, yEnd);
        }
    }

    protected void drawPlayer(Graphics g, Pawn pawn, int x, int y) {
        if (pawn != null) {
            TeamColor teamColor = pawn.getTeamColorForPawn();
            g.setColor(teamColor == TeamColor.RED ? redPlayerColor : bluePlayerColor);
            g.fillOval(this.movementSize + this.rectWidth * x + 1, this.rectHeight * y + 1, this.rectWidth-2, this.rectHeight-2);
            if (pawn.getPiece() != null) {
                g.setColor(Color.yellow);
                g.fillOval(this.movementSize + this.rectWidth * x + 1 + this.rectWidth / 4, this.rectHeight * y + 1 + this.rectHeight / 4, 1 + this.rectWidth / 2, 1 + this.rectHeight/2);
            }
        } else {
            g.setColor(Color.black);
            g.fillOval(this.movementSize + this.rectWidth * x + 1 + this.rectWidth / 4, this.rectHeight * y + 1 + this.rectHeight / 4, 1 + this.rectWidth / 2, 1 + this.rectHeight / 2);

        }

    }

    public void update(Graphics g) {
        paint(g);
    }

    protected void processAndDrawCell(Graphics g, Cell cell, int x, int y) {
        drawBackgroundOfCell(g, cell, x, y);
        drawSymbolOnCell(g, cell, x, y);
    }

    protected void drawBackgroundOfCell(Graphics g, Cell cell, int x, int y) {
        switch (cell.getCellColor()) {
            case BLUE:
                g.setColor(Color.blue);
                break;
            case RED:
                g.setColor(Color.red);
                break;
            case GRAY:
                g.setColor(Color.gray);
                break;
        }
        g.fillRect(this.movementSize + this.rectWidth * x, this.rectHeight * y, this.rectWidth, this.rectHeight);
    }

    protected void drawSymbolOnCell(Graphics g, Cell cell, int x, int y) {
        String symbol = "";
        switch (cell.getCellState()) {
            case DISCOVERED_GOAL:
                g.setColor(Color.yellow);
                symbol = "G";
                break;
            case UNDISCOVERED_GOAL:
                g.setColor(Color.green);
                symbol = "YG";
                break;
            case PIECE:
                if (null == cell.getPiece()){
                    //player gui
                    g.setColor(Color.YELLOW);
                } else {
                    //GM gui
                    if (cell.getPiece().isSham()){
                        g.setColor(Color.ORANGE);
                    } else {
                        g.setColor(Color.YELLOW);
                    }
                }
                symbol = "P";
                break;
            case UNKNOWN:
                if (board.isInTaskArea(new Position(x, y))){
                    g.setColor(Color.black);
                }
                symbol = "U";
                break;
            case EMPTY:
                g.setColor(Color.white);
                symbol = "E";
                break;
            case DISCOVERED_NON_GOAL:
                g.setColor(new Color(50, 50, 50));
                break;
            default:
                g.setColor(Color.CYAN);
                break;
        }
        if (cell.getCellState() != CellState.EMPTY && cell.getCellState() != CellState.UNKNOWN)
            g.fillRect(this.movementSize + this.rectWidth * x, this.rectHeight * y, this.rectWidth, this.rectHeight);
        //g.drawString(symbol, this.movementSize + this.rectWidth * x + this.rectWidth / 12, this.rectHeight * (y + 1) - this.rectHeight / 12);

        Font font = new Font("Arial", Font.PLAIN, 16);
        if ((CellState.EMPTY.equals(cell.getCellState()) || CellState.PIECE.equals(cell.getCellState())) && cell.getDistance() < Integer.MAX_VALUE &&  CellColor.GRAY.equals(cell.getCellColor())) {
            g.setFont(font.deriveFont(this.rectHeight));
            int move = 0;
            if (cell.getDistance()<10) {
                move = this.rectWidth / 3;
            }
            g.drawString(Integer.toString(cell.getDistance()), this.movementSize + this.rectWidth * x + move + 1, this.rectHeight * (y + 1) - 1);
        }
    }
}
