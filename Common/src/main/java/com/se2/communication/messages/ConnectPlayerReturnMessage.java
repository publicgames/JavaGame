package com.se2.communication.messages;

import com.se2.communication.Visitor;
import com.se2.communication.enums.ActionMessageEnum;
import com.se2.communication.enums.StatusMessageEnum;

import java.util.Objects;

public class ConnectPlayerReturnMessage extends MessageReturn {

    private String playerGuid;
    private int portNumber;


    public ConnectPlayerReturnMessage(String playerGuid, int portNumber, StatusMessageEnum statusMessageEnum) {
        super(ActionMessageEnum.connect, statusMessageEnum);
        this.playerGuid = playerGuid;
        this.portNumber = portNumber;
    }

    @Override
    public Message accept(Visitor visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return "ConnectPlayerReturnMessage{" +
                "playerGuid='" + playerGuid + '\'' +
                ", portNumber=" + portNumber +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConnectPlayerReturnMessage)) return false;
        if (!super.equals(o)) return false;
        ConnectPlayerReturnMessage that = (ConnectPlayerReturnMessage) o;
        return portNumber == that.portNumber &&
                Objects.equals(playerGuid, that.playerGuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), playerGuid, portNumber);
    }

    public String getPlayerGuid() {
        return playerGuid;
    }

    public int getPortNumber() {
        return portNumber;
    }
}
