package com.se2.communication.messages;

import com.se2.communication.Visitor;
import com.se2.communication.enums.ActionMessageEnum;

import java.util.Objects;

public class PickUpMessage extends Message {

    private String playerGuid;

    public PickUpMessage(String playerGuid) {
        super(ActionMessageEnum.pickup);
        this.playerGuid = playerGuid;
    }

    @Override
    public Message accept(Visitor visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return "PickUpMessage{" +
                "playerGuid='" + playerGuid + '\'' +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PickUpMessage)) return false;
        if (!super.equals(o)) return false;
        PickUpMessage that = (PickUpMessage) o;
        return Objects.equals(playerGuid, that.playerGuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), playerGuid);
    }

    public String getPlayerGuid() {
        return playerGuid;
    }
}
