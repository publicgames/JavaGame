package com.se2.communication.messages;

import com.se2.communication.Visitor;
import com.se2.communication.enums.ActionMessageEnum;

import java.util.Objects;

public class ReadyMessage extends Message {

    private String playerGuid;

    public ReadyMessage(String playerGuid) {
        super(ActionMessageEnum.ready);
        this.playerGuid = playerGuid;
    }

    @Override
    public Message accept(Visitor visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return "ReadyMessage{" +
                "playerGuid='" + playerGuid + '\'' +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReadyMessage)) return false;
        if (!super.equals(o)) return false;
        ReadyMessage that = (ReadyMessage) o;
        return Objects.equals(playerGuid, that.playerGuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), playerGuid);
    }

    public String getPlayerGuid() {
        return playerGuid;
    }
}
