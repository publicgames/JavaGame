package com.se2.communication.messages;

import com.se2.communication.Visitor;
import com.se2.communication.enums.ActionMessageEnum;
import com.se2.communication.enums.TeamColorMessageEnum;

import java.util.Objects;

public class EndMessage extends Message {

    private TeamColorMessageEnum result;

    public EndMessage(TeamColorMessageEnum resultEnum) {
        super(ActionMessageEnum.end);
        result = resultEnum;
    }

    @Override
    public Message accept(Visitor visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return "EndMessage{" +
                "result=" + result +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EndMessage)) return false;
        if (!super.equals(o)) return false;
        EndMessage that = (EndMessage) o;
        return result == that.result;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), result);
    }

    public TeamColorMessageEnum getResult() {
        return result;
    }
}
