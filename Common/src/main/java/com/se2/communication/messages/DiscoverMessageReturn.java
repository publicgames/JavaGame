package com.se2.communication.messages;

import com.google.gson.annotations.SerializedName;
import com.se2.communication.Visitor;
import com.se2.communication.classes.MessageFields;
import com.se2.communication.classes.MessagePosition;
import com.se2.communication.enums.ActionMessageEnum;
import com.se2.communication.enums.StatusMessageEnum;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DiscoverMessageReturn extends MessageReturn {

    private String playerGuid;
    @SerializedName(value = "position", alternate = {"messagePosition"})
    private MessagePosition messagePosition;
    private List<MessageFields> fields;

    public DiscoverMessageReturn(String playerGuid, MessagePosition messagePosition, StatusMessageEnum statusMessageEnum, List<MessageFields> fields) {
        super(ActionMessageEnum.discover, statusMessageEnum);
        this.playerGuid = playerGuid;
        this.messagePosition = messagePosition;
        this.fields = new ArrayList<>();
        if (fields != null)
            this.fields.addAll(fields);
    }

    @Override
    public Message accept(Visitor visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return "DiscoverMessageReturn{" +
                "playerGuid='" + playerGuid + '\'' +
                ", messagePosition=" + messagePosition +
                ", fields=" + fields +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DiscoverMessageReturn)) return false;
        if (!super.equals(o)) return false;
        DiscoverMessageReturn that = (DiscoverMessageReturn) o;
        return Objects.equals(playerGuid, that.playerGuid) &&
                Objects.equals(messagePosition, that.messagePosition) &&
                Objects.equals(fields, that.fields);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), playerGuid, messagePosition, fields);
    }


    public String getPlayerGuid() {
        return playerGuid;
    }

    public MessagePosition getMessagePosition() {
        return messagePosition;
    }

    public List<MessageFields> getFields() {
        return fields;
    }
}
