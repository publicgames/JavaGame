package com.se2.communication.messages;

import com.se2.communication.enums.ActionMessageEnum;
import com.se2.communication.enums.StatusMessageEnum;

import java.util.Objects;

public class MessageReturn extends Message {
    public final StatusMessageEnum status;

    MessageReturn(ActionMessageEnum actionMessageEnum, StatusMessageEnum status) {
        super(actionMessageEnum);
        this.status = status;
    }

    @Override
    public String toString() {
        return "AbstractMessageReturn{" +
                "statusMessageEnum=" + status +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MessageReturn)) return false;
        if (!super.equals(o)) return false;
        MessageReturn that = (MessageReturn) o;
        return status == that.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), status);
    }
}
