package com.se2.communication.messages;

import com.se2.communication.Visitor;
import com.se2.communication.enums.ActionMessageEnum;
import com.se2.communication.enums.StatusMessageEnum;

import java.util.Objects;

public class PickUpMessageReturn extends MessageReturn {

    private String playerGuid;

    public PickUpMessageReturn(StatusMessageEnum statusMessageEnum, String playerGuid) {
        super(ActionMessageEnum.pickup, statusMessageEnum);
        this.playerGuid = playerGuid;
    }

    @Override
    public Message accept(Visitor visitor) {
        return visitor.visit(this);
    }

    public String getPlayerGuid() {
        return playerGuid;
    }

    @Override
    public String toString() {
        return "PickUpMessageReturn{" +
                "playerGuid='" + playerGuid + '\'' +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PickUpMessageReturn)) return false;
        if (!super.equals(o)) return false;
        PickUpMessageReturn that = (PickUpMessageReturn) o;
        return Objects.equals(playerGuid, that.playerGuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), playerGuid);
    }
}
