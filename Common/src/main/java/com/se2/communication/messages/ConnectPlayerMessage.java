package com.se2.communication.messages;

import com.se2.communication.Visitor;
import com.se2.communication.enums.ActionMessageEnum;

import java.util.Objects;

public class ConnectPlayerMessage extends Message {

    private String playerGuid;
    private int portNumber;

    public ConnectPlayerMessage(String playerGuid, int portNumber) {
        super(ActionMessageEnum.connect);
        this.playerGuid = playerGuid;
        this.portNumber = portNumber;
    }

    @Override
    public Message accept(Visitor visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return "ConnectPlayerMessage{" +
                "playerGuid='" + playerGuid + '\'' +
                ", portNumber=" + portNumber +
                "} " + super.toString();
    }

    public String getPlayerGuid() {
        return playerGuid;
    }

    public int getPortNumber() {
        return portNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConnectPlayerMessage)) return false;
        if (!super.equals(o)) return false;
        ConnectPlayerMessage that = (ConnectPlayerMessage) o;
        return portNumber == that.portNumber &&
                Objects.equals(playerGuid, that.playerGuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), playerGuid, portNumber);
    }
}
