package com.se2.communication.messages;

import com.google.gson.annotations.SerializedName;
import com.se2.communication.Visitor;
import com.se2.communication.classes.MessagePosition;
import com.se2.communication.enums.ActionMessageEnum;

import java.util.Objects;

public class DiscoverMessage extends Message {

    private String playerGuid;
    @SerializedName(value = "position", alternate = {"messagePosition"})
    private MessagePosition messagePosition;

    public DiscoverMessage(String playerGuid, MessagePosition messagePosition) {
        super(ActionMessageEnum.discover);
        this.playerGuid = playerGuid;
        this.messagePosition = messagePosition;
    }

    @Override
    public Message accept(Visitor visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return "DiscoverMessage{" +
                "playerGuid='" + playerGuid + '\'' +
                ", messagePosition=" + messagePosition +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DiscoverMessage)) return false;
        if (!super.equals(o)) return false;
        DiscoverMessage that = (DiscoverMessage) o;
        return Objects.equals(playerGuid, that.playerGuid) &&
                Objects.equals(messagePosition, that.messagePosition);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), playerGuid, messagePosition);
    }

    public String getPlayerGuid() {
        return playerGuid;
    }

    public MessagePosition getMessagePosition() {
        return messagePosition;
    }
}
