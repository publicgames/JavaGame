package com.se2.communication.messages;

import com.se2.communication.Visitor;
import com.se2.communication.enums.ActionMessageEnum;

public class GameSetup extends Message {
    public GameSetup() {
        super(ActionMessageEnum.setup);
    }

    @Override
    public Message accept(Visitor visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return "GameSetup{} " + super.toString();
    }
}
