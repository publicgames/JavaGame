package com.se2.communication.messages;

import com.se2.communication.Visitor;
import com.se2.communication.enums.ActionMessageEnum;

import java.util.Objects;

public class TestMessage extends Message {

    private String playerGuid;

    public TestMessage(String playerGuid) {
        super(ActionMessageEnum.test);
        this.playerGuid = playerGuid;
    }

    @Override
    public Message accept(Visitor visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return "TestMessage{" +
                "playerGuid='" + playerGuid + '\'' +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TestMessage)) return false;
        if (!super.equals(o)) return false;
        TestMessage that = (TestMessage) o;
        return Objects.equals(playerGuid, that.playerGuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), playerGuid);
    }

    public String getPlayerGuid() {
        return playerGuid;
    }
}
