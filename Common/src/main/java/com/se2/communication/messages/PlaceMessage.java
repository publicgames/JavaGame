package com.se2.communication.messages;

import com.se2.communication.Visitor;
import com.se2.communication.enums.ActionMessageEnum;

import java.util.Objects;

public class PlaceMessage extends Message {

    private String playerGuid;

    public PlaceMessage(String playerGuid) {
        super(ActionMessageEnum.place);
        this.playerGuid = playerGuid;
    }

    @Override
    public Message accept(Visitor visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return "PlaceMessage{" +
                "playerGuid='" + playerGuid + '\'' +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PlaceMessage)) return false;
        if (!super.equals(o)) return false;
        PlaceMessage that = (PlaceMessage) o;
        return Objects.equals(playerGuid, that.playerGuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), playerGuid);
    }

    public String getPlayerGuid() {
        return playerGuid;
    }
}
