package com.se2.communication.messages;

import com.se2.communication.Visitor;
import com.se2.communication.enums.ActionMessageEnum;
import com.se2.communication.enums.PlacementResultEnum;
import com.se2.communication.enums.StatusMessageEnum;

import java.util.Objects;

public class PlaceMessageReturn extends MessageReturn {

    private String playerGuid;
    private PlacementResultEnum placementResult;

    public PlaceMessageReturn(StatusMessageEnum statusMessageEnum, String playerGuid, PlacementResultEnum placementResult) {
        super(ActionMessageEnum.place, statusMessageEnum);
        this.playerGuid = playerGuid;
        this.placementResult = placementResult;
    }

    @Override
    public Message accept(Visitor visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return "PlaceMessageReturn{" +
                "playerGuid='" + playerGuid + '\'' +
                ", placementResult=" + placementResult +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PlaceMessageReturn)) return false;
        if (!super.equals(o)) return false;
        PlaceMessageReturn that = (PlaceMessageReturn) o;
        return Objects.equals(playerGuid, that.playerGuid) &&
                placementResult == that.placementResult;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), playerGuid, placementResult);
    }

    public String getPlayerGuid() {
        return playerGuid;
    }

    public PlacementResultEnum getPlacementResult() {
        return placementResult;
    }
}
