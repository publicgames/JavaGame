package com.se2.communication.messages;

import com.google.gson.annotations.SerializedName;
import com.se2.communication.Visitor;
import com.se2.communication.enums.ActionMessageEnum;
import com.se2.communication.enums.DirectionMessageEnum;

import java.util.Objects;

public class MoveMessage extends Message {

    private String playerGuid;
    @SerializedName(value = "direction", alternate = {"directionMessageEnum"})
    private DirectionMessageEnum directionMessageEnum;

    public MoveMessage(String playerGuid, DirectionMessageEnum directionMessageEnum) {
        super(ActionMessageEnum.move);
        this.playerGuid = playerGuid;
        this.directionMessageEnum = directionMessageEnum;
    }

    @Override
    public Message accept(Visitor visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return "MoveMessage{" +
                "playerGuid='" + playerGuid + '\'' +
                ", directionMessageEnum=" + directionMessageEnum +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MoveMessage)) return false;
        if (!super.equals(o)) return false;
        MoveMessage that = (MoveMessage) o;
        return Objects.equals(playerGuid, that.playerGuid) &&
                directionMessageEnum == that.directionMessageEnum;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), playerGuid, directionMessageEnum);
    }

    public String getPlayerGuid() {
        return playerGuid;
    }

    public DirectionMessageEnum getDirectionMessageEnum() {
        return directionMessageEnum;
    }
}
