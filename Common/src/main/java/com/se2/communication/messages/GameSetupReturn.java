package com.se2.communication.messages;

import com.se2.communication.Visitor;
import com.se2.communication.enums.ActionMessageEnum;
import com.se2.communication.enums.StatusMessageEnum;

public class GameSetupReturn extends MessageReturn {

    public GameSetupReturn(StatusMessageEnum statusMessageEnum) {
        super(ActionMessageEnum.setup, statusMessageEnum);
    }

    @Override
    public Message accept(Visitor visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return "GameSetupReturn{} " + super.toString();
    }
}
