package com.se2.communication.messages;

import com.se2.communication.Visitor;
import com.se2.communication.enums.ActionMessageEnum;
import com.se2.communication.enums.StatusMessageEnum;

import java.util.Objects;

public class ReadyMessageReturn extends MessageReturn {

    private String playerGuid;

    public ReadyMessageReturn(StatusMessageEnum statusMessageEnum, String playerGuid) {
        super(ActionMessageEnum.ready, statusMessageEnum);
        this.playerGuid = playerGuid;
    }

    @Override
    public Message accept(Visitor visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return "ReadyMessageReturn{" +
                "playerGuid='" + playerGuid + '\'' +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReadyMessageReturn)) return false;
        if (!super.equals(o)) return false;
        ReadyMessageReturn that = (ReadyMessageReturn) o;
        return Objects.equals(playerGuid, that.playerGuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), playerGuid);
    }

    public String getPlayerGuid() {
        return playerGuid;
    }
}
