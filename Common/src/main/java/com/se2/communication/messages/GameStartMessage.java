package com.se2.communication.messages;

import com.google.gson.annotations.SerializedName;
import com.se2.communication.Visitor;
import com.se2.communication.classes.MessageBoard;
import com.se2.communication.classes.MessagePosition;
import com.se2.communication.enums.ActionMessageEnum;
import com.se2.communication.enums.TeamColorMessageEnum;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class GameStartMessage extends Message {

    private String playerGuid;
    private TeamColorMessageEnum team;
    private String teamRole;
    private int teamSize;
    private List<String> teamGuids;
    @SerializedName(value = "position", alternate = {"messagePosition"})
    private MessagePosition messagePosition;
    @SerializedName(value = "board", alternate = {"messageBoard"})
    private MessageBoard messageBoard;

    public GameStartMessage(String playerGuid, TeamColorMessageEnum team, String teamRole, int teamSize, List<String> teamGuids, MessagePosition messagePosition, MessageBoard messageBoard) {
        super(ActionMessageEnum.start);
        this.playerGuid = playerGuid;
        this.team = team;
        this.teamRole = teamRole;
        this.teamSize = teamSize;
        this.teamGuids = new ArrayList<>();
        this.teamGuids.addAll(teamGuids);
        this.messagePosition = messagePosition;
        this.messageBoard = messageBoard;
    }

    @Override
    public Message accept(Visitor visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return "GameStartMessage{" +
                "playerGuid='" + playerGuid + '\'' +
                ", team=" + team +
                ", teamRole='" + teamRole + '\'' +
                ", teamSize=" + teamSize +
                ", teamGuids=" + teamGuids +
                ", messagePosition=" + messagePosition +
                ", messageBoard=" + messageBoard +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GameStartMessage)) return false;
        if (!super.equals(o)) return false;
        GameStartMessage that = (GameStartMessage) o;
        return teamSize == that.teamSize &&
                Objects.equals(playerGuid, that.playerGuid) &&
                team == that.team &&
                Objects.equals(teamRole, that.teamRole) &&
                Objects.equals(teamGuids, that.teamGuids) &&
                Objects.equals(messagePosition, that.messagePosition) &&
                Objects.equals(messageBoard, that.messageBoard);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), playerGuid, team, teamRole, teamSize, teamGuids, messagePosition, messageBoard);
    }

    public String getPlayerGuid() {
        return playerGuid;
    }

    public TeamColorMessageEnum getTeam() {
        return team;
    }

    public String getTeamRole() {
        return teamRole;
    }

    public int getTeamSize() {
        return teamSize;
    }

    public List<String> getTeamGuids() {
        return teamGuids;
    }

    public MessagePosition getMessagePosition() {
        return messagePosition;
    }

    public MessageBoard getMessageBoard() {
        return messageBoard;
    }
}
