package com.se2.communication.messages;


import com.google.gson.annotations.SerializedName;
import com.se2.communication.Visitor;
import com.se2.communication.classes.MessagePosition;
import com.se2.communication.enums.ActionMessageEnum;
import com.se2.communication.enums.DirectionMessageEnum;
import com.se2.communication.enums.StatusMessageEnum;

import java.util.Objects;

public class MoveMessageReturn extends MessageReturn {

    private String playerGuid;
    @SerializedName(value = "direction", alternate = {"directionMessageEnum"})
    private DirectionMessageEnum directionMessageEnum;
    @SerializedName(value = "position", alternate = {"messagePosition"})
    private MessagePosition messagePosition;

    public MoveMessageReturn(StatusMessageEnum statusMessageEnum, String playerGuid, DirectionMessageEnum directionMessageEnum, MessagePosition messagePosition) {
        super(ActionMessageEnum.move, statusMessageEnum);
        this.playerGuid = playerGuid;
        this.directionMessageEnum = directionMessageEnum;
        this.messagePosition = messagePosition;
    }

    @Override
    public Message accept(Visitor visitor) {
        return visitor.visit(this);
    }

    public String getPlayerGuid() {
        return playerGuid;
    }

    public DirectionMessageEnum getDirectionMessageEnum() {
        return directionMessageEnum;
    }

    public MessagePosition getMessagePosition() {
        return messagePosition;
    }

    @Override
    public String toString() {
        return "MoveMessageReturn{" +
                "playerUUID='" + playerGuid + '\'' +
                ", directionMessageEnum=" + directionMessageEnum +
                ", messagePosition=" + messagePosition +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MoveMessageReturn)) return false;
        if (!super.equals(o)) return false;
        MoveMessageReturn that = (MoveMessageReturn) o;
        return Objects.equals(playerGuid, that.playerGuid) &&
                directionMessageEnum == that.directionMessageEnum &&
                Objects.equals(messagePosition, that.messagePosition);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), playerGuid, directionMessageEnum, messagePosition);
    }
}
