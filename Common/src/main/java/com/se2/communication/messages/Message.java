package com.se2.communication.messages;

import com.se2.communication.Visitable;
import com.se2.communication.Visitor;
import com.se2.communication.enums.ActionMessageEnum;

import java.util.Objects;

public class Message implements Visitable {

    public final ActionMessageEnum action;

    Message(ActionMessageEnum action) {
        this.action = action;
    }

    @Override
    public String toString() {
        return "AbstractMessage{" +
                "actionMessageEnum=" + action +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Message)) return false;
        Message that = (Message) o;
        return action == that.action;
    }

    @Override
    public int hashCode() {
        return Objects.hash(action);
    }

    @Override
    public Message accept(Visitor visitor) {
        return visitor.visit(this);
    }
}
