package com.se2.communication.messages;

import com.se2.communication.Visitor;
import com.se2.communication.enums.ActionMessageEnum;
import com.se2.communication.enums.StatusMessageEnum;

import java.util.Objects;

public class TestMessageReturn extends MessageReturn {

    private String playerGuid;
    private Boolean test;

    public TestMessageReturn(StatusMessageEnum statusMessageEnum, String playerGuid, Boolean test) {
        super(ActionMessageEnum.test, statusMessageEnum);
        this.playerGuid = playerGuid;
        this.test = test;
    }

    public String getPlayerGuid() {
        return playerGuid;
    }

    public Boolean getTest() {
        return test;
    }

    @Override
    public Message accept(Visitor visitor) {
        return visitor.visit(this);
    }

    @Override
    public String toString() {
        return "TestMessageReturn{" +
                "playerGuid='" + playerGuid + '\'' +
                ", test=" + test +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TestMessageReturn)) return false;
        if (!super.equals(o)) return false;
        TestMessageReturn that = (TestMessageReturn) o;
        return Objects.equals(playerGuid, that.playerGuid) &&
                Objects.equals(test, that.test);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), playerGuid, test);
    }
}
