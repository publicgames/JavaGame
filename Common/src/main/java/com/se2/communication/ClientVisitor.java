package com.se2.communication;

import com.se2.communication.messages.Message;

public interface ClientVisitor extends Visitor {
    public boolean gameNotOver();

    public Message getMessageForServer();
}
