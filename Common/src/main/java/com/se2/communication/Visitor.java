package com.se2.communication;

import com.se2.communication.messages.*;

public interface Visitor {
    public Message visit(Message message);

    public Message visit(MessageReturn message);

    public Message visit(ConnectPlayerMessage message);

    public Message visit(ConnectPlayerReturnMessage message);

    public Message visit(DiscoverMessage message);

    public Message visit(DiscoverMessageReturn message);

    public Message visit(EndMessage message);

    public Message visit(GameStartMessage message);

    public Message visit(MoveMessage message);

    public Message visit(MoveMessageReturn message);

    public Message visit(PickUpMessage message);

    public Message visit(PickUpMessageReturn message);

    public Message visit(PlaceMessage message);

    public Message visit(PlaceMessageReturn message);

    public Message visit(TestMessage message);

    public Message visit(TestMessageReturn message);
}
