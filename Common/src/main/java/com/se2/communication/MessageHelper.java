package com.se2.communication;

import com.google.gson.Gson;
import com.se2.communication.enums.ActionMessageEnum;
import com.se2.communication.enums.StatusMessageEnum;
import com.se2.communication.messages.*;

import java.lang.reflect.Type;

public class MessageHelper {

    static Gson gson = new Gson();

    private static Type establishType(ActionMessageEnum action, StatusMessageEnum status) {
        switch (action) {
            case connect:
                return null == status ? ConnectPlayerMessage.class : ConnectPlayerReturnMessage.class;
            case discover:
                return null == status ? DiscoverMessage.class : DiscoverMessageReturn.class;
            case end:
                return null == status ? EndMessage.class : EndMessage.class; //as Team C is sending status
            case start:
                return null == status ? GameStartMessage.class : GameStartMessage.class; //as Team C is sending status
            case move:
                return null == status ? MoveMessage.class : MoveMessageReturn.class;
            case pickup:
                return null == status ? PickUpMessage.class : PickUpMessageReturn.class;
            case place:
                return null == status ? PlaceMessage.class : PlaceMessageReturn.class;
            case test:
                return null == status ? TestMessage.class : TestMessageReturn.class;
            default:
                System.out.println("Type not recognized. action=" + action);
                return null;
        }
    }

    public static Message getMessageFromJson(String message) {
        MessageReturn tmp = gson.fromJson(message, MessageReturn.class);
        if (null != tmp) {
            Type type = MessageHelper.establishType(tmp.action, tmp.status);
            return null == type ? null : gson.fromJson(message, type);
        } else {
            return null;
        }

    }

    public static String getJsonFromMessage(Object toSendBack) {
        return gson.toJson(toSendBack, toSendBack.getClass()) + "\n";
    }
}
