package com.se2.communication;

import com.se2.communication.messages.Message;

public interface Visitable {
    public Message accept(Visitor visitor);
}
