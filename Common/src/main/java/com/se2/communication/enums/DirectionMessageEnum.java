package com.se2.communication.enums;

public enum DirectionMessageEnum {
    Up,
    Down,
    Left,
    Right
}
