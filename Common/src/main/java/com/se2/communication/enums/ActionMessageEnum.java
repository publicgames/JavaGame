package com.se2.communication.enums;

public enum ActionMessageEnum {
    setup,
    connect,
    ready,
    start,
    move,
    pickup,
    test,
    place,
    end,
    discover
}
