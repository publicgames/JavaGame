package com.se2.communication.enums;

public enum PlacementResultEnum {
    Correct,
    Pointless
}
