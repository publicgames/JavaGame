package com.se2.communication.classes;

public class MessagePosition {
    private int x;
    private int y;

    public MessagePosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessagePosition that = (MessagePosition) o;

        if (x != that.x) return false;
        return y == that.y;

    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return "MessagePosition{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
