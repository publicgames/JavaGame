package com.se2.communication.classes;

import com.google.gson.annotations.SerializedName;

public class MessageFields {
    @SerializedName(value = "position", alternate = {"messagePosition"})
    private MessagePosition messagePosition;
    @SerializedName(value = "cell", alternate = {"messageCell"})
    private MessageCell messageCell;

    public MessageFields(MessagePosition messagePosition, MessageCell messageCell) {
        this.messagePosition = messagePosition;
        this.messageCell = messageCell;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessageFields that = (MessageFields) o;

        if (!messagePosition.equals(that.messagePosition)) return false;
        return messageCell.equals(that.messageCell);

    }

    public MessagePosition getMessagePosition() {
        return messagePosition;
    }

    public MessageCell getMessageCell() {
        return messageCell;
    }

    @Override
    public String toString() {
        return "MessageFields{" +
                "messagePosition=" + messagePosition +
                ", messageCell=" + messageCell +
                '}';
    }
}
