package com.se2.communication.classes;

import java.util.Objects;

public class MessageBoard {
    private int boardWidth;
    private int taskAreaHeight;
    private int goalAreaHeight;

    public MessageBoard(int boardWidth, int taskAreaHeight, int goalAreaHeight) {
        this.boardWidth = boardWidth;
        this.taskAreaHeight = taskAreaHeight;
        this.goalAreaHeight = goalAreaHeight;
    }

    public int getBoardWidth() {
        return boardWidth;
    }

    public int getTaskAreaHeight() {
        return taskAreaHeight;
    }

    public int getGoalAreaHeight() {
        return goalAreaHeight;
    }

    @Override
    public String toString() {
        return "MessageBoard{" +
                "boardWidth=" + boardWidth +
                ", taskAreaHeight=" + taskAreaHeight +
                ", goalAreaHeight=" + goalAreaHeight +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MessageBoard)) return false;
        MessageBoard that = (MessageBoard) o;
        return boardWidth == that.boardWidth &&
                taskAreaHeight == that.taskAreaHeight &&
                goalAreaHeight == that.goalAreaHeight;
    }

    @Override
    public int hashCode() {
        return Objects.hash(boardWidth, taskAreaHeight, goalAreaHeight);
    }
}
