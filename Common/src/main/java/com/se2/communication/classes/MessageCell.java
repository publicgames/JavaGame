package com.se2.communication.classes;

import com.se2.communication.enums.CellStateMessageEnum;

import java.util.Objects;

public class MessageCell {
    private CellStateMessageEnum cellState;
    private int distance;
    private String playerGuid;

    public MessageCell(CellStateMessageEnum cellState, int distance, String playerGuid) {
        this.cellState = cellState;
        this.distance = distance;
        this.playerGuid = playerGuid;
    }


    @Override
    public int hashCode() {
        return Objects.hash(cellState, distance, playerGuid);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MessageCell)) return false;
        MessageCell that = (MessageCell) o;
        return distance == that.distance &&
                cellState == that.cellState &&
                Objects.equals(playerGuid, that.playerGuid);
    }

    public CellStateMessageEnum getCellState() {
        return cellState;
    }

    public int getDistance() {
        return distance;
    }

    public String getPlayerGuid() {
        return playerGuid;
    }

    @Override
    public String toString() {
        return "MessageCell{" +
                "cellState=" + cellState +
                ", distance=" + distance +
                ", playerGuid='" + playerGuid + '\'' +
                '}';
    }
}
