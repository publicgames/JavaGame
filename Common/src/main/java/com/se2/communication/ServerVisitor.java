package com.se2.communication;

import com.se2.communication.messages.Message;

public interface ServerVisitor extends Visitor {
    public boolean gameNotOver();

    public Message getMessageFor(String playerGuid);
}
