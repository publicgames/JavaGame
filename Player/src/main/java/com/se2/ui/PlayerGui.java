package com.se2.ui;

import com.se2.communication.enums.DirectionMessageEnum;
import com.se2.game.board.Cell;
import com.se2.player.Player;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

import static com.se2.ui.CommonParts.setBasicOptions;

public class PlayerGui extends BoardGui {
    private static int optionsPanelWidth = 125;
    JLabel jLabel;
    private Player player;

    public PlayerGui(Player player) {
        super(player.getBoard(), player.getName() + " " + player.getPlayerGuid(), optionsPanelWidth);
        this.player = player;
        this.board = null;
    }

    public void paint(Graphics g) {
        if ((this.board == null) && (null != player.getBoard())) {
            this.board = player.getBoard();
            boardWidth = board.getBoardWidth();
            boardHeight = board.getBoardHeight();
            return;
        }
        calculateWidthAndHeight(optionsPanelWidth);
        if (jLabel != null)
            jLabel.setText(player.getInfo());

        for (int y = 0; y < boardHeight; y++) {
            for (int x = 0; x < boardWidth; x++) {
                Cell cell = this.board.getCell(x, y);

                processAndDrawCell(g, cell, x, y);

                if (cell.getPawnGuid() != null) {
                    String playerGuid = cell.getPawnGuid();
                    if (playerGuid.equals(player.getPlayerGuid())) {
                        drawPlayer(g, player, x, y);
                    } else {
                        drawPlayer(g, null, x, y);
                    }
                }
            }
        }
        drawMesh(g);
    }

    public void start() {
        if (thread == null) {
            JFrame window = new JFrame();
            this.window = window;
            setBasicOptions(window, this, title);
            thread = new Thread(this);
            thread.start();

            JSplitPane splitPane = new JSplitPane();
            this.window.getContentPane().add(splitPane, BorderLayout.EAST);

            JPanel jPanel = new JPanel(new FlowLayout()); //JPanel containing all buttons
            jPanel.setPreferredSize(new Dimension(optionsPanelWidth, 1700));
            splitPane.setLeftComponent(null);
            splitPane.setRightComponent(jPanel);
            this.setPreferredSize(new Dimension(window.getWidth() - optionsPanelWidth, 1700));

            splitPane.setDividerLocation(0.8);
            JLabel label_1 = new JLabel("Options");
            jPanel.add(label_1);
            addButtons(jPanel);
        }
    }

    private void addButtons(JPanel jPanel) {
        Dimension preferredSize = new Dimension(125, 25);

        jLabel = new JLabel(player.getInfo(), SwingConstants.CENTER);
        jLabel.setPreferredSize(preferredSize);
        Border border = BorderFactory.createLineBorder(Color.BLACK, 1);

        jLabel.setBorder(border);
        jPanel.add(jLabel);

        JButton btnUp = new JButton("Move up");
        btnUp.addActionListener(e -> {
            player.postMoveMessage(DirectionMessageEnum.Up);
        });
        btnUp.setPreferredSize(preferredSize);
        jPanel.add(btnUp);

        JButton btnDown = new JButton("Move down");
        btnDown.addActionListener(e -> {
            player.postMoveMessage(DirectionMessageEnum.Down);
        });
        btnDown.setPreferredSize(preferredSize);
        jPanel.add(btnDown);

        JButton btnLeft = new JButton("Move left");
        btnLeft.addActionListener(e -> {
            player.postMoveMessage(DirectionMessageEnum.Left);
        });
        btnLeft.setPreferredSize(preferredSize);
        jPanel.add(btnLeft);

        JButton btnRight = new JButton("Move right");
        btnRight.addActionListener(e -> {
            player.postMoveMessage(DirectionMessageEnum.Right);
        });
        btnRight.setPreferredSize(preferredSize);
        jPanel.add(btnRight);

        JButton btnDiscover = new JButton("Discover");
        btnDiscover.addActionListener(e -> {
            player.postDiscoverMessage();
        });
        btnDiscover.setPreferredSize(preferredSize);
        jPanel.add(btnDiscover);

        JButton btnPickup = new JButton("Pickup");
        btnPickup.addActionListener(e -> {
            player.postPickupMessage();
        });
        btnPickup.setPreferredSize(preferredSize);
        jPanel.add(btnPickup);

        JButton btnPlacePiece = new JButton("Place piece");
        btnPlacePiece.addActionListener(e -> {
            player.postPlaceMessage();
        });
        btnPlacePiece.setPreferredSize(preferredSize);
        jPanel.add(btnPlacePiece);

        JButton btnTestPiece = new JButton("Test");
        btnTestPiece.addActionListener(e -> {
            player.postTestPieceMessage();
        });
        btnTestPiece.setPreferredSize(preferredSize);
        jPanel.add(btnTestPiece);
    }
}