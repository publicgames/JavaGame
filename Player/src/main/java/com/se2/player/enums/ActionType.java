package com.se2.player.enums;

public enum ActionType {
    Move,
    Pickup,
    Test,
    Place,
    Destroy,
    Send
}
