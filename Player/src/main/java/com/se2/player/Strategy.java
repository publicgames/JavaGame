package com.se2.player;

import com.se2.communication.enums.DirectionMessageEnum;
import com.se2.communication.enums.StatusMessageEnum;
import com.se2.game.board.Cell;
import com.se2.game.board.Piece;
import com.se2.game.board.Position;
import com.se2.game.player.enums.PlayerState;

import java.util.List;

import static com.se2.tools.ProcessorDefender.sleepPlease;

public class Strategy {
    private int waitingTime = 50;

    Player me;
    Position initialPosition;
    boolean foundPiece; // true when manhattan distance to the piece is 0
    Position toPlacePiece;
    boolean imRed; //meaning the player started on the bottom of the board
    boolean imBlue;

    public Strategy(Player player) {
        this.me = player;
        this.initialPosition = me.getPosition();
        this.toPlacePiece = initialPosition;
        this.foundPiece = false;
    }

    public void play() {
        getColor();
        while (me.getPlayerState().equals(PlayerState.Active)) {
            if (!me.getBoard().isInTaskArea(me.getPosition()))
                goToTaskArea();
            if (me.getBoard().isInTaskArea(me.getPosition()) && foundPiece == false)
                findPiece();
            Piece k = me.getBoard().getCell(me.getPosition()).getPiece(); // the cell on which im standing
            if (foundPiece == true)
                pickUpPiece();
            //else foundPiece = false;
            if (me.getPiece() != null)
                goAndPlacePiece();
        }

    }

    public void getColor() {
        int y = me.getPosition().getY();
        int goalAreaHeight = me.getBoard().getGoalAreaHeight();
        int taskAreaHeight = me.getBoard().getTaskAreaHeight();
        if (y < goalAreaHeight)
            imBlue = true; // im on the top of the board
        else if (y > goalAreaHeight + taskAreaHeight - 1)
            // im on the bottom of the board
            imRed = true;
    }

    public void goToTaskArea() {
        int y = me.getPosition().getY();
        int goalAreaHeight = me.getBoard().getGoalAreaHeight();
        int taskAreaHeight = me.getBoard().getTaskAreaHeight();
        if (y < goalAreaHeight) {
            // im on the top, go down
            int steps = goalAreaHeight - y;
            for (int i = 0; i < steps; i++) {
                moveDown();
            }
        } else if (y > goalAreaHeight + taskAreaHeight - 1) {
            // im on the bottom, go up
            int steps = y - goalAreaHeight - taskAreaHeight + 1;
            for (int i = 0; i < steps; i++) {
                moveUp();
            }
        }
    }

    public Cell discover() {
        me.postDiscoverMessage();
        // wait for an answer
        while (me.responseForDiscover == null)
            sleepPlease(waitingTime);
        if (me.responseForDiscover == StatusMessageEnum.OK) {

            List<Cell> neighbouringCells = me.getBoard().getNeighbouringCells(me.getPosition());
            int smallestDistance = 100; // FIXME arbitrary big enough magic number
            Cell closestCellWithPiece = null;
            for (Cell cell : neighbouringCells) {
                int distance = cell.getDistance();
                if (distance < smallestDistance) {
                    smallestDistance = distance;
                    closestCellWithPiece = cell;

                    if (smallestDistance == 0) {
                        foundPiece = true;
                        return closestCellWithPiece;
                    }
                }
            }
            return closestCellWithPiece;
        }
        return null;
    }

    /**
     * goes to the specified position
     */
    public void moveToPosition(Position position) {
        int stepsX = position.getX() - me.getPosition().getX();
        if (stepsX > 0) {//move right
            for (int i = 0; i < stepsX; i++) {
                moveRight();
            }
        } else if (stepsX < 0) { //move left
            for (int i = 0; i < Math.abs(stepsX); i++) {
                moveLeft();
            }
        }

        int stepsY = position.getY() - me.getPosition().getY();
        if (stepsY > 0) {//move down
            for (int i = 0; i < stepsY; i++) {
                moveDown();
            }
        } else if (stepsY < 0) { //move up
            for (int i = 0; i < Math.abs(stepsY); i++) {
                moveUp();
            }
        }
        for (int i = 0; i < 3; i++) { // try to go to your desired position only 3 times
            if (!me.getPosition().equals(position))
                moveToPosition(position);
        }

    }

    public void moveLeft() {
        me.postMoveMessage(DirectionMessageEnum.Left);
        while (me.responseForMove == null)
            sleepPlease(waitingTime);
        if (me.responseForMove == StatusMessageEnum.DENIED)
            moveRight();
    }

    public void moveRight() {
        me.postMoveMessage(DirectionMessageEnum.Right);
        while (me.responseForMove == null)
            sleepPlease(waitingTime);
        if (me.responseForMove != StatusMessageEnum.OK)
            moveDown();
    }

    public void moveDown() {
        me.postMoveMessage(DirectionMessageEnum.Down);
        while (me.responseForMove == null)
            sleepPlease(waitingTime);
        if (me.responseForMove != StatusMessageEnum.OK)
            moveUp();

    }

    public void moveUp() {
        me.postMoveMessage(DirectionMessageEnum.Up);
        while (me.responseForMove == null)
            sleepPlease(waitingTime);
        if (me.responseForMove != StatusMessageEnum.OK)
            moveLeft();

    }

    public void findPiece() {
        while (foundPiece == false) {
            Cell closestCellWithPiece = discover();
            // goes to the neighbouring cell with smallest manhattan distance (one of)
            moveToPosition(closestCellWithPiece.getPosition());
        }

    }

    public void pickUpPiece() {
        me.postPickupMessage();
        while (me.responseForPickUp == null)
            sleepPlease(waitingTime);
        if (me.responseForPickUp == StatusMessageEnum.DENIED) { // if pick up was denied
            foundPiece = false;
            // go far away -> flip position
            moveToPosition(flipPosition());
        } else if (me.responseForPickUp == StatusMessageEnum.OK) {
            me.postTestPieceMessage();
            while (me.responseForTestPiece == null)
                sleepPlease(waitingTime);
            if (me.getPiece() == (null)) { // if piece disappeared after testing
                foundPiece = false;
            }
        }


    }

    public Position flipPosition() {
        int meX = me.getPosition().getX();
        int meY = me.getPosition().getY();
        // if width is odd get center cell, if width is even get cell to the right of center axis
        int centerX = me.getBoard().getBoardWidth() / 2;
        // if height is even get cell down from the center axis
        int centerY = me.getBoard().getTaskAreaHeight() / 2 + me.getBoard().getGoalAreaHeight();

        int nextX = centerX - meX + centerX;
        int nextY = centerY - meY + centerY;

        Position flipped = new Position(nextX, nextY);
        return flipped;
    }


    public void goAndPlacePiece() {
        moveToPosition(toPlacePiece);
        me.postPlaceMessage(); // place a piece
        while (me.responseForPlacePiece == null)
            sleepPlease(waitingTime);
        if (me.responseForPlacePiece == StatusMessageEnum.OK) {
            foundPiece = false;
            updateToPlacePiece();
        }
    }

    public void updateToPlacePiece() {
        if (imBlue) {
            if (toPlacePiece.getY() < me.getBoard().getTaskAreaHeight() - 1)
                toPlacePiece = new Position(toPlacePiece.getX(), toPlacePiece.getY() + 1);
            else // start filling a new column
                toPlacePiece = new Position(toPlacePiece.getX() + me.getTeam().getSize(), initialPosition.getY());
        }
        if (imRed) {
            if (toPlacePiece.getY() > me.getBoard().getGoalAreaHeight() + me.getBoard().getTaskAreaHeight())
                toPlacePiece = new Position(toPlacePiece.getX(), toPlacePiece.getY() - 1);
            else // start filling a new column
                toPlacePiece = new Position(toPlacePiece.getX() - me.getTeam().getSize(), initialPosition.getY());
        }
    }

}
