package com.se2.player;

import com.se2.communication.ClientVisitor;
import com.se2.communication.MessageHelper;
import com.se2.communication.messages.Message;
import com.se2.tools.Logger;

import java.io.*;
import java.net.Socket;

import static com.se2.tools.ProcessorDefender.sleepPlease;

public class PlayerClient extends Thread {
    private Socket s;
    private ClientVisitor visitor;
    private BufferedReader inputStream;
    private BufferedWriter outputStream;

    public PlayerClient(String ip, int port, ClientVisitor visitor) {
        try {
            this.s = new Socket(ip, port);
            this.visitor = visitor;
            this.inputStream = new BufferedReader(new InputStreamReader(s.getInputStream()));
            this.outputStream = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
        } catch (IOException e) {
            System.out.println("Client unable to start");
            e.printStackTrace();
        }
    }

    public void run() {
        while (visitor.gameNotOver()) {
            try {
                Object anythingToSendToServer = visitor.getMessageForServer();
                if (null != anythingToSendToServer) {
                    Logger.log("Message to be sent: " + anythingToSendToServer);
                    String jsonToSend = MessageHelper.getJsonFromMessage(anythingToSendToServer);
                    System.out.println("\nSending out message:" + jsonToSend);
                    Logger.log("Sending out message: " + jsonToSend);
                    outputStream.write(jsonToSend);
                    outputStream.flush();
                    Logger.log("Sent message: " + jsonToSend);
                }
                if (inputStream.ready()) {
                    String message = inputStream.readLine();
                    System.out.println("\nReceived message:" + message);
                    Message received = MessageHelper.getMessageFromJson(message);
                    if (null == received) {
                        Logger.log("Received message NOT recognised!");
                    } else {
                        Logger.log("Received message: " + received.toString());
                        Object toSendBack = received.accept(visitor);
                        Logger.log("Respond message: " + toSendBack);
                        if (null != toSendBack) {
                            String jsonToSend = MessageHelper.getJsonFromMessage(toSendBack);
                            outputStream.write(jsonToSend);
                            outputStream.flush();
                        }
                    }
                }
            } catch (IOException ex) {
                Logger.log("ERROR: While receiving message got IOException: " + ex);
                Logger.log("Player lost connection to Communication server. Closing...");
                System.out.println("Player lost connection to Communication server. Closing...");
                System.exit(1);
            }
            sleepPlease();
        }

        try {
            System.out.println("Socket Closing");
            s.close();
        } catch (IOException ex) {
            Logger.log("ERROR: While closing socket got IOException: " + ex);
            System.exit(1);
        }
    }

}
