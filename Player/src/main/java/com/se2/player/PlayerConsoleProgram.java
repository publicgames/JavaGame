package com.se2.player;

import com.se2.player.configuration.PlayerConfiguration;
import com.se2.tools.Logger;
import com.se2.ui.PlayerGui;
import org.apache.commons.cli.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class PlayerConsoleProgram {

    public static Player player;
    public static PlayerProperties properties;
    public static PlayerConfiguration configuration;
    private static CommandLine cmd;

    public static void main(String[] args) {
        loadConfiguration(args);
        //Start logger
        Logger.start(configuration.verbose, configuration.loggerFile);
        Logger.log(configuration.toString());

        player = new Player(configuration);

        new PlayerClient(configuration.ipAddress, configuration.portNumber, player).start();

        player.startOneGame();

        //open gui
        PlayerGui boardGui = new PlayerGui(player);
        boardGui.start();
        System.out.println("Player Started");
    }

    private static void loadConfiguration(String[] argv) {
        setUpPropertiesToBeUsed();
        processCommandLine(argv, properties);
        Properties prop = processChoosenConfigFile(properties);
        establishFinalConfiguration(prop);
        configuration = properties.getPlayerConfiguration();
    }

    public static void setUpPropertiesToBeUsed() {
        properties = new PlayerProperties();
    }

    private static void processCommandLine(String[] args, PlayerProperties properties) {
        properties.addCommandLineOptions();

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        cmd = null;

        try {
            cmd = parser.parse(properties.getOptions(), args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("Player", properties.getOptions());
            System.exit(1);
        }

        if (cmd.hasOption("help")) {
            formatter.printHelp("Player", properties.getOptions());
            System.exit(1);
        }
        assignValues(cmd);
    }

    private static void assignValues(CommandLine cmd) {
        for (Option option : cmd.getOptions()) {
            if (option.hasArg()) {
                PlayerProperties.valuesOfProperties.put(option.getArgName(), option.getValue());
            } else {
                PlayerProperties.valuesOfProperties.put(option.getArgName(), "Y");
            }
        }
    }

    private static Properties processChoosenConfigFile(PlayerProperties properties) {
        String fileToLoadFrom = PlayerProperties.valuesOfProperties.getOrDefault(PlayerProperties.CONFIGURATION_FILE, PlayerProperties.DEFAULT_CONFIGURATION_FILE);
        Properties prop = new Properties();
        try {
            InputStream is = null;
            is = ClassLoader.getSystemResourceAsStream(fileToLoadFrom);
            if (null == is) {
                System.out.println("Configuration file not found in resources: " + fileToLoadFrom);
                //try to upload from directory
                is = new FileInputStream(fileToLoadFrom);
                if (null == is) {
                    System.out.println("Configuration file not found in directory: " + fileToLoadFrom);
                    System.exit(1);
                }
            }
            prop.load(is);
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
            System.out.println("File not found: " + fileToLoadFrom);
            System.exit(1);
        } catch (IOException e) {
            System.out.println(e.getMessage());
            System.out.println("Exception IOException when trying to load content of: " + fileToLoadFrom);
            System.exit(1);
        }
        return prop;
    }

    private static void establishFinalConfiguration(Properties prop) {
        List<String> configurationToFill = new ArrayList<String>(PlayerProperties.LIST_OF_PROPERTIES);
        configurationToFill.removeAll(PlayerProperties.valuesOfProperties.keySet());
        List<String> filled = new ArrayList<String>();
        for (String configurable : configurationToFill) {
            PlayerProperties.valuesOfProperties.put(configurable, (String) prop.get(configurable));
            filled.add(configurable);
        }
        configurationToFill.removeAll(filled);
        if (!configurationToFill.isEmpty()) {
            System.out.println("Not not set: " + configurationToFill);
        }
    }
}
