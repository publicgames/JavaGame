package com.se2.player;

import com.se2.communication.enums.StatusMessageEnum;
import com.se2.game.board.Board;
import com.se2.game.board.Cell;
import com.se2.game.board.Position;
import com.se2.game.board.enums.Direction;
import com.se2.game.player.enums.PlayerState;
import com.se2.tools.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.se2.tools.ConvertersUtils.convertDirectionToMessageDirection;
import static com.se2.tools.ProcessorDefender.sleepPlease;
import static java.lang.Math.abs;

public class StrategyWuCash {

    Player me;
    boolean top;
    int playerNumber = -1; //0-3
    List<Position> goalTocheck = new ArrayList<>();
    private int waitingTime = 10;
    private boolean last = false; //false go up down first
    private Position destination = null;
    private int failed;


    public StrategyWuCash(Player player) {
        this.me = player;
    }

    public void play() {
        Board board = me.getBoard();

        //starting from top or bottom?
        top = me.getPosition().getY() < board.getGoalAreaHeight();
        //counting from corners we assign number 1,2,3,4 to a player
        playerNumber = top ? me.getPosition().getX() : board.getBoardWidth() - 1 - me.getPosition().getX();
        Logger.log("I am top:" + top + " and my number is:" + playerNumber);

        //if I am player 3 or 4 I have special job to reach middle and pick up initial pieces.
        if (playerNumber == 4) {
            goTo(top ? new Position(10, 10) : new Position(9, 9), true, false);
        } else if (playerNumber == 3) {
            goTo(top ? new Position(9, 9) : new Position(10, 10), false, false);
        }

        if (new Position(9, 9).equals(me.getPosition()) || new Position(10, 10).equals(me.getPosition())) {
            pickUpPiece();
        }

        setGoalsToCheck();

        while (PlayerState.Active.equals(me.getPlayerState())) {
            if (null != me.getPiece()) {
                //I have a real piece - carry it to next good place...
                Position goingTo = getGoal(failed);
                goTo(goingTo, last, true);
                if (goingTo.equals(me.getPosition())) {
                    failed = 0;
                    me.postPlaceMessage();
                    while (me.responseForPlacePiece == null)
                        sleepPlease(waitingTime);
                    if (me.responseForPlacePiece.equals(StatusMessageEnum.DENIED)) {
                        //something very wrong...
                        Logger.error("Got Denied on place action." + me.placeMessageReturn);
                        sleepPlease(1000);
                    } else {
                        goalTocheck.remove(goingTo);
                    }
                } else {
                    failed++;
                    last = !last;
                }
            } else {
                //I don't have piece.
                Position myPosition = me.getPosition();
                if (board.isInTaskArea(myPosition)) {
                    me.postDiscoverMessage();
                    while (me.responseForDiscover == null)
                        sleepPlease(waitingTime);
                    if (me.responseForDiscover.equals(StatusMessageEnum.DENIED)) {
                        //not standing where i think i am standing... wait for responses
                        sleepPlease(1000);
                    } else {
                        //establish what now, look under feet
                        Cell myCell = board.getCell(me.getPosition());
                        int distance = myCell.getDistance();
                        if (-1 == distance) {
                            //no pieces to found
                            failed = 0;
                            Position goingTo = getPlayerCorner(failed);
                            goTo(goingTo, last, true);
                            continue;
                        } else if (0 == distance) {
                            //lucky, standing over piece.
                            failed = 0;
                            pickUpPiece();
                            if (null != me.getPiece()) {
                                continue;
                            }
                        }

                        //where to go ?
                        int signX;//+ go right if can
                        int signY;//+ go up if can
                        if (me.getPosition().getX() - 1 < 0) {
                            //we are next to boarder.
                            Cell toTheRight = board.getCell(new Position(me.getPosition().getX() + 1, me.getPosition().getY()));
                            signX = (distance - toTheRight.getDistance());
                            //unable to move left - so only up / down
                            signX = Math.max(signX, 0);
                        } else if (me.getPosition().getX() + 1 == board.getBoardWidth()) {
                            //we are next to boarder.
                            Cell toTheLeft = board.getCell(new Position(me.getPosition().getX() - 1, me.getPosition().getY()));
                            signX = (toTheLeft.getDistance() - distance);
                            //unable to move right - so only up / down
                            signX = Math.min(signX, 0);
                        } else {
                            //we are somewhere in the middle
                            Cell toTheLeft = board.getCell(new Position(me.getPosition().getX() - 1, me.getPosition().getY()));
                            Cell toTheRight = board.getCell(new Position(me.getPosition().getX() + 1, me.getPosition().getY()));
                            if (0 == toTheLeft.getDistance()) {
                                goTo(toTheLeft.getPosition(), last, true);
                                continue;
                            }
                            if (0 == toTheRight.getDistance()) {
                                goTo(toTheRight.getPosition(), last, true);
                                continue;
                            }
                            if (toTheLeft.getDistance() == toTheRight.getDistance()) {
                                //we are in correct column - so only up / down
                                signX = 0;
                            } else {
                                signX = (toTheLeft.getDistance() - toTheRight.getDistance());
                            }
                        }

                        if (me.getPosition().getY() == board.getGoalAreaHeight()) {
                            //we are next to boarder of task area
                            Cell toTheBottom = board.getCell(new Position(me.getPosition().getX(), me.getPosition().getY() + 1));
                            signY = (distance - toTheBottom.getDistance());
                            //unable to move up - so only left / right
                            signY = Math.max(signY, 0);
                        } else if (me.getPosition().getY() + 1 == board.getGoalAreaHeight() + board.getTaskAreaHeight()) {
                            //we are next to boarder of task area.
                            Cell toTheTop = board.getCell(new Position(me.getPosition().getX(), me.getPosition().getY() - 1));
                            signY = (toTheTop.getDistance() - distance);
                            //unable to move down - so only left / right
                            signY = Math.min(signY, 0);
                        } else {
                            //we are somewhere in the middle
                            Cell toTheBottom = board.getCell(new Position(me.getPosition().getX(), me.getPosition().getY() + 1));
                            Cell toTheTop = board.getCell(new Position(me.getPosition().getX(), me.getPosition().getY() - 1));
                            if (0 == toTheBottom.getDistance()) {
                                goTo(toTheBottom.getPosition(), last, true);
                                continue;
                            }
                            if (0 == toTheTop.getDistance()) {
                                goTo(toTheTop.getPosition(), last, true);
                                continue;
                            }
                            if (toTheBottom.getDistance() == toTheTop.getDistance()) {
                                //we are in correct row - so only left / right
                                signY = 0;
                            } else {
                                signY = (toTheTop.getDistance() - toTheBottom.getDistance());
                            }
                        }

                        //thanks to sing on signX and signY we know where to go now
                        Position goingTo;
                        if (signX == 0 && signY == 0) {
                            goingTo = new Position(me.getPosition().getX() + new Random().nextInt(2) - 1, me.getPosition().getY() + new Random().nextInt(10)-5);
                        } else {
                            int step = distance < 3 ? 2 : 1;
                            goingTo = new Position(me.getPosition().getX() + (signX / step), me.getPosition().getY() + (signY / step));
                        }
                        goTo(goingTo, last, true);
                    }
                } else {
                    //get to my corner
                    Position goingTo = getPlayerCorner(failed);
                    goTo(goingTo, last, true);
                    if (goingTo.equals(me.getPosition())) {
                        failed = 0;
                    } else {
                        failed++;
                        last = !last;
                    }
                }
            }
        }
    }

    private Position getPlayerCorner(int failed) {
        int w = me.getBoard().getBoardWidth();
        int x = (w / 4) + failed;
        int h = me.getBoard().getTaskAreaHeight();
        int y = (h / 4) + failed;
        int padding = me.getBoard().getGoalAreaHeight();

        switch (playerNumber) {
            case 0:
                return flipPosition(new Position((x) % w, (padding + y) % h), top);
            case 1:
                return flipPosition(new Position((x * 3) % w, (padding + y) % h), top);
            case 2:
                return flipPosition(new Position((x) % w, (padding + 3 * y) % h), top);
            case 3:
                return flipPosition(new Position((x * 3) % w, (padding + 3 * y) % h), top);
            default:
                Logger.error("something wrong with player number");
                return flipPosition(new Position(x + playerNumber % w, padding + y + playerNumber), top);
        }
    }

    private Position getGoal(int failed) {
        if (goalTocheck.isEmpty()) {
            System.out.println("No more goals to check!! Don't stop! Play :)");
            goalTocheck = new ArrayList<>();
            for (int i = 0; i < me.getBoard().getBoardWidth(); i++) {
                for (int j = 0; j < me.getBoard().getGoalAreaHeight(); j++) {
                    goalTocheck.add(new Position(i, j));
                }
            }
        }
        return goalTocheck.size() > failed + 1 ? goalTocheck.get(failed) : goalTocheck.get(new Random().nextInt(goalTocheck.size()));
    }

    private void setGoalsToCheck() {
        int goalAreaSize = me.getBoard().getBoardWidth() * me.getBoard().getGoalAreaHeight();
        for (int i = 0; i < goalAreaSize / 8; i++) {
            Position position = new Position(playerNumber * 5 + i, 0);
            position = flipPosition(position, top);
            goalTocheck.add(position);
        }
        for (int i = 0; i < goalAreaSize / 8; i++) {
            Position position = new Position(playerNumber * 5 + i, 1);
            position = flipPosition(position, top);
            goalTocheck.add(position);
        }

        for (int i = (goalAreaSize / 8) - 1; i > -1; i--) {
            for (int j = 1; j < 4; j++) {
                Position position = new Position(((playerNumber + j) % 4) * 5 + i, 1);
                position = flipPosition(position, top);
                goalTocheck.add(position);
            }
        }
        for (int i = (goalAreaSize / 8) - 1; i > -1; i--) {
            for (int j = 1; j < 4; j++) {
                Position position = new Position(((playerNumber + j) % 4) * 5 + i, 0);
                position = flipPosition(position, top);
                goalTocheck.add(position);
            }
        }
        Logger.log("My goal are:" + goalTocheck);
    }

    private Position flipPosition(Position position, boolean top) {
        return top ? position : new Position(me.getBoard().getBoardWidth() - position.getX() - 1, me.getBoard().getBoardHeight() - position.getY() - 1);
    }

    private void goTo(Position destination, boolean side, boolean slow) {
        Position followPosition = new Position(me.getPosition().getX(), me.getPosition().getY());
        int sideMove = destination.getX() - me.getPosition().getX();
        int frontMove = destination.getY() - me.getPosition().getY();
        Direction direction;
        if (side) {
            direction = sideMove > 0 ? Direction.RIGHT : Direction.LEFT;
            move(direction, sideMove, slow);
            direction = frontMove > 0 ? Direction.DOWN : Direction.UP;
            move(direction, frontMove, slow);
        } else {
            direction = frontMove > 0 ? Direction.DOWN : Direction.UP;
            move(direction, frontMove, slow);
            direction = sideMove > 0 ? Direction.RIGHT : Direction.LEFT;
            move(direction, sideMove, slow);
        }

    }

    private void move(Direction direction, int numberOfSteps, boolean slow) {
        numberOfSteps = abs(numberOfSteps);
        for (int j = 0; j < numberOfSteps; j++) {
            me.postMoveMessage(convertDirectionToMessageDirection(direction));
            if (slow) {
                //wait for response
                while (me.moveMessageReturn == null) {
                    sleepPlease(waitingTime);
                }
                if (me.moveMessageReturn.status != StatusMessageEnum.OK) {
                    //was not able to move, failed, rethink
                    break;
                }
            }
        }
    }

    private void pickUpPiece() {
        me.postPickupMessage();
        while (me.responseForPickUp == null)
            sleepPlease(waitingTime);
        if (me.responseForPickUp == StatusMessageEnum.OK) {
            me.postTestPieceMessage();
            while (me.responseForTestPiece == null)
                sleepPlease(waitingTime);
        }
    }
}
