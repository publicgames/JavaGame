package com.se2.player;

import com.se2.player.configuration.PlayerConfiguration;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PlayerProperties {

    public static final String DEFAULT_CONFIGURATION_FILE = "default_player_config.cfg";
    public static final String CONFIGURATION_FILE = "CONFIGURATION_FILE";
    public static final String IP_TO_CONNECT_TO = "IP_TO_CONNECT_TO";
    public static final String PORT_TO_CONNECT_TO = "PORT_TO_CONNECT_TO";
    public static final String DEFAULT_LOG_FILE = ".//logs//output.txt";
    public static final String LOG_FILE = "LOG_FILE";
    public static final String NAME_TO_USE = "NAME_TO_USE";
    public static final String TRUE = "y|Y|t|T|true|TRUE|yes|YES";
    public static final String VERBOSE = "VERBOSE";
    public static final String MAX_CONNECTION_ATTEMPS = "MAX_CONNECTION_ATTEMPS";
    public static final String STRATEGY = "STRATEGY";
    public static final List<String> LIST_OF_PROPERTIES = List.of(
            //DEFAULT_CONFIGURATION_FILE,
            CONFIGURATION_FILE,
            IP_TO_CONNECT_TO,
            PORT_TO_CONNECT_TO,
            //DEFAULT_LOG_FILE,
            LOG_FILE,
            NAME_TO_USE,
            TRUE,
            VERBOSE,
            MAX_CONNECTION_ATTEMPS,
            STRATEGY
    );
    private static final String HELP = "help";
    public static Map<String, String> valuesOfProperties = new HashMap<>();
    private static Options options = new Options();

    protected static Option addCommandLineOption(String opt, String longOpt, boolean hasArg, String description) {
        Option optionToAdd = new Option(opt, longOpt.toLowerCase(), hasArg, description);
        optionToAdd.setRequired(false);
        optionToAdd.setArgName(longOpt);
        optionToAdd.setType(String.class);
        options.addOption(optionToAdd);
        return optionToAdd;
    }

    public void addCommandLineOptions() {
        addCommandLineOption("h", PlayerProperties.HELP, false, "print out this information");
        addCommandLineOption("c", PlayerProperties.CONFIGURATION_FILE, true, "configuration file with absolute path");
        addCommandLineOption("ip", PlayerProperties.IP_TO_CONNECT_TO, true, "ip to connect to");
        addCommandLineOption("n", PlayerProperties.NAME_TO_USE, true, "name for console");
        addCommandLineOption("o", PlayerProperties.LOG_FILE, true, "out log file with absolute path");
        addCommandLineOption("p", PlayerProperties.PORT_TO_CONNECT_TO, true, "port to connect to");
        addCommandLineOption("v", PlayerProperties.VERBOSE, false, "turn on verbose/debug mode").setType(Boolean.class);
        addCommandLineOption("mc", PlayerProperties.MAX_CONNECTION_ATTEMPS, false, "max connection attemps when being denied").setType(Integer.class);
        addCommandLineOption("s", PlayerProperties.STRATEGY, true, "strategy - one of: HUMAN, Karol, WuCash, Tetyana (default)").setType(String.class);
    }

    public Options getOptions() {
        return options;
    }

    public PlayerConfiguration getPlayerConfiguration() {
        return new PlayerConfiguration(
                valuesOfProperties.get(CONFIGURATION_FILE),
                valuesOfProperties.get(IP_TO_CONNECT_TO),
                Integer.parseInt(valuesOfProperties.get(PORT_TO_CONNECT_TO)),
                valuesOfProperties.get(LOG_FILE),
                valuesOfProperties.get(NAME_TO_USE),
                valuesOfProperties.get(VERBOSE).matches(TRUE),
                Integer.parseInt(valuesOfProperties.get(MAX_CONNECTION_ATTEMPS)),
                valuesOfProperties.get(STRATEGY)
        );
    }
}
