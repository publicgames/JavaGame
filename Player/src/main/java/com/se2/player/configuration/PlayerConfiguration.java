package com.se2.player.configuration;

public class PlayerConfiguration {

    public final String configurationFile;
    public final String ipAddress;
    public final int portNumber;
    public final String name;
    public final boolean verbose;
    public final String loggerFile;

    public final int maxConnectionAttemps;
    public final String strategy;

    public PlayerConfiguration(String configuration_file, String ip_to_connect_to, int port_number, String logger_file, String name_to_use, boolean verbose, int maxConnectionAttemps, String strategy) {
        this.configurationFile = configuration_file;
        this.ipAddress = ip_to_connect_to;
        this.portNumber = port_number;
        this.name = name_to_use;
        this.verbose = verbose;
        this.loggerFile = logger_file;
        this.maxConnectionAttemps = maxConnectionAttemps;
        this.strategy = strategy;
    }

    @Override
    public String toString() {
        return "PlayerConfiguration{" +
                "configurationFile='" + configurationFile + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                ", portNumber=" + portNumber +
                ", name='" + name + '\'' +
                ", verbose=" + verbose +
                ", loggerFile='" + loggerFile + '\'' +
                ", maxConnectionAttemps=" + maxConnectionAttemps +
                ", strategy=" + strategy +
                '}';
    }
}
