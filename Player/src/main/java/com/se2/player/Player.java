package com.se2.player;

import com.se2.communication.ClientVisitor;
import com.se2.communication.classes.MessageFields;
import com.se2.communication.classes.MessagePosition;
import com.se2.communication.enums.ActionMessageEnum;
import com.se2.communication.enums.DirectionMessageEnum;
import com.se2.communication.enums.PlacementResultEnum;
import com.se2.communication.enums.StatusMessageEnum;
import com.se2.communication.messages.*;
import com.se2.game.board.*;
import com.se2.game.board.enums.CellColor;
import com.se2.game.board.enums.CellState;
import com.se2.game.player.enums.PlayerState;
import com.se2.game.team.Team;
import com.se2.game.team.enums.TeamColor;
import com.se2.player.configuration.PlayerConfiguration;
import com.se2.tools.Logger;
import com.se2.tools.exceptions.BadConfigurationException;
import com.se2.tools.exceptions.LogicFailureException;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static com.se2.tools.ConvertersUtils.*;
import static com.se2.tools.Logger.log;
import static com.se2.tools.Logger.warning;
import static com.se2.tools.ProcessorDefender.sleepPlease;

public class Player extends Pawn implements ClientVisitor {

    final AtomicInteger connectionsTries = new AtomicInteger();
    private final PlayerConfiguration playerConfiguration;
    Boolean won = null;
    StatusMessageEnum responseForDiscover = null; //not cleaning as used in "Strategy.java"
    DiscoverMessageReturn discoverMessageReturn = null;

    StatusMessageEnum responseForMove = null; //not cleaning as used in "Strategy.java"
    MoveMessageReturn moveMessageReturn = null;

    StatusMessageEnum responseForPickUp = null; //not cleaning as used in "Strategy.java"
    PickUpMessageReturn pickUpMessageReturn = null;

    StatusMessageEnum responseForPlacePiece = null; //not cleaning as used in "Strategy.java"
    PlaceMessageReturn placeMessageReturn = null;

    StatusMessageEnum responseForTestPiece = null; //not cleaning as used in "Strategy.java"
    TestMessageReturn testMessageReturn = null;

    private Team team;
    private Board board;
    private Queue<Message> messagesToSendToGameMaster = new LinkedList<>();
    private Map<String, List<Message>> messagesToSend = new HashMap<>(); //for sending messages to players


    public Player(PlayerConfiguration playerConfiguration) {
        super();
        this.playerConfiguration = playerConfiguration;
        this.team = new Team(null);
    }

    public Team getTeam() {
        return team;
    }

    public void postMoveMessage(DirectionMessageEnum directionMessageEnum) {
        responseForMove = null;
        moveMessageReturn = null;
        sendMessage(null, new MoveMessage(getPlayerGuid(), directionMessageEnum));
    }

    public void postDiscoverMessage() {
        responseForDiscover = null;
        discoverMessageReturn = null;
        if (null != getPosition()) {
            sendMessage(null, new DiscoverMessage(getPlayerGuid(), new MessagePosition(getPosition().getX(), getPosition().getY())));
        }
    }

    public void postPickupMessage() {
        responseForPickUp = null;
        pickUpMessageReturn = null;
        sendMessage(null, new PickUpMessage(getPlayerGuid()));
    }


    public void postPlaceMessage() {
        responseForPlacePiece = null;
        placeMessageReturn = null;
        sendMessage(null, new PlaceMessage(getPlayerGuid()));
    }

    public void postTestPieceMessage() {
        responseForTestPiece = null;
        testMessageReturn = null;
        sendMessage(null, new TestMessage(getPlayerGuid()));
    }

    private void updateBoardWithDiscoveredCells(List<Cell> cells) {
        cells.forEach(
                cell -> {
                    cell.setCellColor(CellColor.GRAY);
                    this.board.updateCell(cell);
                }
        );
    }

    private void pickupPiece() {
        clearCellFromPieceData();
        piece = new Piece(null);
    }

    private void clearCellFromPieceData() {
        Cell cell = board.getCell(position);
        cell.setPiece(null);
        cell.setCellState(CellState.EMPTY);
        cell.setDistance(Integer.MAX_VALUE); //There was piece here
        board.updateCell(cell);
    }

    public Board getBoard() {
        return board;
    }

    public String getName() {
        return this.playerConfiguration.name;
    }

    void gameStarted() {
        if (PlayerState.WaitingForGameStartMessage.equals(playerState)) {
            playerState = PlayerState.Active;
        } else {
            throw new LogicFailureException("Player logic error WaitingForGameStartMessage --> Active");
        }
    }

    void gameEnded() {
        if (PlayerState.Active.equals(playerState)) {
            playerState = PlayerState.Completed;
        } else {
            throw new LogicFailureException("Player logic error WaitingForGameStartMessage --> Active");
        }
    }

    private Message receivedMessageNotForMe(Message message) {
        Logger.log("Player received message: " + message.toString());
        return null;
    }

    @Override
    public Message visit(Message message) {
        warning("Business logic error - this should not be received. Received:" + message);
        return null;
    }

    @Override
    public Message visit(MessageReturn message) {
        warning("Business logic error - this should not be received. Received:" + message);
        return null;
    }

    @Override
    public Message visit(ConnectPlayerMessage message) {
        return receivedMessageNotForMe(message);
    }

    @Override
    public Message visit(ConnectPlayerReturnMessage message) {
        String guid = message.getPlayerGuid();

        if (!getPlayerGuid().equals(guid) || !PlayerState.Initializing.equals(playerState)) {
            return receivedMessageNotForMe(message);
        }

        Logger.log(message.action, guid, message.status, "");
        if (StatusMessageEnum.OK.equals(message.status)) {
            playerState = PlayerState.WaitingForGameStartMessage;
            return null;
        } else {
            connectionsTries.incrementAndGet();
            if (playerConfiguration.maxConnectionAttemps < connectionsTries.get()) {
                return null;
            }
            return new ConnectPlayerMessage(guid, message.getPortNumber());
        }
    }

    @Override
    public Message visit(DiscoverMessage message) {
        return receivedMessageNotForMe(message);
    }

    @Override
    public Message visit(DiscoverMessageReturn message) {
        String guid = message.getPlayerGuid();

        if (!getPlayerGuid().equals(guid)) {
            return receivedMessageNotForMe(message);
        }

        Logger.log(message.action, guid, message.status, "");
        responseForDiscover = message.status;
        discoverMessageReturn = message;
        if (StatusMessageEnum.OK == message.status) {
            List<MessageFields> fields = message.getFields();
            List<Cell> cells = convertMessageFieldsToCells(fields);
            updateBoardWithDiscoveredCells(cells);
        }
        return null;
    }

    @Override
    public Message visit(EndMessage message) {
        if (null == message.getResult()) {
            won = null;
            Logger.log("Received null as game result. Possible draw or removed for being stuck.");
        }
        if (team.getTeamColor().equals(convertTeamColorMessageEnumToTeamColor(message.getResult()))) {
            won = true;
            Logger.log("WE HAVE WON!");
        } else {
            won = false;
            Logger.log("CONGRATULATION FOR OTHER TEAM!");
        }
        gameEnded();
        Logger.log(ActionMessageEnum.end, this.getPlayerGuid(), StatusMessageEnum.OK, won ? "won" : "lost");
        return null;
    }

    private void establishTeam(String receivedGuid, TeamColor receivedTeamColor, List<String> guids) {
        team = new Team(receivedTeamColor);
        for (String element : guids) {
            if (receivedGuid.equals(element)) {
                team.addPawn(this);
            } else {
                team.addPawn(new Pawn(element, receivedTeamColor));
            }
        }
    }

    @Override
    public Message visit(GameStartMessage message) {
        Logger.log(ActionMessageEnum.start, this.getPlayerGuid(), null, "received");
        String receivedGuid = message.getPlayerGuid();
        if (!getPlayerGuid().equals(receivedGuid) || !PlayerState.WaitingForGameStartMessage.equals(playerState)) {
            return receivedMessageNotForMe(message);
        }
        TeamColor receivedTeamColor = convertTeamColorMessageEnumToTeamColor(message.getTeam());
        if (null == receivedTeamColor) {
            throw new BadConfigurationException("Received team Color is null ");
        }
        List<String> guids = message.getTeamGuids();
        if (null == guids || guids.isEmpty()) {
            throw new BadConfigurationException("Received team guid empty ");
        }
        setTeamColorForPawn(receivedTeamColor);
        establishTeam(receivedGuid, receivedTeamColor, guids);
        position = convertMessagePositionToPosition(message.getMessagePosition());
        if (null == message.getMessageBoard()) {
            throw new BadConfigurationException("Received board is null ");
        }
        board = convertMessageBoardToBoard(message.getMessageBoard(), true);
        Cell cellToUpdate = board.getCell(position.getX(), position.getY());
        cellToUpdate.setPawnGuid(getPlayerGuid());
        gameStarted();
        Logger.log(ActionMessageEnum.start, this.getPlayerGuid(), StatusMessageEnum.OK, "Successfully processed");
        return null;
    }

    @Override
    public Message visit(MoveMessage message) {
        return receivedMessageNotForMe(message);
    }

    @Override
    public Message visit(MoveMessageReturn message) {
        String guid = message.getPlayerGuid();

        if (!getPlayerGuid().equals(guid)) {
            return receivedMessageNotForMe(message);
        }

        Logger.log(message.action, guid, message.status, message.getDirectionMessageEnum().toString());
        responseForMove = message.status;
        moveMessageReturn = message;
        if (PlayerState.Active.equals(playerState) && StatusMessageEnum.OK.equals(message.status)) {
            Position received = convertMessagePositionToPosition(message.getMessagePosition());
            if (null == received) {
                Logger.log("Received null position");
                return receivedMessageNotForMe(message);
            }
            Cell oldCell = board.getCell(position);
            oldCell.setPawnGuid(null);
            board.updateCell(oldCell);
            Cell newCell = board.getCell(received);
            newCell.setPawnGuid(getPlayerGuid());
            board.updateCell(oldCell);
            setPosition(received);

            return null;
        } else {
            return receivedMessageNotForMe(message);
        }
    }

    @Override
    public Message visit(PickUpMessage message) {
        return receivedMessageNotForMe(message);
    }

    @Override
    public Message visit(PickUpMessageReturn message) {
        String guid = message.getPlayerGuid();

        if (!getPlayerGuid().equals(guid)) {
            return receivedMessageNotForMe(message);
        }

        Logger.log(message.action, guid, message.status, piece == null ? "null" : "piece");
        responseForPickUp = message.status;
        pickUpMessageReturn = message;
        if (StatusMessageEnum.OK == message.status) {
            pickupPiece();
        }
        if ((StatusMessageEnum.DENIED == message.status) && (null == piece)) { //player had no piece
            clearCellFromPieceData();
        }
        return null;
    }

    @Override
    public Message visit(PlaceMessage message) {
        return receivedMessageNotForMe(message);
    }

    @Override
    public Message visit(PlaceMessageReturn message) {
        String guid = message.getPlayerGuid();

        if (!getPlayerGuid().equals(guid) || !PlayerState.Active.equals(playerState)) {
            return receivedMessageNotForMe(message);
        }

        Logger.log(message.action, guid, message.status, message.getPlacementResult() == null ? "null" : message.getPlacementResult().toString());
        responseForPlacePiece = message.status;
        placeMessageReturn = message;
        if (StatusMessageEnum.OK == message.status) {
            if (null == message.getPlacementResult()) {
                Logger.log("Received messaged OK for place message with null as placement result");
                return receivedMessageNotForMe(message);
            }
            Cell cell = board.getCell(position);
            CellState cellState = PlacementResultEnum.Correct.equals(message.getPlacementResult()) ? CellState.DISCOVERED_GOAL : CellState.DISCOVERED_NON_GOAL;
            cell.setCellState(cellState);
            board.updateCell(cell);
            piece = null;
        } else if (StatusMessageEnum.DENIED == message.status) {
            Logger.warning("Received messaged DENIED for place message. Player piece = " + piece.toString() + ". Cleaning piece.");
            piece = null;
        } else {
            warning("Received messaged without expected OK/DENIED: " + message);
        }
        return null;
    }

    @Override
    public Message visit(TestMessage message) {
        return receivedMessageNotForMe(message);
    }

    @Override
    public Message visit(TestMessageReturn message) {
        String guid = message.getPlayerGuid();

        if (!getPlayerGuid().equals(guid) || !PlayerState.Active.equals(playerState)) {
            return receivedMessageNotForMe(message);
        }

        Logger.log(message.action, guid, message.status, message.getTest() == null ? "null" : message.getTest().toString());
        responseForTestPiece = message.status;
        testMessageReturn = message;
        if (StatusMessageEnum.OK == message.status) {
            if (null == message.getTest()) {
                Logger.log("Received messaged OK for piece test with null test result");
                return receivedMessageNotForMe(message);
            }
            if (message.getTest()) {
                piece = new Piece(false);
            } else {
                piece = null;
            }
        }
        if ((StatusMessageEnum.DENIED == message.status)) {
            Logger.log("Received messaged Denied for piece test");
            piece = null; //Player had no piece
        }
        return null;
    }

    @Override
    public boolean gameNotOver() {
        return !PlayerState.Completed.equals(playerState);
    }

    @Override
    public Message getMessageForServer() {
        if (null != messagesToSendToGameMaster.peek()) {
            return messagesToSendToGameMaster.poll();
        } else {
            return null;
        }
    }

    public void startOneGame() {
        Game game = new Game(this);
        (new Thread(game)).start();

    }

    private void sendMessage(Pawn player, Message message) {
        if (null == player) {
            messagesToSendToGameMaster.add(message);
        } else {
            List<Message> messages = messagesToSend.computeIfAbsent(player.getPlayerGuid(), k -> new ArrayList<>());
            messages.add(message);
        }
    }

    public String getInfo() {
        switch (playerState) {
            case Initializing:
                return "Not Connected";
            case Connected:
                return "Connected";
            case WaitingForGameStartMessage:
                return "Not   started";
            case Active:
                return "Playing";
            case Completed:
                return null != won ? won ? "VICTORY !!!!!" : "   LOST   " : "WHO WON?";
            default:
                warning("Player state is not recognized. Player state = " + playerState);
                return "ERROR";
        }
    }

    public class Game implements Runnable {

        private final Player player;

        public Game(Player player) {
            this.player = player;
        }

        @Override
        public void run() {
            //Try to connect to GM
            sendMessage(null, new ConnectPlayerMessage(getPlayerGuid(), playerConfiguration.portNumber));
            while (playerConfiguration.maxConnectionAttemps > connectionsTries.get() && playerState.equals(PlayerState.Initializing)) {
                sleepPlease();
            }
            if (playerState.equals(PlayerState.Initializing)) {
                System.out.println("Player did not connect. Number of connectionsTries:" + connectionsTries.get());
                System.exit(1);
            }

            while (playerState.equals(PlayerState.WaitingForGameStartMessage)) {
                sleepPlease();
            }
            //Game started

            long mark1 = System.currentTimeMillis();
            if (playerState.equals(PlayerState.Active)) {
                switch (playerConfiguration.strategy) {
                    case "HUMAN":
                        log("Will play as human.");
                        break;
                    case "Karol":
                        log("Will use strategy Karol.");
                        StrategyKarol strategyKarol = new StrategyKarol(player);
                        strategyKarol.play();
                        break;
                    case "Tetyana":
                        log("Will use strategy Tetyana.");
                        Strategy strategy = new Strategy(player);
                        strategy.play();
                        break;
                    case "WuCash":
                    default:
                        log("Will use strategy WuCash. It's default strategy.");
                        StrategyWuCash strategyWuCash = new StrategyWuCash(player);
                        strategyWuCash.play();
                }
                sleepPlease();
            }
            //gameOver
        }
    }
}
