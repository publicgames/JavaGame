package com.se2.player;

import com.se2.game.player.enums.PlayerState;
import com.se2.player.configuration.PlayerConfiguration;
import com.se2.tools.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static com.se2.tools.ProcessorDefender.sleepPlease;
import static org.junit.jupiter.api.Assertions.*;

class PlayerIT {
    static String jarfileCS = "..\\CommunicationServer\\target\\communication-server.jar";
    static String jarfileGM = "..\\GameMaster\\target\\game-master.jar";
    static String jarfilePlayer = "..\\Player\\target\\player.jar";
    Process csProcess;
    Process gmProcess;
    Process player1Process;
    Process player2Process;
    PlayerClient playerClient03;
    PlayerClient playerClient04;
    Process player5Process;
    Process player6Process;
    Process player7Process;
    Process player8Process;

    private static Process startProcess(String jarfile, String options) {
        try {
            return Runtime.getRuntime().exec("cmd.exe start /C java -jar " + jarfile + options);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.exit(1);
        return null;
    }



    @Test
    public void testWith8PlayersBigBoardSmallDelays() {
        String delays = " -delaydestroy 5 " + " -delaypiece 10000 " + " -delaymove 5 " + " -delaydiscover 5 " + " -delaytest 5 " + " -delaypick 5 " + " -delayplace 5 ";
        gmProcess = startProcess(jarfileGM, " -initial_pieces 2 -initial_pieces_positions 15,15:15,16 -max_pieces 20 -max_team_size 4 -predefined_goal_positions 0,0:1,0:31,0:0,31:30,31:31,31 -board_width 32 -board_task_height 26 -board_goal_height 3 " + delays);
        sleepPlease(2000);
        csProcess = startProcess(jarfileCS, "");
        sleepPlease(2000);
        player1Process = startProcess(jarfilePlayer, " -o playerIT1.txt -n Player001");
        sleepPlease(2000);
        player2Process = startProcess(jarfilePlayer, " -o playerIT2.txt -n Player002");
        sleepPlease(2000);

        PlayerConfiguration playerConfiguration03 = new PlayerConfiguration(
                "default_player_config003.cfg",
                "127.0.0.1",
                9999,
                "player3log.out",
                "Player003",
            true,
                4,
                "Default");
        Logger.start(playerConfiguration03.verbose, playerConfiguration03.loggerFile);
        Logger.log(playerConfiguration03.toString());
        Player player03 = new Player(playerConfiguration03);
        player03.startOneGame();
        playerClient03 = new PlayerClient(playerConfiguration03.ipAddress, playerConfiguration03.portNumber, player03);
        playerClient03.start();
        sleepPlease(2000);

        PlayerConfiguration playerConfiguration04 = new PlayerConfiguration(
                "default_player_config004.cfg",
                "127.0.0.1",
                9999,
                "player4log.out",
                "Player004",
                true,
                4,
                "Default");
        Logger.start(playerConfiguration04.verbose, playerConfiguration04.loggerFile);
        Logger.log(playerConfiguration04.toString());
        Player player04 = new Player(playerConfiguration04);
        player04.startOneGame();
        playerClient04 = new PlayerClient(playerConfiguration04.ipAddress, playerConfiguration04.portNumber, player04);
        playerClient04.start();
        sleepPlease(2000);

        player5Process = startProcess(jarfilePlayer, " -o playerIT5.txt -n Player005");
        sleepPlease(2000);
        player6Process = startProcess(jarfilePlayer, " -o playerIT6.txt -n Player006");
        sleepPlease(2000);
        player7Process = startProcess(jarfilePlayer, " -o playerIT7.txt -n Player007");
        sleepPlease(2000);
        player8Process = startProcess(jarfilePlayer, " -o playerIT8.txt -n Player008");
        sleepPlease(2000);

        sleepPlease(1000);
        long givenTime = 60000;
        long mark1 = System.currentTimeMillis();
        while (true && ((System.currentTimeMillis()) - mark1 < givenTime)) {
            sleepPlease(500);
            if (PlayerState.Completed.equals(player04.getPlayerState())) {
                break;
            }
        }
        sleepPlease(2000);
        if (((System.currentTimeMillis()) - mark1 < givenTime)) {
            assertEquals(PlayerState.Completed, player03.getPlayerState());
            assertEquals(PlayerState.Completed, player04.getPlayerState());

            if (player03.won) {
                assertEquals(false, player04.won);
            } else {
                assertEquals(true, player04.won);
            }
        } else {
            System.out.println("Game did not finished in given time! " + givenTime/1000 + " seconds.");
            fail();
        }
    }

    @Test
    public void testWith4Players() {
        int default_delays = 5;
        String delays = " -delaydestroy " + default_delays + " -delaypiece 10000 " + " -delaymove " + default_delays + " -delaydiscover " + default_delays + " -delaytest " + default_delays + " -delaypick " + default_delays + " -delayplace " + default_delays;
        gmProcess = startProcess(jarfileGM, " -initial_pieces 2 -initial_pieces_positions 1,1:2,2 -max_team_size 2 -predefined_goal_positions 0,0:1,0:2,3:3,3 -board_width 4 -board_task_height 2 -board_goal_height 1 -delaypiece 10000 " + delays);
        sleepPlease(2000);
        csProcess = startProcess(jarfileCS, "");
        sleepPlease(2000);
        player1Process = startProcess(jarfilePlayer, " -o playerIT1.txt -n Player001");
        sleepPlease(2000);
        player2Process = startProcess(jarfilePlayer, " -o playerIT2.txt -n Player002");
        sleepPlease(2000);

        PlayerConfiguration playerConfiguration03 = new PlayerConfiguration(
                "default_player_config003.cfg",
                "127.0.0.1",
                7654,
                "player3log.out",
                "Player003",
                true,
                4,
                "Default");
        Logger.start(playerConfiguration03.verbose, playerConfiguration03.loggerFile);
        Logger.log(playerConfiguration03.toString());
        Player player03 = new Player(playerConfiguration03);
        player03.startOneGame();
        playerClient03 = new PlayerClient(playerConfiguration03.ipAddress, playerConfiguration03.portNumber, player03);
        playerClient03.start();
        sleepPlease(2000);

        PlayerConfiguration playerConfiguration04 = new PlayerConfiguration(
                "default_player_config004.cfg",
                "127.0.0.1",
                7654,
                "player4log.out",
                "Player004",
                true,
                4,
                "Default");
        Logger.start(playerConfiguration04.verbose, playerConfiguration04.loggerFile);
        Logger.log(playerConfiguration04.toString());
        Player player04 = new Player(playerConfiguration04);
        player04.startOneGame();
        playerClient04 = new PlayerClient(playerConfiguration04.ipAddress, playerConfiguration04.portNumber, player04);
        playerClient04.start();


        sleepPlease(1000);
        long givenTime = 60000;
        long mark1 = System.currentTimeMillis();
        while (true && ((System.currentTimeMillis()) - mark1 < givenTime)) {
            sleepPlease(500);
            if (PlayerState.Completed.equals(player04.getPlayerState())) {
                break;
            }
        }
        sleepPlease(2000);
        if (((System.currentTimeMillis()) - mark1 < givenTime)) {
            assertEquals(PlayerState.Completed, player03.getPlayerState());
            assertEquals(PlayerState.Completed, player04.getPlayerState());

            if (player03.won) {
                assertEquals(false, player04.won);
            } else {
                assertEquals(true, player04.won);
            }
        } else {
            System.out.println("Game did not finished in given time! " + givenTime/1000 + " seconds.");
            fail();
        }
    }

    private void localChampionships(String teamBlueStrategy, String teamRedStrategy) {
        long start = System.currentTimeMillis();
        gmProcess = startProcess(jarfileGM, " ");
        sleepPlease(2000);
        csProcess = startProcess(jarfileCS, "");
        sleepPlease(2000);
        player1Process = startProcess(jarfilePlayer, " -o playerIT1.txt -n Player001 -s " + teamBlueStrategy);
        sleepPlease(2000);
        player2Process = startProcess(jarfilePlayer, " -o playerIT2.txt -n Player002 -s " + teamRedStrategy);
        sleepPlease(2000);

        PlayerConfiguration playerConfiguration03 = new PlayerConfiguration(
                "default_player_config003.cfg",
                "127.0.0.1",
                7654,
                "player3log.out",
                "Player003",
                true,
                4,
                teamBlueStrategy);
        Logger.start(playerConfiguration03.verbose, playerConfiguration03.loggerFile);
        Logger.log(playerConfiguration03.toString());
        Player player03 = new Player(playerConfiguration03);
        player03.startOneGame();
        playerClient03 = new PlayerClient(playerConfiguration03.ipAddress, playerConfiguration03.portNumber, player03);
        playerClient03.start();
        sleepPlease(2000);

        PlayerConfiguration playerConfiguration04 = new PlayerConfiguration(
                "default_player_config004.cfg",
                "127.0.0.1",
                7654,
                "player4log.out",
                "Player004",
                true,
                4,
                teamRedStrategy);
        Logger.start(playerConfiguration04.verbose, playerConfiguration04.loggerFile);
        Logger.log(playerConfiguration04.toString());
        Player player04 = new Player(playerConfiguration04);
        player04.startOneGame();
        playerClient04 = new PlayerClient(playerConfiguration04.ipAddress, playerConfiguration04.portNumber, player04);
        playerClient04.start();
        sleepPlease(2000);


        player5Process = startProcess(jarfilePlayer, " -o playerIT5.txt -n Player005 -s " + teamBlueStrategy);
        sleepPlease(2000);
        player6Process = startProcess(jarfilePlayer, " -o playerIT6.txt -n Player006 -s " + teamRedStrategy);
        sleepPlease(2000);
        player7Process = startProcess(jarfilePlayer, " -o playerIT7.txt -n Player007 -s " + teamBlueStrategy);
        sleepPlease(2000);
        player8Process = startProcess(jarfilePlayer, " -o playerIT8.txt -n Player008 -s " + teamRedStrategy);
        sleepPlease(2000);


        sleepPlease(1000);
        long givenTime = 600000;
        long mark1 = System.currentTimeMillis();
        while ((System.currentTimeMillis()) - mark1 < givenTime) {
            sleepPlease(500);
            if (PlayerState.Completed.equals(player04.getPlayerState())) {
                break;
            }
        }
        sleepPlease(2000);
        if (((System.currentTimeMillis()) - mark1 < givenTime)) {
            assertEquals(PlayerState.Completed, player03.getPlayerState());
            assertEquals(PlayerState.Completed, player04.getPlayerState());

            if (player03.won) {
                System.out.println("Player 3 won with strategy " + teamBlueStrategy);
                assertEquals(false, player04.won);
            } else {
                System.out.println("Player 4 won with strategy " + teamRedStrategy);
                assertEquals(true, player04.won);
            }
        } else {
            System.out.println("Game did not finished in given time! " + givenTime/1000 + " seconds.");
            fail();
        }
        System.out.println("Test took " + ((start - System.currentTimeMillis())/1000) + " seconds");
    }

    @Test
    public void localChampionshipsTvsK() {
        String teamBlueStrategy = "Tetyana";
        String teamRedStrategy = "Karol";
        localChampionships(teamBlueStrategy, teamRedStrategy);
    }

    @Test
    public void localChampionshipsTvsL() {
        String teamBlueStrategy = "Tetyana";
        String teamRedStrategy = "WuCash";
        localChampionships(teamBlueStrategy, teamRedStrategy);
    }

    @Test
    public void localChampionshipsLvsK() {
        String teamBlueStrategy = "WuCash";
        String teamRedStrategy = "Karol";
        localChampionships(teamBlueStrategy, teamRedStrategy);
    }

    @AfterEach
    public void cleanup() {
        cleanupProcess(csProcess);
        cleanupProcess(gmProcess);
        cleanupProcess(player1Process);
        cleanupProcess(player2Process);
        cleanupProcess(player5Process);
        cleanupProcess(player6Process);
        cleanupProcess(player7Process);
        cleanupProcess(player8Process);

        playerClient03.interrupt();
        playerClient04.interrupt();
    }

    private void cleanupProcess(Process process) {
        if (null != process) {
            process.descendants().forEachOrdered(ProcessHandle::destroy);
            process.destroy();
        }
    }
}