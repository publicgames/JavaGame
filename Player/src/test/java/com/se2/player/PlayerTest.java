package com.se2.player;

import com.se2.communication.classes.MessageBoard;
import com.se2.communication.classes.MessageFields;
import com.se2.communication.classes.MessagePosition;
import com.se2.communication.enums.DirectionMessageEnum;
import com.se2.communication.enums.PlacementResultEnum;
import com.se2.communication.enums.StatusMessageEnum;
import com.se2.communication.enums.TeamColorMessageEnum;
import com.se2.communication.messages.*;
import com.se2.game.board.*;
import com.se2.game.board.enums.CellColor;
import com.se2.game.board.enums.CellState;
import com.se2.game.player.enums.PlayerState;
import com.se2.game.team.Team;
import com.se2.game.team.enums.TeamColor;
import com.se2.player.configuration.PlayerConfiguration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.se2.tools.ConvertersUtils.convertCellsToMessageFields;
import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {
    private static final String FIXME = "FIXME";
    private static final int playerPort = 7777;
    private static final Position playerInitPosition = new Position(4, 10);
    private static final Position playerActivePosition = new Position(3, 10);
    private static final Position playerActivePositionForDiscoverTests = new Position(1, 4);
    private static String playerUUIDInit;
    private static String playerUUIDActive;
    private static Player playerInit;
    private static Player playerActive;
    private static PlayerConfiguration playerInitConfiguration;
    private static PlayerConfiguration playerActiveConfiguration;

    @BeforeEach
    public void setUp() {
        //Two players for tests
        //playerInit connection tests (till startGameMessage)
        //playerActive game tests (beyond startGameMessage)

        playerInitConfiguration = new PlayerConfiguration(
                "DefaultFile",
                "192.168.0.1",
                5555,
                "log.out",
                "Before Active Game phase",
                true,
                3,
                "Default");
        playerInit = new Player(playerInitConfiguration);
        playerUUIDInit = playerInit.getPlayerGuid();

        playerActiveConfiguration = new PlayerConfiguration(
                "DefaultFile",
                "192.168.0.1",
                5555,
                "log.out",
                "Playing Player",
                true,
                3,
                "Default");
        playerActive = new Player(playerActiveConfiguration);
        playerUUIDActive = playerActive.getPlayerGuid();

        // setup for already connected player - playerActive
        Message returns = playerActive.visit(new ConnectPlayerReturnMessage(playerUUIDActive, playerPort, StatusMessageEnum.OK));
        assertEquals(PlayerState.WaitingForGameStartMessage, playerActive.getPlayerState());

        GameStartMessage gameStartMessage = new GameStartMessage(
                playerUUIDActive,
                TeamColorMessageEnum.Red,
                "FIXME - specification alteration point 1" + FIXME,
                2,
                List.of(playerUUIDInit, playerUUIDActive),
                new MessagePosition(playerActivePosition.getX(), playerActivePosition.getY()),
                new MessageBoard(5, 5, 3)
        );
        assertNull(playerActive.visit(gameStartMessage));

        //Board used in those tests.
        /*
        +-----+
        |B    | <-- blue player on 0,0 - not added in set up
        | GG G|
        |   GG|
        +-----+
        |123  | <-- 1,2,3 Manhattan Distance
        |PR2  | <-- playerActive in discover test (position 1,4)
        |123  |
        |    P|
        |     |
        +-----+
        |G    |
        | GGGG|
        |   RR| <-- most right (playerActive (3,10)) the other one is playerInit (4,10)
        +-----+
        */
    }

    /**
     * ConnectPlayerStatus
     * Use Case 1: As Player I want to process players connect messages response, so that I can join game
     * subcase01 - happy path
     */
    @Test
    public void connectPlayerMessageReturn01Test() {
        ConnectPlayerReturnMessage received = new ConnectPlayerReturnMessage(playerUUIDInit, playerPort, StatusMessageEnum.OK);
        assertAll(
                () -> assertEquals(PlayerState.Initializing, playerInit.getPlayerState()),
                () -> assertNull(playerInit.visit(received)),
                () -> assertEquals(PlayerState.WaitingForGameStartMessage, playerInit.getPlayerState()) // status changed
        );
    }

    /**
     * ConnectPlayerStatus
     * Use Case 1: As Player I want to process players connect messages response, so that I can join game
     * subcase02 - received Denied. try to connect 3 times
     */
    @Test
    public void connectPlayerMessageReturn02Test() {
        playerInit.connectionsTries.incrementAndGet();
        assertAll(
                () -> assertEquals(1, playerInit.connectionsTries.get()),
                () -> assertEquals(PlayerState.Initializing, playerInit.getPlayerState())
        );
        ConnectPlayerReturnMessage received = new ConnectPlayerReturnMessage(playerUUIDInit, playerPort, StatusMessageEnum.DENIED);
        ConnectPlayerMessage expected = new ConnectPlayerMessage(playerUUIDInit, playerPort); //try to connect again
        assertAll(
                () -> assertEquals(1, playerInit.connectionsTries.get()),
                () -> assertEquals(PlayerState.Initializing, playerInit.getPlayerState()),
                () -> assertEquals(expected, playerInit.visit(received)),
                () -> assertEquals(2, playerInit.connectionsTries.get())
        );
        assertAll(
                () -> assertEquals(2, playerInit.connectionsTries.get()),
                () -> assertEquals(PlayerState.Initializing, playerInit.getPlayerState()),
                () -> assertEquals(expected, playerInit.visit(received)),
                () -> assertEquals(3, playerInit.connectionsTries.get())
        );
        assertAll(
                () -> assertEquals(3, playerInit.connectionsTries.get()),
                () -> assertEquals(PlayerState.Initializing, playerInit.getPlayerState()),
                () -> assertNull(playerInit.visit(received)),
                () -> assertEquals(4, playerInit.connectionsTries.get())
        );
    }

    /**
     * Use Case 2: As Player I want to process players connect messages, so I don't get stuck
     * subcase01 - happy path
     */
    @Test
    public void connectPlayerMessage01Test() {
        ConnectPlayerMessage received = new ConnectPlayerMessage(playerUUIDInit, playerPort);
        assertNull(playerInit.visit(received));
    }

    /**
     * GameStart
     * Use Case 4: As Player I want to process game start messages, so start playing
     * subcase01 - happy path
     */
    @Test
    public void gameStartMessage01Test() {
        // setup
        playerInit.visit(new ConnectPlayerReturnMessage(playerUUIDInit, playerPort, StatusMessageEnum.OK));
        assertEquals(PlayerState.WaitingForGameStartMessage, playerInit.getPlayerState());

        // test
        GameStartMessage received = new GameStartMessage(
                playerUUIDInit,
                TeamColorMessageEnum.Red,
                "FIXME - specification alteration point 1" + FIXME,
                2,
                List.of(playerUUIDInit, playerUUIDActive),
                new MessagePosition(playerInitPosition.getX(), playerInitPosition.getY()),
                new MessageBoard(5, 5, 3)
        );
        //state before receiving message
        assertAll(
                () -> assertEquals(playerUUIDInit, playerInit.getPlayerGuid()),
                () -> assertNull(playerInit.getTeamColorForPawn()),
                //not testing team role - specification alteration point 1. - FIXME - clean
                () -> assertEquals(0, playerInit.getTeam().getSize()),
                () -> assertEquals(new Team(null), playerInit.getTeam()),
                () -> assertNull(playerInit.getPosition()),
                () -> assertNull(playerInit.getBoard())
        );
        //Expected:
        assertNull(playerInit.visit(received)); // process message
        assertEquals(playerUUIDInit, playerInit.getPlayerGuid());
        assertEquals(TeamColor.RED, playerInit.getTeamColorForPawn());
        //not testing team role - specification alteration point 1. - FIXME - clean
        assertEquals(2, playerInit.getTeam().getSize());
        Team expectedTeam = new Team(TeamColor.RED);
        Pawn expectedPawn = new Pawn(playerUUIDInit, new Position(4, 10), null, null, TeamColor.RED);
        expectedPawn.setPlayerState(PlayerState.Active);
        expectedTeam.addPawn(expectedPawn);
        expectedTeam.addPawn(new Pawn(playerUUIDActive, null, null, null, TeamColor.RED));
        assertEquals(expectedTeam, playerInit.getTeam());
        assertEquals(new Position(4, 10), playerInit.getPosition());
        Board expectedBoard = new Board(5, 5, 3, true);
        expectedBoard.getCell(4, 10).setPawnGuid(playerUUIDInit);
        assertEquals(expectedBoard, playerInit.getBoard());
    }

    /**
     * ActionStatus - MoveStatus message
     * Use Case 5: As player I want to process the MessagePosition to make my move
     * subcase01 - status OK - going Up
     * subcase01 - status OK - going Up
     */
    @Test
    public void moveMessageReturn01Test() {
        //given
        Position startingPosition = new Position(playerActive.getPosition().getX(), playerActive.getPosition().getY());

        MessagePosition messagePosition = new MessagePosition(playerActive.getPosition().getX(), playerActive.getPosition().getY() - 1);
        MoveMessageReturn received = new MoveMessageReturn(StatusMessageEnum.OK, playerUUIDActive, DirectionMessageEnum.Up, messagePosition);

        //when
        Position expectedPlayerPosition = new Position(playerActive.getPosition().getX(), playerActive.getPosition().getY() - 1);

        //then
        assertAll(
                () -> assertEquals(startingPosition, playerActive.getPosition()),
                () -> assertEquals(playerUUIDActive, playerActive.getBoard().getCell(startingPosition).getPawnGuid()),
                () -> assertNull(playerActive.getBoard().getCell(expectedPlayerPosition).getPawnGuid()),
                () -> assertNull(playerActive.visit(received)),
                () -> assertEquals(expectedPlayerPosition, playerActive.getPosition()),
                () -> assertNull(playerActive.getBoard().getCell(startingPosition).getPawnGuid()),
                () -> assertEquals(playerUUIDActive, playerActive.getBoard().getCell(expectedPlayerPosition).getPawnGuid())
        );
    }

    /**
     * ActionStatus - MoveStatus message
     * Use Case 5: As player I want to process the move message response to make my move
     * subcase02 - status OK - going Left
     */
    @Test
    public void moveMessageReturn02Test() {
        //given
        Position startingPosition = new Position(playerActive.getPosition().getX(), playerActive.getPosition().getY());
        MessagePosition messagePosition = new MessagePosition(playerActive.getPosition().getX() - 1, playerActive.getPosition().getY());
        MoveMessageReturn received = new MoveMessageReturn(StatusMessageEnum.OK, playerUUIDActive, DirectionMessageEnum.Left, messagePosition);

        //when
        Position expectedPlayerPosition = new Position(playerActive.getPosition().getX() - 1, playerActive.getPosition().getY());

        //when
        assertAll(
                () -> assertEquals(startingPosition, playerActive.getPosition()),
                () -> assertEquals(playerUUIDActive, playerActive.getBoard().getCell(startingPosition).getPawnGuid()),
                () -> assertNull(playerActive.getBoard().getCell(expectedPlayerPosition).getPawnGuid()),
                () -> assertNull(playerActive.visit(received)),
                () -> assertEquals(expectedPlayerPosition, playerActive.getPosition()),
                () -> assertNull(playerActive.getBoard().getCell(startingPosition).getPawnGuid()),
                () -> assertEquals(playerUUIDActive, playerActive.getBoard().getCell(expectedPlayerPosition).getPawnGuid())
        );
    }

    /**
     * ActionStatus - MoveStatus message
     * Use Case 5: As player I want to process the move message response to make my move
     * subcase03 - status OK - going Right
     */
    @Test
    public void moveMessageReturn03Test() {
        // given
        Position startingPosition = new Position(playerActive.getPosition().getX(), playerActive.getPosition().getY());
        MessagePosition messagePosition = new MessagePosition(playerActive.getPosition().getX() + 1, playerActive.getPosition().getY());
        MoveMessageReturn received = new MoveMessageReturn(StatusMessageEnum.OK, playerUUIDActive, DirectionMessageEnum.Right, messagePosition);

        //when
        Position expectedPlayerPosition = new Position(playerActive.getPosition().getX() + 1, playerActive.getPosition().getY());

        //then
        assertAll(
                () -> assertEquals(startingPosition, playerActive.getPosition()),
                () -> assertEquals(playerUUIDActive, playerActive.getBoard().getCell(startingPosition).getPawnGuid()),
                () -> assertNull(playerActive.getBoard().getCell(expectedPlayerPosition).getPawnGuid()),
                () -> assertNull(playerActive.visit(received)),
                () -> assertEquals(expectedPlayerPosition, playerActive.getPosition()),
                () -> assertNull(playerActive.getBoard().getCell(startingPosition).getPawnGuid()),
                () -> assertEquals(playerUUIDActive, playerActive.getBoard().getCell(expectedPlayerPosition).getPawnGuid())
        );
    }

    /**
     * ActionStatus - MoveStatus message
     * Use Case 5: As player I want to process the move message response to make my move
     * subcase04 - status Denied - going Down - field outside board
     */
    @Test
    public void moveMessageReturn04Test() {
        // given
        Position startingPosition = new Position(playerActive.getPosition().getX(), playerActive.getPosition().getY());
        MessagePosition messagePosition = new MessagePosition(playerActive.getPosition().getX(), playerActive.getPosition().getY() + 1);
        MoveMessageReturn received = new MoveMessageReturn(StatusMessageEnum.DENIED, playerUUIDActive, DirectionMessageEnum.Down, messagePosition);

        //when
        Position expectedPlayerPosition = new Position(playerActive.getPosition().getX(), playerActive.getPosition().getY());

        //then
        assertAll(
                () -> assertEquals(startingPosition, playerActive.getPosition()),
                () -> assertEquals(playerUUIDActive, playerActive.getBoard().getCell(startingPosition).getPawnGuid()),
                () -> assertNull(playerActive.visit(received)),
                () -> assertEquals(expectedPlayerPosition, playerActive.getPosition()),
                () -> assertEquals(playerUUIDActive, playerActive.getBoard().getCell(startingPosition).getPawnGuid())
        );
    }

    /**
     * ActionStatus - MoveStatus message
     * Use Case 5: As player I want to process the move message response to make my move
     * subcase05 - status OK, move up then down (to check that down works)
     */
    @Test
    public void moveMessageReturn05Test() {
        //given
        Position startingPosition = new Position(playerActive.getPosition().getX(), playerActive.getPosition().getY());
        Position positionAfterUp = new Position(playerActive.getPosition().getX(), playerActive.getPosition().getY() - 1);

        MessagePosition messagePosition = new MessagePosition(playerActive.getPosition().getX(), playerActive.getPosition().getY() - 1);

        //when
        MoveMessageReturn received1 = new MoveMessageReturn(StatusMessageEnum.OK, playerUUIDActive, DirectionMessageEnum.Up, messagePosition);
        assertNull(playerActive.visit(received1));

        messagePosition = new MessagePosition(playerActive.getPosition().getX(), playerActive.getPosition().getY() + 1);
        MoveMessageReturn received2 = new MoveMessageReturn(StatusMessageEnum.OK, playerUUIDActive, DirectionMessageEnum.Down, messagePosition);

        Position expectedPlayerPosition = startingPosition;

        //then
        assertAll(
                () -> assertEquals(positionAfterUp, playerActive.getPosition()),
                () -> assertEquals(playerUUIDActive, playerActive.getBoard().getCell(positionAfterUp).getPawnGuid()),
                () -> assertNull(playerActive.getBoard().getCell(expectedPlayerPosition).getPawnGuid()),
                () -> assertNull(playerActive.visit(received2)),
                () -> assertEquals(expectedPlayerPosition, playerActive.getPosition()),
                () -> assertNull(playerActive.getBoard().getCell(positionAfterUp).getPawnGuid()),
                () -> assertEquals(playerUUIDActive, playerActive.getBoard().getCell(expectedPlayerPosition).getPawnGuid())
        );
    }

    /**
     * ActionStatus - PickUpStatus message
     * Use Case 6: As a Player I want to process the pick up message to be able to pick up a piece
     * subcase01 - happy path - player had picked up a piece successfully
     */
    @Test
    public void pickUpMessageReturn01Test() {
        playerActive.getBoard().updateCell(new Cell(
                CellState.PIECE,
                playerActive.getPlayerGuid(),
                0,
                new Position(playerActive.getPosition().getX(), playerActive.getPosition().getY()),
                CellColor.GRAY,
                new Piece(null)
        ));

        //when
        PickUpMessageReturn received = new PickUpMessageReturn(StatusMessageEnum.OK, playerUUIDActive);

        //then
        assertAll(
                () -> assertNull(playerActive.visit(received)),
                () -> assertNotNull(playerActive.getPiece()), // player has a piece now
                () -> assertNull(playerActive.getPiece().isSham()),
                () -> assertNull(playerActive.getBoard().getCell(playerActive.getPosition()).getPiece()),
                () -> assertEquals(CellState.EMPTY, playerActive.getBoard().getCell(playerActive.getPosition()).getCellState())
        );
    }

    /**
     * ActionStatus - PickUpStatus message
     * Use Case 6: As a Player I want to process the pick up message to be able to pich up a piece
     * subcase02 - DENIED - player does not have a piece, player thinks there is a piece on board
     * Assumption: player was not holding a piece already
     */
    @Test
    public void pickUpMessageReturn02Test() {
        playerActive.getBoard().updateCell(new Cell(
                CellState.PIECE,
                playerActive.getPlayerGuid(),
                0,
                new Position(playerActive.getPosition().getX(), playerActive.getPosition().getY()),
                CellColor.GRAY,
                new Piece(null)
        ));
        // test
        PickUpMessageReturn received = new PickUpMessageReturn(StatusMessageEnum.DENIED, playerUUIDActive);
        assertAll(
                () -> assertNotNull(playerActive.getBoard().getCell(playerActive.getPosition()).getPiece()),
                () -> assertEquals(CellState.PIECE, playerActive.getBoard().getCell(playerActive.getPosition()).getCellState()),
                () -> assertNull(playerActive.visit(received)),
                () -> assertNull(playerActive.getPiece()), // player has no piece
                () -> assertNull(playerActive.getBoard().getCell(playerActive.getPosition()).getPiece()),
                () -> assertEquals(CellState.EMPTY, playerActive.getBoard().getCell(playerActive.getPosition()).getCellState())
        );
    }

    /**
     * ActionStatus - TestStatus message
     * Use Case 7: As a Player I want to check if Piece is a sham or not to know what to do with it, pickup or not.
     * subcase01 - happy path - status OK, test = true
     */
    @Test
    public void testMessageReturn01Test() {
        // setup
        playerActive.setPiece(new Piece(null));
        // test
        TestMessageReturn received = new TestMessageReturn(StatusMessageEnum.OK, playerUUIDActive, true);
        assertAll(
                () -> assertEquals(new Piece(null), playerActive.getPiece()),
                () -> assertNull(playerActive.visit(received)),
                () -> assertEquals(new Piece(false), playerActive.getPiece()) // player has true piece
        );
    }

    /**
     * ActionStatus - TestStatus message
     * Use Case 7: As a Player I want to check if Piece is a sham or not to know what to do with it, pickup or not.
     * subcase02 - status DENIED
     */
    @Test
    public void testMessageReturn02Test() {
        // setup
        playerActive.setPiece(new Piece(null));
        // test
        TestMessageReturn received = new TestMessageReturn(StatusMessageEnum.DENIED, playerUUIDActive, null);
        assertAll(
                () -> assertEquals(new Piece(null), playerActive.getPiece()),
                () -> assertNull(playerActive.visit(received)),
                () -> assertNull(playerActive.getPiece()) // player did not had piece
        );
    }

    /**
     * ActionStatus - TestStatus message
     * Use Case 7: As a Player I want to check if Piece is a sham or not to know what to do with it, pickup or not.
     * subcase03 - status OK, test = false
     */
    @Test
    public void testMessageReturn03Test() {
        // setup
        playerActive.setPiece(new Piece(null));
        // test
        TestMessageReturn received = new TestMessageReturn(StatusMessageEnum.OK, playerUUIDActive, false);
        assertAll(
                () -> assertEquals(new Piece(null), playerActive.getPiece()),
                () -> assertNull(playerActive.visit(received)),
                () -> assertNull(playerActive.getPiece()) // player has true piece
        );
    }

    /**
     * ActionStatus - PlaceStatus message
     * Use Case 8: As a Player I want to know if the Piece I have placed discovered a goal or not
     * subcase01 - happy path - correct placement, status OK, player looses piece
     */
    @Test
    public void placeMessageReturn01Test() {
        // setup
        playerActive.setPiece(new Piece(null));

        // test
        PlaceMessageReturn received = new PlaceMessageReturn(StatusMessageEnum.OK, playerUUIDActive, PlacementResultEnum.Correct);
        //FIXME
        //number of discovered goals +1, but player does not track it
        assertAll(
                () -> assertEquals(new Piece(null), playerActive.getPiece()),
                () -> assertEquals(CellState.UNKNOWN, playerActive.getBoard().getCell(playerActive.getPosition()).getCellState()),
                () -> assertNull(playerActive.visit(received)),
                () -> assertEquals(CellState.DISCOVERED_GOAL, playerActive.getBoard().getCell(playerActive.getPosition()).getCellState()),
                () -> assertNull(playerActive.getPiece())
        );
    }

    /**
     * ActionStatus - PlaceStatus message
     * Use Case 8: As a Player I want to know if the Piece I have placed discovered a goal or not
     * subcase02 - happy path - pointless placement, status OK, player looses piece
     */
    @Test
    public void placeMessageReturn02Test() {
        // setup
        playerActive.setPiece(new Piece(null));

        // test
        PlaceMessageReturn received = new PlaceMessageReturn(StatusMessageEnum.OK, playerUUIDActive, PlacementResultEnum.Pointless);
        //FIXME
        //number of discovered goals +1, but player does not track it
        assertAll(
                () -> assertEquals(new Piece(null), playerActive.getPiece()),
                () -> assertEquals(CellState.UNKNOWN, playerActive.getBoard().getCell(playerActive.getPosition()).getCellState()),
                () -> assertNull(playerActive.visit(received)),
                () -> assertEquals(CellState.DISCOVERED_NON_GOAL, playerActive.getBoard().getCell(playerActive.getPosition()).getCellState()),
                () -> assertNull(playerActive.getPiece())
        );
    }

    /**
     * ActionStatus - PlaceStatus message
     * Use Case 8: As a Player I want to know if the Piece I have placed discovered a goal or not
     * subcase03 - status DENIED
     */
    @Test
    public void placeMessageReturn03Test() {
        // setup
        playerActive.setPiece(new Piece(null));

        // test
        PlaceMessageReturn received = new PlaceMessageReturn(StatusMessageEnum.DENIED, playerUUIDActive, null);
        //FIXME
        //number of discovered goals +1, but player does not track it
        assertAll(
                () -> assertEquals(new Piece(null), playerActive.getPiece()),
                () -> assertEquals(CellState.UNKNOWN, playerActive.getBoard().getCell(playerActive.getPosition()).getCellState()),
                () -> assertNull(playerActive.visit(received)),
                () -> assertEquals(CellState.UNKNOWN, playerActive.getBoard().getCell(playerActive.getPosition()).getCellState()),
                () -> assertNull(playerActive.getPiece())
        );
    }

    /**
     * ActionStatus - Discover messages
     * Use Case 9: As Player I want to process players discover messages response, so that I can know the fields around me
     * subcase01 - happy path
     */
    @Test
    public void discoverMessageReturn01Test() {
        //given
        //player position (1,4)
        playerActive.setPosition(playerActivePositionForDiscoverTests);
        playerActive.getBoard().getCell(playerActivePosition).setPawnGuid(null);
        playerActive.getBoard().getCell(playerActivePositionForDiscoverTests).setPawnGuid(playerUUIDActive);

        List<Cell> neighbouringCells01 = new ArrayList<>();
        neighbouringCells01.add(new Cell(CellState.UNKNOWN, null, 0, new Position(0, 3), CellColor.GRAY, null));
        neighbouringCells01.add(new Cell(CellState.UNKNOWN, null, 0, new Position(1, 3), CellColor.GRAY, null));
        neighbouringCells01.add(new Cell(CellState.UNKNOWN, null, 0, new Position(2, 3), CellColor.GRAY, null));
        neighbouringCells01.add(new Cell(CellState.UNKNOWN, null, 0, new Position(0, 4), CellColor.GRAY, null));
        neighbouringCells01.add(new Cell(CellState.UNKNOWN, playerUUIDActive, 0, new Position(1, 4), CellColor.GRAY, null));
        neighbouringCells01.add(new Cell(CellState.UNKNOWN, null, 0, new Position(2, 4), CellColor.GRAY, null));
        neighbouringCells01.add(new Cell(CellState.UNKNOWN, null, 0, new Position(0, 5), CellColor.GRAY, null));
        neighbouringCells01.add(new Cell(CellState.UNKNOWN, null, 0, new Position(1, 5), CellColor.GRAY, null));
        neighbouringCells01.add(new Cell(CellState.UNKNOWN, null, 0, new Position(2, 5), CellColor.GRAY, null));

        List<Cell> neighbouringCells02 = new ArrayList<>();
        neighbouringCells02.add(new Cell(CellState.EMPTY, null, 1, new Position(0, 3), CellColor.GRAY, null));
        neighbouringCells02.add(new Cell(CellState.EMPTY, null, 2, new Position(1, 3), CellColor.GRAY, null));
        neighbouringCells02.add(new Cell(CellState.EMPTY, null, 3, new Position(2, 3), CellColor.GRAY, null));
        neighbouringCells02.add(new Cell(CellState.PIECE, null, 0, new Position(0, 4), CellColor.GRAY, null));
        neighbouringCells02.add(new Cell(CellState.EMPTY, playerUUIDActive, 1, new Position(1, 4), CellColor.GRAY, null));
        neighbouringCells02.add(new Cell(CellState.EMPTY, null, 2, new Position(2, 4), CellColor.GRAY, null));
        neighbouringCells02.add(new Cell(CellState.EMPTY, null, 1, new Position(0, 5), CellColor.GRAY, null));
        neighbouringCells02.add(new Cell(CellState.EMPTY, null, 2, new Position(1, 5), CellColor.GRAY, null));
        neighbouringCells02.add(new Cell(CellState.EMPTY, null, 3, new Position(2, 5), CellColor.GRAY, null));

        List<MessageFields> neighbouringCellsMessageFields01 = convertCellsToMessageFields(neighbouringCells02);

        MessagePosition messagePositionOfPlayer1 = new MessagePosition(playerActive.getPosition().getX(), playerActive.getPosition().getY());

        List<Cell> checkBeforeUpdate = playerActive.getBoard().getNeighbouringCells(playerActive.getPosition());
        assertEquals(neighbouringCells01, checkBeforeUpdate);

        //when
        DiscoverMessageReturn received = new DiscoverMessageReturn(playerUUIDActive, messagePositionOfPlayer1,
                StatusMessageEnum.OK, neighbouringCellsMessageFields01);
        Message result = playerActive.visit(received);

        List<Cell> updated = playerActive.getBoard().getNeighbouringCells(playerActive.getPosition()); // what player has after updating

        List<Cell> expected = neighbouringCells02;

        //then
        assertAll(
                () -> assertEquals(expected, updated),
                () -> assertNull(result)
        );
    }

    /**
     * ActionStatus - Discover messages
     * Use Case 9: As Player I want to process players discover messages response, so that I can know the fields around me
     * subcase02 - message with DENIED is not changing fields on board of player.
     */
    @Test
    public void discoverMessageReturn02Test() {
        //given
        //player position (1,4)
        playerActive.setPosition(playerActivePositionForDiscoverTests);
        playerActive.getBoard().getCell(playerActivePosition).setPawnGuid(null);
        playerActive.getBoard().getCell(playerActivePositionForDiscoverTests).setPawnGuid(playerUUIDActive);

        List<Cell> neighbouringCells02 = new ArrayList<>();
        neighbouringCells02.add(new Cell(CellState.EMPTY, null, 1, new Position(0, 3), CellColor.GRAY, null));
        neighbouringCells02.add(new Cell(CellState.EMPTY, null, 2, new Position(1, 3), CellColor.GRAY, null));
        neighbouringCells02.add(new Cell(CellState.EMPTY, null, 3, new Position(2, 3), CellColor.GRAY, null));
        neighbouringCells02.add(new Cell(CellState.PIECE, null, 0, new Position(0, 4), CellColor.GRAY, null));
        neighbouringCells02.add(new Cell(CellState.EMPTY, playerUUIDActive, 1, new Position(1, 4), CellColor.GRAY, null));
        neighbouringCells02.add(new Cell(CellState.EMPTY, null, 2, new Position(2, 4), CellColor.GRAY, null));
        neighbouringCells02.add(new Cell(CellState.EMPTY, null, 1, new Position(0, 5), CellColor.GRAY, null));
        neighbouringCells02.add(new Cell(CellState.EMPTY, null, 2, new Position(1, 5), CellColor.GRAY, null));
        neighbouringCells02.add(new Cell(CellState.EMPTY, null, 3, new Position(2, 5), CellColor.GRAY, null));

        MessagePosition messagePositionOfPlayer1 = new MessagePosition(playerActive.getPosition().getX(), playerActive.getPosition().getY());
        List<Cell> checkBeforeUpdate = playerActive.getBoard().getNeighbouringCells(playerActive.getPosition());

        // when
        DiscoverMessageReturn received = new DiscoverMessageReturn(playerUUIDActive, messagePositionOfPlayer1,
                StatusMessageEnum.DENIED, null);
        List<MessageFields> neighbouringCellsMessageFields02 = convertCellsToMessageFields(neighbouringCells02);
        DiscoverMessageReturn received2 = new DiscoverMessageReturn(playerUUIDActive, messagePositionOfPlayer1,
                StatusMessageEnum.OK, neighbouringCellsMessageFields02);

        //then
        assertEquals(checkBeforeUpdate, playerActive.getBoard().getNeighbouringCells(playerActive.getPosition()));
        assertNull(playerActive.visit(received));
        assertEquals(checkBeforeUpdate, playerActive.getBoard().getNeighbouringCells(playerActive.getPosition()));
        assertNull(playerActive.visit(received2));
        assertEquals(neighbouringCells02, playerActive.getBoard().getNeighbouringCells(playerActive.getPosition()));
        assertNull(playerActive.visit(received));
        assertEquals(neighbouringCells02, playerActive.getBoard().getNeighbouringCells(playerActive.getPosition()));
    }

    /**
     * GameEnd
     * Use Case 10: As a Player I want to know when game ends and who has won, so that I do not continue playing
     * subcase01 - RED won
     */
    @Test
    public void endMessage01Test() {
        EndMessage received = new EndMessage(TeamColorMessageEnum.Red);

        assertEquals(PlayerState.Active, playerActive.getPlayerState());
        assertNull(playerActive.visit(received));
        assertEquals(PlayerState.Completed, playerActive.getPlayerState());
    }

    /**
     * GameEnd
     * Use Case 10: As a Player I want to know when game ends and who has won, so that I do not continue playing
     * subcase02 - BLUE won
     */
    @Test
    public void endMessage02Test() {
        // when
        EndMessage received = new EndMessage(TeamColorMessageEnum.Blue);
        // then
        assertEquals(PlayerState.Active, playerActive.getPlayerState());
        assertNull(playerActive.visit(received));
        assertEquals(PlayerState.Completed, playerActive.getPlayerState());
    }

    /**
     * Use Case 11: As a Player I don't want to get stuck with DiscoverMessage, this message is not for player
     * subcase01
     */
    @Test
    public void discoverMessage01Test() {
        // setup
        MessagePosition messagePosition = new MessagePosition(playerActivePosition.getX(), playerActivePosition.getY());
        // test
        DiscoverMessage received = new DiscoverMessage(playerUUIDActive, messagePosition);
        assertNull(playerActive.visit(received));
    }

    /**
     * Use Case 13: As a Player I don't want to get stuck with MoveMessage, this message is not for player
     * subcase01
     */
    @Test
    public void moveMessage01Test() {
        MoveMessage received = new MoveMessage(playerUUIDActive, DirectionMessageEnum.Down);
        assertNull(playerActive.visit(received));
    }

    /**
     * Use Case 13: As a Player I don't want to get stuck with MoveMessage, this message is not for player
     * subcase02 - up
     */
    @Test
    public void moveMessage02Test() {
        MoveMessage received = new MoveMessage(playerUUIDActive, DirectionMessageEnum.Up);
        assertNull(playerActive.visit(received));
    }

    /**
     * Use Case 13:
     * subcase03 - left
     */
    @Test
    public void moveMessage03Test() {
        MoveMessage received = new MoveMessage(playerUUIDActive, DirectionMessageEnum.Left);
        assertNull(playerActive.visit(received));
    }

    /**
     * Use Case 13:
     * subcase04 - right
     */
    @Test
    public void moveMessage04Test() {
        MoveMessage received = new MoveMessage(playerUUIDActive, DirectionMessageEnum.Right);
        assertNull(playerActive.visit(received));
    }

    /**
     * Use Case 14: As a Player I don't want to get stuck with PickUpMessage, this message is not for player
     * subcase01 - down
     */
    @Test
    public void pickUpMessage01Test() {
        PickUpMessage received = new PickUpMessage(playerUUIDActive);
        assertNull(playerActive.visit(received));
    }

    /**
     * Use Case 15: As a Player I don't want to get stuck with PlaceMessage, this message is not for player
     * subcase01 -
     */
    @Test
    public void placeMessage01Test() {
        PlaceMessage received = new PlaceMessage(playerUUIDActive);
        assertNull(playerActive.visit(received));
    }

    /**
     * Use Case 16: As a Player I don't want to get stuck with PlaceMessage, this message is not for player
     * subcase01
     */
    @Test
    public void testMessage01Test() {
        TestMessage received = new TestMessage(playerUUIDActive);
        assertNull(playerActive.visit(received));
    }


}