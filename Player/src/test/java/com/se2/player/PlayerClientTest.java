package com.se2.player;

import com.se2.communication.MessageHelper;
import com.se2.communication.classes.MessageBoard;
import com.se2.communication.classes.MessagePosition;
import com.se2.communication.enums.DirectionMessageEnum;
import com.se2.communication.enums.StatusMessageEnum;
import com.se2.communication.enums.TeamColorMessageEnum;
import com.se2.communication.messages.*;
import com.se2.game.board.Cell;
import com.se2.game.board.Piece;
import com.se2.game.board.Position;
import com.se2.game.board.enums.CellColor;
import com.se2.game.board.enums.CellState;
import com.se2.game.player.enums.PlayerState;
import com.se2.player.configuration.PlayerConfiguration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class PlayerClientTest {

    private final String ip = "127.0.0.1";
    private final int port = 9999;
    private final int x = 0;
    private final int y = 9;
    private final PlayerConfiguration playerOneConfiguration = new PlayerConfiguration(
            "DefaultFile",
            ip,
            port,
            "log.out",
            "TestPlayer",
            true,
            3,
            "Default");
    private GameStartMessage gameStartMessage;

    @Mock
    private Player player;

    private ServerSocket serverSocket;
    private Socket socket;
    private String messageInSocket;
    private BufferedReader bufferedReader;
    private BufferedWriter bufferedWriter;

    @BeforeEach
    public void setup() {
        new Thread(
                () -> {
                    try {
                        this.serverSocket = new ServerSocket(port);
                        this.socket = serverSocket.accept();
                        this.bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                        while (true) {
                            messageInSocket = this.bufferedReader.readLine();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        ).start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.player = new Player(playerOneConfiguration);
        this.player.setPlayerState(PlayerState.WaitingForGameStartMessage);
        this.gameStartMessage = new GameStartMessage(
                this.player.getPlayerGuid(),
                TeamColorMessageEnum.Red,
                "Whatever",
                1,
                List.of(this.player.getPlayerGuid()),
                new MessagePosition(x, y),
                new MessageBoard(4, 6, 2));
    }

    @AfterEach
    public void cleanup() {
        try {
            this.serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSocketWithReceivingMoveMessage() {
        //given
        PlayerClient playerClient = new PlayerClient(ip, port, this.player);
        playerClient.start();
        this.player.visit(gameStartMessage);

        //when
        DirectionMessageEnum direction = DirectionMessageEnum.Up;
        MoveMessage expectedMoveMessage = new MoveMessage(this.player.getPlayerGuid(), direction);
        this.player.postMoveMessage(direction);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        MoveMessage actualMoveMessage = (MoveMessage) MessageHelper.getMessageFromJson(messageInSocket);

        //then
        assertEquals(expectedMoveMessage, actualMoveMessage);
    }

    @Test
    public void testSocketWithReceivingDiscoverMessage() {
        //given
        PlayerClient playerClient = new PlayerClient(ip, port, this.player);
        playerClient.start();
        this.player.visit(gameStartMessage);

        //when
        DiscoverMessage expectedDiscoverMessage = new DiscoverMessage(this.player.getPlayerGuid(), new MessagePosition(this.player.getPosition().getX(), this.player.getPosition().getY()));
        this.player.postDiscoverMessage();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        DiscoverMessage actualDiscoverMessage = (DiscoverMessage) MessageHelper.getMessageFromJson(messageInSocket);

        //then
        assertEquals(expectedDiscoverMessage, actualDiscoverMessage);
    }

    @Test
    public void testSocketWithReceivingPickupMessage() {
        //given
        PlayerClient playerClient = new PlayerClient(ip, port, this.player);
        playerClient.start();
        this.player.visit(gameStartMessage);

        //when
        PickUpMessage expectedPickupMessage = new PickUpMessage(this.player.getPlayerGuid());
        this.player.postPickupMessage();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        PickUpMessage actualPickupMessage = (PickUpMessage) MessageHelper.getMessageFromJson(messageInSocket);

        //then
        assertEquals(expectedPickupMessage, actualPickupMessage);
    }

    @Test
    public void testSocketWithReceivingPlaceMessage() {
        //given
        PlayerClient playerClient = new PlayerClient(ip, port, this.player);
        playerClient.start();
        this.player.visit(gameStartMessage);

        //when
        PlaceMessage expectedPlaceMessage = new PlaceMessage(this.player.getPlayerGuid());
        this.player.postPlaceMessage();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        PlaceMessage actualPlaceMessage = (PlaceMessage) MessageHelper.getMessageFromJson(messageInSocket);

        //then
        assertEquals(expectedPlaceMessage, actualPlaceMessage);
    }

    @Test
    public void testSocketWithReceivingTestMessage() {
        //given
        PlayerClient playerClient = new PlayerClient(ip, port, this.player);
        playerClient.start();
        this.player.visit(gameStartMessage);

        //when
        TestMessage expectedTestMessage = new TestMessage(this.player.getPlayerGuid());
        this.player.postTestPieceMessage();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        TestMessage actualTestMessage = (TestMessage) MessageHelper.getMessageFromJson(messageInSocket);

        //then
        assertEquals(expectedTestMessage, actualTestMessage);
    }

    @Test
    public void testSocketWithSendingMoveMessage() {
        //given
        PlayerClient playerClient = new PlayerClient(ip, port, this.player);
        playerClient.start();
        this.player.visit(gameStartMessage);

        MoveMessageReturn moveMessageReturn = new MoveMessageReturn(StatusMessageEnum.OK,
                this.player.getPlayerGuid(),
                DirectionMessageEnum.Up,
                new MessagePosition(this.x, this.y - 1));

        try {
            this.bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            String jsonToSend = MessageHelper.getJsonFromMessage(moveMessageReturn);
            this.bufferedWriter.write(jsonToSend);
            this.bufferedWriter.flush();
            Thread.sleep(1000);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Position expectedPosition = new Position(this.x, this.y - 1);

        assertEquals(expectedPosition, player.getPosition());
    }

    @Test
    public void testSocketWithSendingPickupMessage() {
        //given
        int x = 3;
        int y = 4;
        PlayerClient playerClient = new PlayerClient(ip, port, this.player);
        playerClient.start();
        this.player.visit(gameStartMessage);
        MoveMessageReturn moveMessageReturn = new MoveMessageReturn(StatusMessageEnum.OK,
                this.player.getPlayerGuid(),
                DirectionMessageEnum.Up,
                new MessagePosition(x, y));

        this.player.getBoard().updateCell(new Cell(CellState.PIECE,
                this.player.getPlayerGuid(),
                0,
                new Position(x, y),
                CellColor.GRAY,
                new Piece(false)));
        PickUpMessageReturn pickUpMessageReturn = new PickUpMessageReturn(StatusMessageEnum.OK,
                this.player.getPlayerGuid());

        //when
        try {
            this.bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            String jsonToSend = MessageHelper.getJsonFromMessage(moveMessageReturn);
            this.bufferedWriter.write(jsonToSend);
            this.bufferedWriter.flush();
            Thread.sleep(1000);

            jsonToSend = MessageHelper.getJsonFromMessage(pickUpMessageReturn);
            this.bufferedWriter.write(jsonToSend);
            this.bufferedWriter.flush();
            Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Piece actualPiece = this.player.getPiece();

        //then
        assertNotNull(actualPiece);
    }
}